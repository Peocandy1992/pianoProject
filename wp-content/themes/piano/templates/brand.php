<?php
/**
 * Template Name: Brand
 */
?>
<?php get_header(); ?>
<div class="breadcrumb">
    <a href="#">HOME</a> &nbsp; &nbsp; >  &nbsp; &nbsp;
    <a href="#">Yamaha</a>
</div>
<div class="row">
    <div class="container">
        <div class="col-sm-9">
            <div class="brand_image_top form-group">
                <img src="<?php bloginfo('template_url'); ?>/images/yamaha.png" class="img-responsive" alt="Brand" />
            </div>
            <div class="brand_description form-group">
                Là đại lý Yamaha lớn nhất miền Bắc, chúng tôi có tất cả các sản phẩm mới nhất từ ​​dòng sản phẩm của Yamaha với giá cực rẻ. Bạn có thể tìm kiếm tất cả các sản phẩm của Yamaha theo danh sách bên dưới.
            </div>


            <?php get_template_part('templates/part/product', 'categories'); ?>

            <?php get_template_part('templates/part/product', 'feature'); ?>

            <?php get_template_part('templates/part/product', 'new'); ?>

            <?php get_template_part('templates/part/product', 'saleoff'); ?>



        </div>
        <div class="col-sm-3">
            <?php get_template_part('templates/part/sidebar', 'hot_bestsale'); ?>
            <?php get_template_part('templates/part/sidebar', 'bestsale'); ?>
            <?php get_template_part('templates/part/sidebar', 'bestsale'); ?>
            <?php get_template_part('templates/part/sidebar', 'support'); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
