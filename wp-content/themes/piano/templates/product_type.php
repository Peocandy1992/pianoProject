<?php
/**
 * Template Name: Product Type
 */
?>
<?php get_header(); ?>
<div class="row">
    <div class="container">
    <div class="col-sm-9">
        <div class="product_type_top">
            <div class="breadcrumb">
                <a href="#"><i class="fa fa-home"></i></a> &nbsp; &nbsp; >  &nbsp; &nbsp;
                <a href="#">Yamaha</a>
            </div>

            <div class="product_type_title">
                Upright Piano
            </div>

            <ul class="product_type_menu">
                <li><a href="#">Mới về</a></li>
                <li><a href="#">Bán chạy</a></li>
                <li><a href="#">Mới về</a></li>
                <li><a href="#">Đang giảm giá</a></li>
            </ul>
        </div>

        <?php get_template_part('templates/part/product', 'brand'); ?>

    </div>
    <div class="col-sm-3">

        <?php get_template_part('templates/part/sidebar', 'support'); ?>

        <?php get_template_part('templates/part/sidebar', 'hot_bestsale'); ?>

    </div>
    <div class="clearfix"></div>

    <div class="col-sm-12">
        <?php get_template_part('templates/part/product', 'feature'); ?>

        <?php get_template_part('templates/part/product', 'support'); ?>

    </div>
    </div>
</div>
<?php get_footer(); ?>

