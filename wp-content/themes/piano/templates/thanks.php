<?php
/**
 * Template Name: Thanks
 */
?>
<?php get_header(); ?>
<div class="container">
<div class="breadcrumb">
    <a href="#"><i class="fa fa-home"></i></a> &nbsp; &nbsp; >  &nbsp; &nbsp;
    <a href="#">Giỏ hàng</a> &nbsp; &nbsp; >  &nbsp; &nbsp;
    <a href="#">Thanks</a>
</div>
<div class="row">
    <div class="col-sm-9">
        <div class="row">
            <div class="col-sm-7">
                <div class="about_title">
                    Cảm ơn Quý khách đã đặt hàng tại Pianobt.vn!
                </div>
                <div class="order_result_container">
                    <div class="form-group">
                        Pianobt xác nhận đã nhận được đơn đặt hàng của Quý khách theo thông tin bên dưới.<br/>
                        Đội hỗ trợ sẽ liên hệ với Quý khách sớm nhất có thể. Nếu Quý khách thắc mắc trong việc mua hàng, đừng ngần ngại liên lạc với chúng tôi.
                    </div>
                    <div class="cus_info_row">
                        <div class="cus_title">ĐỊA CHỈ:</div>
                        <div class="cus_content">
                            Chung cư FLC, Lê Đức Thọ, Nam Từ Liêm, Hà Nội<br/>
                            Quận Nam Từ Liêm<br/>
                            Hà Nội<br/>
                        </div>
                    </div>
                    <div class="cus_info_row">
                        <span class="cus_title">EMAIL:</span>
                        <span class="cus_content">
                            abc@gmail.com
                        </span>
                    </div>
                    <div class="cus_info_row">
                        <span class="cus_title">SỐ ĐIỆN THOẠI:</span>
                        <span class="cus_content">
                            0987654321
                        </span>
                    </div>
                    <table class="table table-bordered table_order">
                        <tr>
                            <td>23232</td>
                            <td>1x Thomann DP-31 B</td>
                            <td>Có sẵn hàng</td>
                            <td class="text-right">48.000.000đ</td>
                        </tr>
                        <tr>
                            <td colspan="3">Phí shipping</td>
                            <td class="text-right">0đ</td>
                        </tr>
                        <tr>
                            <td colspan="3">Tổng</td>
                            <td class="text-right">48.000.000đ</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-5">
                <img src="<?php bloginfo('template_url'); ?>/images/thanks.png" class="img-responsive pull-right" alt="Thanks" />
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="our_channer_container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="sidebar_box channel_box">
                        <div class="channel_image">
                            <img src="<?php bloginfo('template_url'); ?>/images/ic-facebook-big.png" class="img-responsive" alt="Facebook" />
                        </div>
                        <div class="channel_name">Fanpage</div>
                        <div class="channel_description">Video, demo, hướng dẫn, các sự kiện đặc biệt và gặp gỡ những nghệ sĩ, những người bạn của Pianobt</div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="sidebar_box channel_box">
                        <div class="channel_image">
                            <img src="<?php bloginfo('template_url'); ?>/images/ic-youtube-big.png" class="img-responsive" alt="Facebook" />
                        </div>
                        <div class="channel_info">
                            <div class="channel_name">Youtube Channel</div>
                            <div class="channel_description">Video, demo, hướng dẫn, các sự kiện đặc biệt và gặp gỡ những nghệ sĩ, những người bạn của Pianobt</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <?php get_template_part('templates/part/sidebar', 'support'); ?>

        <?php get_template_part('templates/part/sidebar-cart', 'policy'); ?>

        <?php get_template_part('templates/part/sidebar', 'fanpage'); ?>
    </div>
</div>
</div>
<?php get_footer(); ?>


