<div class="container">
<div class="col-sm-9">
    <?php the_content(); ?>
</div>
<div class="col-sm-3">
    <?php get_sidebar('cart'); ?>
</div>
</div>