<div class="sidebar_box">
    <div class="sidebar_title">PianoBT Fanpage</div>
    <div class="sidebar_content">
        <img src="<?php bloginfo('template_url'); ?>/images/fanpage.png" alt="">
        <p class="fanpage_title">Theo dõi Fanpage của PianoBT và bạn sẽ không bao giờ bõ lỡ những câu chuyện thú vị về âm nhạc và piano.
        </p>

        <?php
        $linkFacebook = get_field('link_facebook', 'option');
        ?>
        <div class="fb-page" data-href="<?php echo $linkFacebook; ?>" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
             <div class="btn btn_fanpage"><a href="<?php echo $linkFacebook; ?>"><img src="<?php bloginfo('template_url'); ?>/images/240px-F_icon.svg.png" alt="" class="fb_img">LIKE FANPAGE</a></div>
        </div>
    </div>
</div>