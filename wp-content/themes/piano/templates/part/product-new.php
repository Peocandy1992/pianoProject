<div class="news_product_block hidden-xs">
    <div class="block_title has_carousel_nav">
        <span>Hàng mới về</span>
        <div class="carousel_box">
            <div class="carousel_pre">
                <img src="<?php echo  get_bloginfo('template_url').'/images/left-btn.png' ?>" alt="" class="btn-disable" >
                <img src="<?php echo  get_bloginfo('template_url').'/images/left-btn-disable.png' ?>" alt="" class="btn-able">
            </div>
            <div class="carousel_next">
                <img src="<?php echo  get_bloginfo('template_url').'/images/right-btn.png' ?>" alt="" class="btn-disable">
                <img src="<?php echo  get_bloginfo('template_url').'/images/right-btn-disable.png' ?>" alt="" class="btn-able">
            </div>
        </div>
    </div>
    <div class="block_container">
        <div class="news_product_list owl-carousel owl-theme">
            <?php
            $newProduct = get_field('new_product', 'option');
            if(!empty($newProduct)){
                foreach($newProduct as $key => $value){
                    $value = $value['product'];
                    $productId = $value->ID;
                    $link = get_permalink($productId);
                    $_product = wc_get_product($productId);

            ?>
            <div class="product_box">
                <div class="product_image">
                    <a href="<?php echo $link; ?>">
                        <img src="<?php echo !empty(get_the_post_thumbnail_url($productId))? get_the_post_thumbnail_url($productId) : bloginfo('template_url').'/images/default-thumb.png' ?>" class="img-responsive" alt="<?php echo get_the_title($productId); ?>" />
                    </a>
                </div>
                <div class="product_name">
                    <a href="<?php echo $link; ?>">
                        <?php echo get_the_title($productId); ?>
                    </a>
                </div>
                <div class="product_rating">
                    <?php $numberStar = get_field('rating', $productId);
                    if($numberStar > 0) {
                        for ($star = 0; $star < $numberStar; $star++) {
                            ?>
                            <!--                                        <i class='fa fa-star'></i>-->
                            <img src="<?php echo get_bloginfo('template_url') . '/images/star.png' ?>">
                        <?php }
                    }else{ ?>
                        <img src="<?php echo  get_bloginfo('template_url').'/images/star.png'  ?>">
                        <img src="<?php echo  get_bloginfo('template_url').'/images/star.png'  ?>">
                        <img src="<?php echo  get_bloginfo('template_url').'/images/star.png'  ?>">
                        <img src="<?php echo  get_bloginfo('template_url').'/images/star.png'  ?>">
                        <img src="<?php echo  get_bloginfo('template_url').'/images/star.png'  ?>">
                    <?php }
                    ?>
                </div>
                <div class="product_price">
                    <?php echo $_product->get_price() ? number_format($_product->get_price()).'đ' : 'Liên hệ'; ?>
                </div>

                <div class="product_mask">
                    <div class="product_image">
                        <a href="<?php echo $link; ?>">
                            <img src="<?php echo !empty(get_the_post_thumbnail_url($productId))? get_the_post_thumbnail_url($productId) : bloginfo('template_url').'/images/default-thumb.png'?>" class="img-responsive" alt="<?php echo get_the_title($productId); ?>" />
                        </a>
                    </div>
                    <div class="product_name">
                        <a href="<?php echo $link; ?>">
                            <?php echo get_the_title($productId); ?>
                        </a>
                    </div>
                    <div class="product_rating">
                        <?php $numberStar = get_field('rating', $productId);
                        if($numberStar > 0) {
                            for ($star = 0; $star < $numberStar; $star++) {
                                ?>
                                <!--                                        <i class='fa fa-star'></i>-->
                                <img src="<?php echo get_bloginfo('template_url') . '/images/star.png' ?>">
                            <?php }
                        }else{ ?>
                            <img src="<?php echo  get_bloginfo('template_url').'/images/star.png'  ?>">
                            <img src="<?php echo  get_bloginfo('template_url').'/images/star.png'  ?>">
                            <img src="<?php echo  get_bloginfo('template_url').'/images/star.png'  ?>">
                            <img src="<?php echo  get_bloginfo('template_url').'/images/star.png'  ?>">
                            <img src="<?php echo  get_bloginfo('template_url').'/images/star.png'  ?>">
                        <?php }
                        ?>
                    </div>
                    <div class="product_price">
                        <?php echo $_product->get_price() ? number_format($_product->get_price()).'đ' : 'Liên hệ'; ?>
                    </div>

                    <div class="add_to_cart">

                        <?php $product = wc_get_product($productId); ?>
                        <a href='<?php echo $product->add_to_cart_url() . '&quantity=1' ?>'>
                           <div class="btn btn-warning btn_order">Thêm Vào giỏ
                            hàng
                           </div>
                        </a>
                    </div>
                </div>
            </div>
            <?php
                }
            }?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<script type="text/javascript" language="javascript">
    var $ = jQuery;
    $(document).ready(function () {
        $('.sidebar_product_order .btn_order').click(function () {
            var product_id = $('.product_id').val();
            var number_product = parseInt($('.number_product').val());
            if (number_product > 0) {
                window.location = location.href + '?add-to-cart=' + product_id + '&quantity=' + number_product;
            } else {
                alert('Bạn phải nhập số sản phẩm muốn đặt hàng');
            }
        });
    });
</script>