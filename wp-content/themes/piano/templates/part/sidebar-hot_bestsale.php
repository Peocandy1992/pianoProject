<div class="sidebar_box hot_best_sale">
    <div class="sidebar_content">
        <div class="hot_best_sale_top">
            <div class="hot_best_sale_title pull-left">#1 Best Seller: </div>
            <div class="hot_best_sale_price pull-right">34.000.000 đ</div>
            <div class="clearfix"></div>
        </div>
        <div class="hot_best_sale_image">
            <img src="<?php bloginfo('template_url'); ?>/images/product.png" class="img-responsive" alt="Product" />
        </div>
        <div class="hot_best_sale_number">
            Hơn 750 chiếc đã được bán
        </div>
        <div class="hot_best_sale_name">
            Yamaha P-255 B
        </div>
    </div>
</div>