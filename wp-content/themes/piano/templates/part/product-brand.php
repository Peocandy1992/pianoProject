<div class="brand_block">
    <div class="block_title has_carousel_nav hidden-xs">
        <span>Thương hiệu hàng đầu</span>
        <div class="carousel_box">
            <!--            <div class="carousel_pre"><i class="fa fa-angle-left"></i></div>-->
            <!--            <div class="carousel_next"><i class="fa fa-angle-right"></i></div>-->
            <div class="carousel_pre swiper_pre">
<!--                <i class="fa fa-angle-left"></i>-->
                <!--                <div class="swiper-button-prev"></div>-->
                <img src="<?php echo  get_bloginfo('template_url').'/images/left-btn.png' ?>" alt="" class="btn-disable" >
                <img src="<?php echo  get_bloginfo('template_url').'/images/left-btn-disable.png' ?>" alt="" class="btn-able">
            </div>
            <div class="carousel_next swiper_next">
<!--                <i class="fa fa-angle-right"></i>-->
                <!--                <div class="swiper-button-next"></div>-->
                <img src="<?php echo  get_bloginfo('template_url').'/images/right-btn.png' ?>" alt="" class="btn-disable">
                <img src="<?php echo  get_bloginfo('template_url').'/images/right-btn-disable.png' ?>" alt="" class="btn-able">

            </div>
        </div>
    </div>

    <div class="block_container block_list_brand hidden-xs">
        <div class="row">
            <div class="list_brand swiper-container">
                <div class="swiper-wrapper">
                    <?php
                    $brand = get_field('brand', 'option');
                    if (!empty($brand)) {
                        foreach ($brand as $key => $value) {
                            $value = $value['item'];
                            $imageId = get_term_meta($value->term_id, 'pwb_brand_image');
                            if (!empty($imageId[0])) {
                                $image = wp_get_attachment_url($imageId[0]);
                            } else {
                                $image = get_bloginfo('template_url') . '/images/brand.png';
                            }
                            ?>
                            <div class="brand_li swiper-slide">
                                <a href="<?php echo get_term_link($value->term_id); ?>">
                                    <img src="<?php echo $image; ?>" class="img-responsive"
                                         alt="<?php echo $value->name; ?>"/>
                                </a>
                            </div>
                            <?php
                        }
                    }
                    ?>


                </div>

            </div>
        </div>
    </div>

    <div class="block_title block_title_responsive col-xs-12 visible-xs container">
        Thương hiệu hàng đầu
    </div>

    <div class="block_container block_list_brand block_list_brand_responsive visible-xs container">
        <div class="row">
            <div class="list_brand ">
                <div class="">
                    <?php
                    $brand = get_field('brand', 'option');
                    if (!empty($brand)) {
                        foreach ($brand as $key => $value) {
                            $value = $value['item'];
                            $imageId = get_term_meta($value->term_id, 'pwb_brand_image');
                            if (!empty($imageId[0])) {
                                $image = wp_get_attachment_url($imageId[0]);
                            } else {
                                $image = get_bloginfo('template_url') . '/images/brand.png';
                            }
                            ?>
                            <div class="brand_li col-xs-6">
                                <a href="<?php echo get_term_link($value->term_id); ?>">
                                    <img src="<?php echo $image; ?>" class="img-responsive"
                                         alt="<?php echo $value->name; ?>"/>
                                </a>
                            </div>
                            <?php
                        }
                    }
                    ?>


                </div>

            </div>
        </div>
    </div>
</div>