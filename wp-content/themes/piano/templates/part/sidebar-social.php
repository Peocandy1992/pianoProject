<div class="sidebar_box social_box">
    <div class="sidebar_content">
        <ul class="list_social">
            <li class="col-sm-6 col-xs-6">
                <div class="social_image">
                    <a href="<?php echo get_field('link_facebook', 'option'); ?>">
                        <img src="<?php bloginfo('template_url'); ?>/images/ic-facebook.png" class="img-responsive" alt="Social" />
                    </a>

                </div>
                <div class="social_name">
                    <a href="<?php echo get_field('link_facebook', 'option'); ?>">Facebook</a>
                </div>
            </li>
            <li class="col-sm-6 col-xs-6">
                <div class="social_image">
                    <a href="<?php echo get_field('link_zalo', 'option'); ?>">
                        <img src="<?php bloginfo('template_url'); ?>/images/ic-zalo.png" class="img-responsive" alt="Social" />
                    </a>

                </div>
                <div class="social_name">
                    <a href="<?php echo get_field('link_zalo', 'option'); ?>">Zalo</a>
                </div>
            </li>
            <li class="col-sm-6 col-xs-6">
                <div class="social_image">
                    <a href="<?php echo get_field('link_youtube', 'option'); ?>">
                        <img src="<?php bloginfo('template_url'); ?>/images/ic-youtube.png" class="img-responsive" alt="Social" />
                    </a>

                </div>
                <div class="social_name">
                    <a href="<?php echo get_field('link_youtube', 'option'); ?>">Youtube</a>
                </div>
            </li>
            <li class="col-sm-6 col-xs-6">
                <div class="social_image">
                    <a href="<?php echo get_field('link_instagram', 'option'); ?>">
                        <img src="<?php bloginfo('template_url'); ?>/images/ic-instagram.png" class="img-responsive" alt="Social" />
                    </a>

                </div>
                <div class="social_name">
                    <a href="<?php echo get_field('link_instagram', 'option'); ?>">Instagram</a>
                </div>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
</div>