<div class="sidebar_box">
    <div class="sidebar_title special_title">Hỗ trợ trực tuyến</div>
    <div class="sidebar_content">
        <div class="support_box text-center">
            <div class="row">
                <ul class="list_support">
                    <?php
                    $support = get_field('support', 'option');
                    if(!empty($support)){
                        foreach($support as $key => $value){
                            ?>
                            <li>
                                <a href="<?php echo $value['link']; ?>">
                                    <img src="<?php echo $value['image']; ?>" class="img-responsive img-rounded" alt="Support" />
                                </a>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </div>
            <div class="support_title" style="font-family: Roboto-Bold;font-size: 16px;  text-align: center;
  color: #333333;">Tư vấn Piano</div>
            <div class="support_email"><?php echo get_field('email', 'option'); ?></div>
            <div class="support_hotline">Phone: <?php echo get_field('phone', 'option'); ?></div>
            <div class="support_hotline">Mobile: <?php echo get_field('hotline', 'option'); ?></div>
        </div>
    </div>
</div>