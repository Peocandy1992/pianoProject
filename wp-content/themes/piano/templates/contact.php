<?php
/**
 * Template Name: Contact
 */
?>
<?php get_header(); ?>
<div class="container contact_page">
    <div class="sidebar col-sm-3">
        <?php get_template_part('templates/part/sidebar', 'support'); ?>

        <?php get_template_part('templates/part/sidebar', 'work_time'); ?>

        <?php get_template_part('templates/part/sidebar', 'fanpage'); ?>
    </div>

    <div class="col-sm-9">
        <div class="breadcrumb">
            <?php
            if (function_exists('bcn_display')) {
                bcn_display();
            }
            ?>
        </div>
        <div class="about_content">
            <?php if (have_posts()) {
                the_post();
                ?>
            <?php } ?>
            <div class="about_title">
                <?php the_title(); ?>
            </div>
            <div class="about_content_top">
                <div class="row">
                    <div class="col-sm-8">
                        <?php the_content(); ?>
                    </div>
                    <div class="col-sm-4">
                        <img src="<?php bloginfo('template_url'); ?>/images/phone.png" class="img-responsive"
                             alt="Contact" style="position: absolute;margin-top: -63px;" />
                    </div>
                </div>
            </div>

            <div class="contact_box">
                <div class="row">
                    <div class="col-sm-7">
                        <div class="form-group"></div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                Bất kỳ lúc nào bạn cần hỗ trợ, liên hệ với chúng tôi qua số điện thoại và email sau:
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="contact_item form-group">
                            <i class="fa fa-mobile"></i> <?php echo get_field('phone', 'option'); ?>
                        </div>
                        <div class="contact_item form-group">
                            <i class="fa fa-envelope"></i> <?php echo get_field('email', 'option'); ?>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <img src="<?php bloginfo('template_url'); ?>/images/customer-care.png" class="img-responsive"
                             alt="Customer Care"/>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="about_gallery">
                <div class="about_title">
                    Đến thăm cửa hàng của Pianobt
                </div>
                <div class="form-group">
                    <?php echo get_field('address_description', get_the_ID()); ?>
                </div>
                <div class="row">
                    <div class="col-sm-7">
                        <?php echo get_field('map', get_the_ID()); ?>
                    </div>
                    <div class="col-sm-5">
                        <?php echo get_field('address', get_the_ID()); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php get_footer(); ?>

