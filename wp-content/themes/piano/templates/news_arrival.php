<?php
/**
 * Template Name: News Arrival
 */
?>
<?php get_header(); ?>
<div class="container">
    <div class="col-sm-9">
        <div class="breadcrumb">
            <a href="#">HOME</a> &nbsp; &nbsp; > &nbsp; &nbsp;
            <a href="#">Upright Piano</a> &nbsp; &nbsp; > &nbsp; &nbsp;
            <a href="#">Mới về</a>
        </div>
        <div class="about_content">
            <div class="about_title">
                Upright Piano mới
            </div>

            <div class="form-group">
                <p>Chúng tôi có 7 sản phẩm đàn Upright Piano trong tháng này.</p>
            </div>

            <div class="new_arrival_list">
                <?php for ($i = 0; $i < 10; $i++) { ?>
                    <div class="new_arrival_box">
                        <div class="col-sm-3 new_arrival_image">
                            <img src="<?php bloginfo('template_url'); ?>/images/product.png" class="img-responsive"
                                 alt="Product"/>
                        </div>
                        <div class="col-sm-6 new_arrival_info">
                            <div class="new_arrival_name">
                                <a href="#">Yamaha YDP-S34 B Set</a>
                            </div>
                            <div class="new_arrival_description">
                                Yamaha YDP-S34 WA Arius Digital Piano - 88-note GHS keyboard, Yamaha CFX Piano Sound,
                                192-note polyphony, 10 voices, reverb, Damper Resonance, 3 pedals, internal store with
                                10 demo songs anf 50 piano songs, recording 2 tracks/1 song, Intelligent Acoustic
                                Control...
                            </div>
                            <div class="new_arrival_status">
                                Có sẵn hàng
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="new_arrival_price">
                                86.000.000 đ
                            </div>
                            <div class="new_arrival_brand">
                                <img src="<?php bloginfo('template_url'); ?>/images/yamaha.png" class="img-responsive"
                                     alt="Brand"/>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                <?php } ?>

                <div class="paginate text-right">
                    <ul class="ul_paginate">
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">6</a></li>
                    </ul>
                </div>

            </div>

        </div>
    </div>
    <div class="sidebar col-sm-3">
        <?php get_sidebar(); ?>
    </div>
</div>
<?php get_footer(); ?>
