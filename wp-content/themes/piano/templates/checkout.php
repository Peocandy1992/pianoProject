<?php
/**
 * Template Name: Check Out
 */
?>
<?php get_header(); ?>
<div class="breadcrumb">
    <a href="#">HOME</a> &nbsp; &nbsp; > &nbsp; &nbsp;
    <a href="#">Giỏ hàng</a> &nbsp; &nbsp; > &nbsp; &nbsp;
    <a href="#">Thanh toán</a>
</div>
<div class="row">
    <div class="container">
        <div class="col-sm-9">
            <div class="about_title">
                Thực hiện thanh toán
            </div>
            <div class="shopping_cart_container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="customer_info_box sidebar_box">
                            <div class="sidebar_title">THÔNG TIN KHÁCH HÀNG</div>
                            <div class="customer_form">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="Họ và tên"/>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="Số điện thoại"/>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="Email"/>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control"
                                                   placeholder="Tỉnh/ Thành phố"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control"
                                                   placeholder="Quận/huyện"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control"
                                           placeholder="Địa chỉ giao hàng"/>
                                </div>
                            </div>
                        </div>
                        <div class="payment_type sidebar_box">
                            <div class="sidebar_title">HÌNH THỨC THANH TOÁN</div>
                            <div class="payment_type_container">
                                <div class="radio">
                                    <label><input type="radio" name="optradio">Tiền mặt</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="optradio" checked>Chuyển khoản ngân hàng</label>
                                </div>
                                <ul class="payment_type_list">
                                    <li class="payment_type_item">
                                        <div class="pay_logo">
                                            <img src="<?php bloginfo('template_url'); ?>/images/vietcombank.png"
                                                 class="img-responsive center-block" alt="Vietcombank"/>
                                        </div>
                                        <div class="pay_info text-center">
                                            <b>Ngân hàng TMCP Ngoại Thương Việt Nam</b><br/>
                                            Chi nhánh Đống Đa, Hà Nội<br/>
                                            <b>Chủ tài khoản:</b> Nguyễn Bá Thành<br/>
                                            <b>Số Tài Khoản:</b> 0021000295898<br/>
                                        </div>
                                    </li>
                                    <li class="payment_type_item">
                                        <div class="pay_logo">
                                            <img src="<?php bloginfo('template_url'); ?>/images/bidv.png"
                                                 class="img-responsive center-block" alt="Vietcombank"/>
                                        </div>
                                        <div class="pay_info text-center">
                                            <b>Ngân hàng Ngân hàng Đầu tư và Phát triển Việt Nam</b><br/>
                                            Chi nhánh Hai Bà Trưng, Hà Nội<br/>
                                            <b>Chủ tài khoản: Nguyễn Bá Thành</b><br/>
                                            <b>Số Tài Khoản: 12110000164110</b><br/>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="checkout_cart">
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>
                                        <div class="sidebar_title no-padding no-margin">
                                            GIỎ HÀNG
                                        </div>
                                    </td>
                                    <td class="text-right">
                                        <a href="#">Xóa</a>
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="no-border">
                                        <a href="#">1x Thomann DP-31 B</a><br/>
                                        <span>Có sẵn hàng</span><br/>
                                    </td>
                                    <td class="no-border text-right">
                                        <div class="checkout_price">48.000.000 đ</div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="no-border">
                                        <a href="#">1x Thomann DP-31 B</a><br/>
                                        <span>Có sẵn hàng</span><br/>
                                    </td>
                                    <td class="no-border text-right">
                                        <div class="checkout_price">48.000.000 đ</div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="no-border">
                                        Phí shipping
                                    </td>
                                    <td class="no-border text-right">
                                        0đ
                                    </td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td>
                                        Tổng
                                    </td>
                                    <td class="text-right">
                                        <div class="checkout_price"><b>48.000.000 đ</b></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        *Giá trên đã bao gồm thuế VAT
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


        </div>
        <div class="col-sm-3">
            <?php get_sidebar('cart'); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>

