<?php
/**
 * Template Name: Cart Empty
 */
?>
<?php get_header(); ?>
<div class="breadcrumb hidden-xs">
    <a href="#">HOME</a> &nbsp; &nbsp; > &nbsp; &nbsp;
    <a href="#">Giỏ hàng</a>
</div>
<div class="row">
    <div class="container">
        <div class="col-sm-9">
            <div class="about_title">
                Giỏ hàng của bạn
            </div>
            <div class="shopping_cart_container">
                <div class="cart_empty_container">
                    <div class="cart_empty_image">
                        <img src="<?php bloginfo('template_url'); ?>/images/cart_empty.png"
                             class="img-responsive center-block" alt="Cart Empty"/>
                    </div>
                    <div class="cart_empty_note text-center">
                        Không có sản phẩm nào trong giỏ hàng của bạn.
                    </div>
                    <a href="/" class="cart_empty_button center-block">
                        TIẾP TỤC MUA SẮM
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-3 hidden-xs">
            <?php get_sidebar('cart'); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
