<?php
show_admin_bar(false);
add_image_size('274_153', 274, 153, true);
add_image_size('360_360', 360, 360, true);

load_theme_textdomain( 'havy', get_template_directory() . '/languages' );

register_nav_menus(
    array(
        'header-menu' => __( 'Header Menu' ),
    )
);

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'  => 'Cấu hình website',
        'menu_title'  => 'Cấu hình website',
        'menu_slug'   => 'website_config',
        'capability'  => 'edit_posts',
        'redirect'    => true,
        'position'    => 59
    ));

    acf_add_options_sub_page(array(
        'page_title'  => esc_html__( 'Cài đặt chung', 'piano' ),
        'menu_title'  => esc_html__( 'Cài đặt chung', 'piano' ),
        'parent_slug' => 'website_config',
    ));

    acf_add_options_sub_page(array(
        'page_title'  => esc_html__( 'Quản lý trang chủ', 'piano' ),
        'menu_title'  => esc_html__( 'Quản lý trang chủ', 'piano' ),
        'parent_slug' => 'website_config',
    ));

    acf_add_options_sub_page(array(
        'page_title'  => esc_html__( 'Quản lý mã Header, Footer', 'piano' ),
        'menu_title'  => esc_html__( 'Header, Footer Code', 'piano' ),
        'parent_slug' => 'website_config',
    ));
}

function add_woocommerce_support() {
    add_theme_support( 'woocommerce');
}
add_action('after_setup_theme', 'add_woocommerce_support');


function piano_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Footer Block 1', 'piano' ),
        'id'            => 'footer1',
        'description'   => __( 'Add footer1 here to appear in your sidebar.', 'piano' ),
        'before_widget' => '<div class="footer_box">',
        'after_widget'  => '</div></div>',
        'before_title'  => '<div class="footer_box_title">',
        'after_title'  => '</div><div class="footer_box_content">',

    ) );
    register_sidebar( array(
        'name'          => __( 'Footer Block 2', 'piano' ),
        'id'            => 'footer2',
        'description'   => __( 'Add footer1 here to appear in your sidebar.', 'piano' ),
        'before_widget' => '<div class="footer_box">',
        'after_widget'  => '</div></div>',
        'before_title'  => '<div class="footer_box_title">',
        'after_title'  => '</div><div class="footer_box_content">',

    ) );
    register_sidebar( array(
        'name'          => __( 'Footer Block 3', 'piano' ),
        'id'            => 'footer3',
        'description'   => __( 'Add footer1 here to appear in your sidebar.', 'piano' ),
        'before_widget' => '<div class="footer_box">',
        'after_widget'  => '</div></div>',
        'before_title'  => '<div class="footer_box_title">',
        'after_title'  => '</div><div class="footer_box_content">',

    ) );
    register_sidebar( array(
        'name'          => __( 'Footer Block 4', 'piano' ),
        'id'            => 'footer4',
        'description'   => __( 'Add footer1 here to appear in your sidebar.', 'piano' ),
        'before_widget' => '<div class="footer_box">',
        'after_widget'  => '</div></div>',
        'before_title'  => '<div class="footer_box_title">',
        'after_title'  => '</div><div class="footer_box_content">',

    ) );
    register_sidebar( array(
        'name'          => __( 'Footer Block 5', 'piano' ),
        'id'            => 'footer5',
        'description'   => __( 'Add footer1 here to appear in your sidebar.', 'piano' ),
        'before_widget' => '<div class="footer_box">',
        'after_widget'  => '</div></div>',
        'before_title'  => '<div class="footer_box_title">',
        'after_title'  => '</div><div class="footer_box_content">',

    ) );
}
add_action( 'widgets_init', 'piano_widgets_init' );


function custom_override_checkout_fields( $fields ) {
    $fields['billing']['billing_first_name']['required'] = false;
    $fields['billing']['billing_last_name']['required'] = false;
    $fields['billing']['billing_phone']['required'] = false;
    $fields['billing']['billing_address_1']['required'] =  false;
    $fields['billing']['billing_email']['required'] = false;
    $fields['billing']['billing_city']['required'] = false;
    $fields['billing']['billing_address_2']['label'] = 'Quận Huyện';
  

    $newFields = array();
    $newFields['billing']['billing_last_name'] = $fields['billing']['billing_last_name'];
    $newFields['billing']['billing_phone'] = $fields['billing']['billing_phone'];
    $newFields['billing']['billing_email'] = $fields['billing']['billing_email'];
    $newFields['billing']['billing_city'] = $fields['billing']['billing_city'];
    $newFields['billing']['billing_address_2'] = $fields['billing']['billing_address_2'];
    $newFields['billing']['billing_address_1'] = $fields['billing']['billing_address_1'];
    unset($newFields['billing']['billing_state']['country_field']);

    $fields['billing'] = $newFields['billing'];
    return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );


// function to display number of posts.
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

// function to count views.
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

// Lấy sản phẩm đề xuất
function getSuggestProductBy($termKey, $termValue, $limit = 8)
{
    $suggestProductArgs = array(
        'post_type' => 'product',
        'posts_per_page' => $limit,
        $termKey => $termValue,
        'orderby'   => 'rand'
    );
    $suggestProduct = new WP_Query( $suggestProductArgs );

    return $suggestProduct;
}

// Lấy sản phẩm phổ biến (order theo view)
function getPopularProductBy($termKey, $termValue, $limit = 8)
{
    $popularProductArgs = array(
        'post_type' => 'product',
        'posts_per_page' => $limit,
        $termKey => $termValue,
        'orderby'   => 'meta_value_num',
        'meta_key'  => 'post_views_count',
        'order' => 'DESC'
    );
    $popularProduct = new WP_Query( $popularProductArgs );

    return $popularProduct;
}


// lấy sản phẩm đang giảm giá
function getSaleOffProductBy($termKey, $termValue, $limit = 8)
{
    $saleOffProductArgs = array(
        'post_type' => 'product',
        'posts_per_page' => $limit,
        $termKey => $termValue,
        'meta_query'     => array(
            'relation' => 'OR',
            array( // Simple products type
                'key'           => '_sale_price',
                'value'         => 0,
                'compare'       => '>',
                'type'          => 'numeric'
            ),
            array( // Variable products type
                'key'           => '_min_variation_sale_price',
                'value'         => 0,
                'compare'       => '>',
                'type'          => 'numeric'
            )
        ),
        'orderby' => 'ID',
        'order' => 'DESC'
    );
    $saleOffProduct = new WP_Query($saleOffProductArgs);
    return $saleOffProduct;
}


// lấy sản phẩm mới
function getNewProductBy($termKey, $termValue, $limit = 8)
{
    $newProductArgs = array(
        'post_type' => 'product',
        'posts_per_page' => $limit,
        $termKey => $termValue,
        'orderby' => 'ID',
        'order' => 'DESC'
    );
    $newProduct = new WP_Query( $newProductArgs );

    return $newProduct;
}


function getBestSaleProductBy($termKey, $termValue, $limit = 8)
{
    $bestSaleProductArgs = array(
        'post_type' => 'product',
        'posts_per_page' => $limit,
        $termKey => $termValue,
        'meta_key' => 'total_sales',
        'orderby' => 'meta_value_num',
        'order' => 'DESC'
    );
    $bestSaleProduct = new WP_Query( $bestSaleProductArgs );

    return $bestSaleProduct;
}

// lấy sản phẩm đề xuất (gợi ý)
function getSuggestProductByid($product_id,$limit = 9){
    $suggestProductArgs = array(
        'post_type' => 'product',
        'posts_per_page' => $limit,
        'post__in' => $product_id,
        'orderby'   => 'rand'
    );
    $suggestProduct = new WP_Query( $suggestProductArgs );

    return $suggestProduct;

}


add_action( 'wp_ajax_nopriv_product', 'ajax_product_handler' );
add_action( 'wp_ajax_product', 'ajax_product_handler' );

function ajax_product_handler()
{
    $post = $_POST;
    $postPerPage = 12;
    if(!empty($post['post_per_page']) && is_numeric($post['post_per_page']) && $post['post_per_page'] < 48){
        $postPerPage = $post['post_per_page'];
    }
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => $postPerPage,
    );
    $metaQuery = array();
    if(!empty($post['price_from'])){
        $metaQuery[] = array(
            'key' => '_price',
            'value' => $post['price_from'],
            'compare' => '>',
            'type' => 'NUMERIC'
        );
    }
    if(!empty($post['price_to'])){
        $metaQuery[] = array(
            'key' => '_price',
            'value' => $post['price_to'],
            'compare' => '<=',
            'type' => 'NUMERIC'
        );
    }
    if(!empty($metaQuery)){
        $args['meta_query'] = $metaQuery;
    }
    if(!empty($post['brand'])){
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'pwb-brand',
                'field' => 'slug',
                'terms' => array_keys($post['brand']),
                'operator' => 'IN'
            )
        );
    }

    if(!empty($post['stock_status'])){
        $args['meta_query'] =  array(
            array(
                'key' => '_stock_status',
                'value' => $post['stock_status']
            ),

        );
    }


    $product = new WP_Query($args);

    $brandCount = countProductByBrand($post, $args);

    $priceCount = countProductByPrice($post, $args);


    echo json_encode(array(
        'success' => true, 'message' => 'Thành công',
        'product' => renderProduct($product),
        'count' => array(
            'brand' => $brandCount,
            'price' => $priceCount,
        )
    ));
    die;
}

// if(isset($_REQUEST['action']) && $_REQUEST['action']=='product'):
//         do_action( 'wp_ajax_' . $_REQUEST['action'] );
//         do_action( 'wp_ajax_nopriv_' . $_REQUEST['action'] );
// endif;

function countProductByPrice($post, $args)
{
    $count = array();
    $priceRange = array(
        array('from' => '0', 'to' => '10000000'),
        array('from' => '10000000', 'to' => '20000000'),
        array('from' => '20000000', 'to' => '50000000'),
        array('from' => '50000000', 'to' => '100000000'),
        array('from' => '100000000', 'to' => '200000000'),
        array('from' => '200000000', 'to' => '500000000'),
        array('from' => '500000000', 'to' => '0')
    );
    if(!empty($priceRange)) {
        foreach ($priceRange as $key => $value) {
            if(!empty($post['price_from']) || !empty($post['price_to'])){
                if($post['price_from'] == $value['from'] && $post['price_to'] == $value['to']){
                    $args['post_type'] = 'product';
                    $args['post_per_page'] = -1;
                    $args['meta_query'] = array(
                        array(
                            'key' => '_price',
                            'value' => $value['from'],
                            'compare' => '>',
                            'type' => 'NUMERIC'
                        ),
                        array(
                            'key' => '_price',
                            'value' => $value['to'] != 0 ? $value['to'] : 10000000000,
                            'compare' => '<=',
                            'type' => 'NUMERIC'
                        )
                    );
                    $listProduct = new WP_Query($args);
                    $count[$value['from'].'_'.$value['to']] = $listProduct->post_count;
                }else{
                    $count[$value['from'].'_'.$value['to']] = 0;
                }
            }else{
                $args['post_type'] = 'product';
                $args['post_per_page'] = -1;
                $args['meta_query'] = array(
                    array(
                        'key' => '_price',
                        'value' => $value['from'],
                        'compare' => '>',
                        'type' => 'NUMERIC'
                    ),
                    array(
                        'key' => '_price',
                        'value' => $value['to'] != 0 ? $value['to'] : 10000000000,
                        'compare' => '<=',
                        'type' => 'NUMERIC'
                    )
                );
                $listProduct = new WP_Query($args);
                $count[$value['from'].'_'.$value['to']] = $listProduct->post_count;
            }
        }
    }

    return $count;
}


function countProductByBrand($post, $args)
{
    $count = array();
    $brand = get_terms(array('taxonomy' => 'pwb-brand', 'hide_empty' => false));
    if(!empty($brand)) {
        foreach ($brand as $key => $value) {
            if(!empty($value->slug)){
                if(!empty($post['brand'])){
                    if(in_array($value->slug, array_keys($post['brand']))){
                        $args['post_type'] = 'product';
                        $args['post_per_page'] = -1;
                        $args['tax_query'] = array(
                            array(
                                'taxonomy' => 'pwb-brand',
                                'field' => 'slug',
                                'terms' => [$value->slug],
                                'operator' => 'IN'
                            )
                        );
                        $listProduct = new WP_Query($args);
                        $count[$value->slug] = $listProduct->post_count;
                    }else{
                        $count[$value->slug] = 0;
                    }
                }else{
                    $args['post_type'] = 'product';
                    $args['post_per_page'] = -1;
                    $args['tax_query'] = array(
                        array(
                            'taxonomy' => 'pwb-brand',
                            'field' => 'slug',
                            'terms' => [$value->slug],
                            'operator' => 'IN'
                        )
                    );
                    $listProduct = new WP_Query($args);
                    $count[$value->slug] = $listProduct->post_count;
                }
            }
        }
    }
    return $count;
}


function renderProduct($product)
{
    $html = '';
    if($product->have_posts()){
        while($product->have_posts()){
            $product->the_post();
            $_product = wc_get_product(get_the_ID());
            if(!empty($product)){
                // if(method_exists($_product, 'get_price')){
                //     $price = $_product->get_price() ? number_format($_product->get_price()).'đ' : 'Liên hệ';
                // }else{
                //     $price = 'Liên hệ';
                // }
                $price = $_product->get_price() ? number_format($_product->get_price()).'đ' : 'Liên hệ';
                $star = '';
                if(function_exists('get_field')){
                    $numberStar = get_field('rating', $_product);
                    if ($numberStar > 0) {
                        for ($star = 0; $star < $numberStar; $star++) {
                            $star .= '<img src="'.get_bloginfo('template_url').'/images/star.png'.'">';
                        }
                    } else {
                        $star = '<img src="'.get_bloginfo('template_url').'/images/star.png"><img src="'.get_bloginfo('template_url').'/images/star.png"><img src="'.get_bloginfo('template_url').'/images/star.png"><img src="'.get_bloginfo('template_url').'/images/star.png"><img src="'.get_bloginfo('template_url').'/images/star.png">';
                       
                    }
                }

                  $checkstatus = $_product->get_stock_quantity();
                            if($checkstatus > 0){
                                $stock =  "Còn hàng";
                            }else{
                                $stock =   "Hết hàng";
                            }

                $html .= '<div class="product_box col-sm-3 col-xs-6">
                    <div class="product_image">
                        <a href="'.get_the_permalink().'">
                            <img src="'.get_the_post_thumbnail_url().'" class="img-responsive" alt="'.get_the_title().'">
                        </a>
                    </div>
                    <div class="product_name">
                        <a href="'.get_the_permalink().'">
                            '.get_the_title().'
                        </a>
                    </div>
                    <div class="product_rating">'.$star.'</div>
                     <div class="stock_status hidden-lg visible-xs">
                      '.$stock.'
                    </div>
                    <div class="product_price">
                        '.$price.'
                    </div>
                </div>';
            }
        }
    }
    return $html;
}

// add walker to menu
class nav_menu_walker extends Walker_Nav_Menu {

    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = str_repeat( $t, $depth );

        // Default class.
        $classes = array( 'sub-menu' );

        /**
         * Filters the CSS class(es) applied to a menu list element.
         *
         * @since 4.8.0
         *
         * @param array    $classes The CSS classes that are applied to the menu `<ul>` element.
         * @param stdClass $args    An object of `wp_nav_menu()` arguments.
         * @param int      $depth   Depth of menu item. Used for padding.
         */
        $class_names = join( ' ', apply_filters( 'nav_menu_submenu_css_class', $classes, $args, $depth ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        $output .= "{$n}{$indent}<ul$class_names>{$n}";
        $output .= "<div class='clearfix sub_menu_container'>";
    }

    public function end_lvl( &$output, $depth = 0, $args = array() )
    {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = str_repeat( $t, $depth );
        $output .= "</div>";
        $output .= "$indent</ul>{$n}";
    }


    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 )
    {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        /**
         * Filters the arguments for a single nav menu item.
         *
         * @since 4.4.0
         *
         * @param stdClass $args  An object of wp_nav_menu() arguments.
         * @param WP_Post  $item  Menu item data object.
         * @param int      $depth Depth of menu item. Used for padding.
         */
        $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

        /**
         * Filters the CSS class(es) applied to a menu item's list item element.
         *
         * @since 3.0.0
         * @since 4.1.0 The `$depth` parameter was added.
         *
         * @param array    $classes The CSS classes that are applied to the menu item's `<li>` element.
         * @param WP_Post  $item    The current menu item.
         * @param stdClass $args    An object of wp_nav_menu() arguments.
         * @param int      $depth   Depth of menu item. Used for padding.
         */
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        /**
         * Filters the ID applied to a menu item's list item element.
         *
         * @since 3.0.1
         * @since 4.1.0 The `$depth` parameter was added.
         *
         * @param string   $menu_id The ID that is applied to the menu item's `<li>` element.
         * @param WP_Post  $item    The current menu item.
         * @param stdClass $args    An object of wp_nav_menu() arguments.
         * @param int      $depth   Depth of menu item. Used for padding.
         */
        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

        $output .= $indent . '<li' . $id . $class_names .'>';

        $atts = array();
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
        $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
        $atts['href']   = ! empty( $item->url )        ? $item->url        : '';

        /**
         * Filters the HTML attributes applied to a menu item's anchor element.
         *
         * @since 3.6.0
         * @since 4.1.0 The `$depth` parameter was added.
         *
         * @param array $atts {
         *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
         *
         *     @type string $title  Title attribute.
         *     @type string $target Target attribute.
         *     @type string $rel    The rel attribute.
         *     @type string $href   The href attribute.
         * }
         * @param WP_Post  $item  The current menu item.
         * @param stdClass $args  An object of wp_nav_menu() arguments.
         * @param int      $depth Depth of menu item. Used for padding.
         */
        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        /** This filter is documented in wp-includes/post-template.php */
        $title = apply_filters( 'the_title', $item->title, $item->ID );

        /**
         * Filters a menu item's title.
         *
         * @since 4.4.0
         *
         * @param string   $title The menu item's title.
         * @param WP_Post  $item  The current menu item.
         * @param stdClass $args  An object of wp_nav_menu() arguments.
         * @param int      $depth Depth of menu item. Used for padding.
         */
        $title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before . $title . $args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;

        /**
         * Filters a menu item's starting output.
         *
         * The menu item's starting output only includes `$args->before`, the opening `<a>`,
         * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
         * no filter for modifying the opening and closing `<li>` for a menu item.
         *
         * @since 3.0.0
         *
         * @param string   $item_output The menu item's starting HTML output.
         * @param WP_Post  $item        Menu item data object.
         * @param int      $depth       Depth of menu item. Used for padding.
         * @param stdClass $args        An object of wp_nav_menu() arguments.
         */
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    public function end_el( &$output, $item, $depth = 0, $args = array(), $id = 0 )
    {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $output .= "</li>{$n}";
    }
}

add_filter('woocommerce_order_item_name', 'woo_order_item_with_link', 10, 3);
function woo_order_item_with_link( $item_name, $item, $bool ) {
    $url = get_permalink( $item['product_id'] ) ;
    return '<a href="'. $url .'">'. $item_name .'</a>';
}
//
//if( function_exists('acf_add_local_field_group') ):
//
//    acf_add_local_field_group(array (
//        'key' => 'group_5afee261d5dfd',
//        'title' => 'About Us',
//        'fields' => array (
//            array (
//                'key' => 'field_5b04f6389d0c5',
//                'label' => 'Nội dung mô tả',
//                'name' => 'description_text',
//                'type' => 'wysiwyg',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'tabs' => 'all',
//                'toolbar' => 'full',
//                'media_upload' => 1,
//                'delay' => 0,
//            ),
//            array (
//                'key' => 'field_5b04f64f9d0c6',
//                'label' => 'Ảnh mô tả',
//                'name' => 'description_image',
//                'type' => 'image',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'return_format' => 'url',
//                'preview_size' => 'thumbnail',
//                'library' => 'all',
//                'min_width' => '',
//                'min_height' => '',
//                'min_size' => '',
//                'max_width' => '',
//                'max_height' => '',
//                'max_size' => '',
//                'mime_types' => '',
//            ),
//            array (
//                'key' => 'field_5b0516579d0c7',
//                'label' => 'Danh sách hình ảnh',
//                'name' => 'gallery',
//                'type' => 'gallery',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'min' => '',
//                'max' => '',
//                'insert' => 'append',
//                'library' => 'all',
//                'min_width' => '',
//                'min_height' => '',
//                'min_size' => '',
//                'max_width' => '',
//                'max_height' => '',
//                'max_size' => '',
//                'mime_types' => '',
//            ),
//        ),
//        'location' => array (
//            array (
//                array (
//                    'param' => 'post_template',
//                    'operator' => '==',
//                    'value' => 'templates/about_us.php',
//                ),
//            ),
//        ),
//        'menu_order' => 0,
//        'position' => 'normal',
//        'style' => 'default',
//        'label_placement' => 'top',
//        'instruction_placement' => 'label',
//        'hide_on_screen' => '',
//        'active' => 1,
//        'description' => '',
//    ));
//
//    acf_add_local_field_group(array (
//        'key' => 'group_5b05a4e1ad9ec',
//        'title' => 'Brand',
//        'fields' => array (
//            array (
//                'key' => 'field_5b0617e4b9918',
//                'label' => 'Category',
//                'name' => 'category',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b0617f0b9919',
//                        'label' => 'Category',
//                        'name' => 'category',
//                        'type' => 'taxonomy',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'taxonomy' => 'product_cat',
//                        'field_type' => 'select',
//                        'allow_null' => 0,
//                        'add_term' => 1,
//                        'save_terms' => 0,
//                        'load_terms' => 0,
//                        'return_format' => 'object',
//                        'multiple' => 0,
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b062031c535a',
//                'label' => '#1 Best Sale',
//                'name' => 'best_sale',
//                'type' => 'post_object',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'post_type' => array (
//                    0 => 'product',
//                ),
//                'taxonomy' => array (
//                ),
//                'allow_null' => 0,
//                'multiple' => 0,
//                'return_format' => 'object',
//                'ui' => 1,
//            ),
//            array (
//                'key' => 'field_5b06205ac535b',
//                'label' => 'Số lượng sản phẩm được bán',
//                'name' => 'number_sale',
//                'type' => 'text',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'prepend' => '',
//                'append' => '',
//                'maxlength' => '',
//            ),
//        ),
//        'location' => array (
//            array (
//                array (
//                    'param' => 'taxonomy',
//                    'operator' => '==',
//                    'value' => 'pwb-brand',
//                ),
//            ),
//        ),
//        'menu_order' => 0,
//        'position' => 'normal',
//        'style' => 'default',
//        'label_placement' => 'top',
//        'instruction_placement' => 'label',
//        'hide_on_screen' => '',
//        'active' => 1,
//        'description' => '',
//    ));
//
//    acf_add_local_field_group(array (
//        'key' => 'group_5afdb22b60ccd',
//        'title' => 'Cài đặt trang chủ',
//        'fields' => array (
//            array (
//                'key' => 'field_5b0275f93af50',
//                'label' => 'Cấu hình trang chủ',
//                'name' => '',
//                'type' => 'tab',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'placement' => 'top',
//                'endpoint' => 0,
//            ),
//            array (
//                'key' => 'field_5b02712113de2',
//                'label' => 'Chính sách',
//                'name' => 'policy',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b02713513de3',
//                        'label' => 'Icon',
//                        'name' => 'icon',
//                        'type' => 'image',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'return_format' => 'url',
//                        'preview_size' => 'thumbnail',
//                        'library' => 'all',
//                        'min_width' => '',
//                        'min_height' => '',
//                        'min_size' => '',
//                        'max_width' => '',
//                        'max_height' => '',
//                        'max_size' => '',
//                        'mime_types' => '',
//                    ),
//                    array (
//                        'key' => 'field_5b02714713de4',
//                        'label' => 'Tên',
//                        'name' => 'name',
//                        'type' => 'text',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'default_value' => '',
//                        'placeholder' => '',
//                        'prepend' => '',
//                        'append' => '',
//                        'maxlength' => '',
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b36f80ba405a',
//                'label' => 'Cam kết chính sách',
//                'name' => 'commit_policy',
//                'type' => 'wysiwyg',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'tabs' => 'all',
//                'toolbar' => 'full',
//                'media_upload' => 1,
//                'delay' => 0,
//            ),
//            array (
//                'key' => 'field_5b02714f13de5',
//                'label' => 'Slider',
//                'name' => 'slider',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b02716313de7',
//                        'label' => 'Ảnh',
//                        'name' => 'image',
//                        'type' => 'image',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'return_format' => 'url',
//                        'preview_size' => 'thumbnail',
//                        'library' => 'all',
//                        'min_width' => '',
//                        'min_height' => '',
//                        'min_size' => '',
//                        'max_width' => '',
//                        'max_height' => '',
//                        'max_size' => '',
//                        'mime_types' => '',
//                    ),
//                    array (
//                        'key' => 'field_5b02717113de8',
//                        'label' => 'Link',
//                        'name' => 'link',
//                        'type' => 'text',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'default_value' => '',
//                        'placeholder' => '',
//                        'prepend' => '',
//                        'append' => '',
//                        'maxlength' => '',
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b02735fd135b',
//                'label' => 'Danh mục sản phẩm',
//                'name' => 'category',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b027387d135c',
//                        'label' => 'Product Category',
//                        'name' => 'product_category',
//                        'type' => 'taxonomy',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'taxonomy' => 'product_cat',
//                        'field_type' => 'select',
//                        'allow_null' => 0,
//                        'add_term' => 1,
//                        'save_terms' => 0,
//                        'load_terms' => 0,
//                        'return_format' => 'object',
//                        'multiple' => 0,
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b0273a1d135d',
//                'label' => 'Thương hiệu',
//                'name' => 'brand',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b0273e2d135e',
//                        'label' => 'Chọn thương hiệu',
//                        'name' => 'item',
//                        'type' => 'taxonomy',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'taxonomy' => 'pwb-brand',
//                        'field_type' => 'select',
//                        'allow_null' => 0,
//                        'add_term' => 1,
//                        'save_terms' => 0,
//                        'load_terms' => 0,
//                        'return_format' => 'object',
//                        'multiple' => 0,
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b02741ad1360',
//                'label' => 'Hàng mới về',
//                'name' => 'new_product',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b02741ad1361',
//                        'label' => 'Chọn sản phẩm',
//                        'name' => 'product',
//                        'type' => 'post_object',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'post_type' => array (
//                            0 => 'product',
//                        ),
//                        'taxonomy' => array (
//                        ),
//                        'allow_null' => 0,
//                        'multiple' => 0,
//                        'return_format' => 'object',
//                        'ui' => 1,
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b027456d1362',
//                'label' => 'Sản phẩm giảm giá',
//                'name' => 'saleoff',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b027456d1363',
//                        'label' => 'Chọn sản phẩm',
//                        'name' => 'product',
//                        'type' => 'post_object',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'post_type' => array (
//                            0 => 'product',
//                        ),
//                        'taxonomy' => array (
//                        ),
//                        'allow_null' => 0,
//                        'multiple' => 0,
//                        'return_format' => 'object',
//                        'ui' => 1,
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b027467d1364',
//                'label' => 'Tư vấn trực tuyến',
//                'name' => 'online_guide',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b027497d1365',
//                        'label' => 'Bài viết',
//                        'name' => 'item',
//                        'type' => 'post_object',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'post_type' => array (
//                            0 => 'post',
//                        ),
//                        'taxonomy' => array (
//                            0 => 'category:huong-dan',
//                        ),
//                        'allow_null' => 0,
//                        'multiple' => 0,
//                        'return_format' => '',
//                        'ui' => 1,
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b0274afd1366',
//                'label' => 'Tin tức',
//                'name' => 'news',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b0274afd1367',
//                        'label' => 'Bài viết',
//                        'name' => 'item',
//                        'type' => 'post_object',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'post_type' => array (
//                            0 => 'post',
//                        ),
//                        'taxonomy' => array (
//                            0 => 'category:tin-tuc',
//                        ),
//                        'allow_null' => 0,
//                        'multiple' => 0,
//                        'return_format' => 'object',
//                        'ui' => 1,
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b053acff7c96',
//                'label' => 'Sản phẩm bán chạy',
//                'name' => '',
//                'type' => 'tab',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'placement' => 'top',
//                'endpoint' => 0,
//            ),
//            array (
//                'key' => 'field_5b053a44f7c94',
//                'label' => 'Sản phẩm bán chạy',
//                'name' => 'bestsale_product',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b053a56f7c95',
//                        'label' => 'Sản phẩm bán chạy',
//                        'name' => 'item',
//                        'type' => 'post_object',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'post_type' => array (
//                            0 => 'product',
//                        ),
//                        'taxonomy' => array (
//                        ),
//                        'allow_null' => 0,
//                        'multiple' => 0,
//                        'return_format' => 'object',
//                        'ui' => 1,
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b0275103af44',
//                'label' => 'Sản phẩm nổi bật',
//                'name' => '',
//                'type' => 'tab',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'placement' => 'top',
//                'endpoint' => 0,
//            ),
//            array (
//                'key' => 'field_5b0275393af45',
//                'label' => 'Được đề xuất',
//                'name' => 'suggest_product',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b02756c3af46',
//                        'label' => 'Sản phẩm',
//                        'name' => 'product',
//                        'type' => 'post_object',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'post_type' => array (
//                            0 => 'product',
//                        ),
//                        'taxonomy' => array (
//                        ),
//                        'allow_null' => 0,
//                        'multiple' => 0,
//                        'return_format' => '',
//                        'ui' => 1,
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b02757e3af47',
//                'label' => 'Bán chạy',
//                'name' => 'best_sale_product',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b02757e3af48',
//                        'label' => 'Sản phẩm',
//                        'name' => 'product',
//                        'type' => 'post_object',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'post_type' => array (
//                            0 => 'product',
//                        ),
//                        'taxonomy' => array (
//                        ),
//                        'allow_null' => 0,
//                        'multiple' => 0,
//                        'return_format' => '',
//                        'ui' => 1,
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b0275913af49',
//                'label' => 'Phổ biến',
//                'name' => 'common_product',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b0275913af4a',
//                        'label' => 'Sản phẩm',
//                        'name' => 'product',
//                        'type' => 'post_object',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'post_type' => array (
//                            0 => 'product',
//                        ),
//                        'taxonomy' => array (
//                        ),
//                        'allow_null' => 0,
//                        'multiple' => 0,
//                        'return_format' => '',
//                        'ui' => 1,
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b0275ad3af4b',
//                'label' => 'Mới về',
//                'name' => 'new_arrival',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b0275ad3af4c',
//                        'label' => 'Sản phẩm',
//                        'name' => 'product',
//                        'type' => 'post_object',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'post_type' => array (
//                            0 => 'product',
//                        ),
//                        'taxonomy' => array (
//                        ),
//                        'allow_null' => 0,
//                        'multiple' => 0,
//                        'return_format' => '',
//                        'ui' => 1,
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b0275dc3af4e',
//                'label' => 'Đang giảm giá',
//                'name' => 'feature_product_saleoff',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b0275dc3af4f',
//                        'label' => 'Sản phẩm',
//                        'name' => 'product',
//                        'type' => 'post_object',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'post_type' => array (
//                            0 => 'product',
//                        ),
//                        'taxonomy' => array (
//                        ),
//                        'allow_null' => 0,
//                        'multiple' => 0,
//                        'return_format' => 'object',
//                        'ui' => 1,
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b398dde1d6f5',
//                'label' => 'Index Responsive content',
//                'name' => '',
//                'type' => 'tab',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'placement' => 'top',
//                'endpoint' => 0,
//            ),
//            array (
//                'key' => 'field_5b398df01d6f6',
//                'label' => 'Quick link',
//                'name' => 'quick_link',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 4,
//                'layout' => 'table',
//                'button_label' => 'Add Quick Link',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b398e001d6f7',
//                        'label' => 'Icon',
//                        'name' => 'icon_link',
//                        'type' => 'image',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'return_format' => 'url',
//                        'preview_size' => 'thumbnail',
//                        'library' => 'all',
//                        'min_width' => '',
//                        'min_height' => '',
//                        'min_size' => '',
//                        'max_width' => '',
//                        'max_height' => '',
//                        'max_size' => '',
//                        'mime_types' => '',
//                    ),
//                    array (
//                        'key' => 'field_5b398e131d6f8',
//                        'label' => 'Href',
//                        'name' => 'href_quick_link',
//                        'type' => 'text',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'default_value' => '',
//                        'placeholder' => '',
//                        'prepend' => '',
//                        'append' => '',
//                        'maxlength' => '',
//                    ),
//                    array (
//                        'key' => 'field_5b398e1c1d6f9',
//                        'label' => 'Title',
//                        'name' => 'title_quick_link',
//                        'type' => 'text',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'default_value' => '',
//                        'placeholder' => '',
//                        'prepend' => '',
//                        'append' => '',
//                        'maxlength' => '',
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b39a50985f5c',
//                'label' => 'logo header menu',
//                'name' => 'logo_header_menu',
//                'type' => 'image',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'return_format' => 'url',
//                'preview_size' => 'thumbnail',
//                'library' => 'all',
//                'min_width' => '',
//                'min_height' => '',
//                'min_size' => '',
//                'max_width' => '',
//                'max_height' => '',
//                'max_size' => '',
//                'mime_types' => '',
//            ),
//        ),
//        'location' => array (
//            array (
//                array (
//                    'param' => 'options_page',
//                    'operator' => '==',
//                    'value' => 'acf-options-quan-ly-trang-chu',
//                ),
//            ),
//        ),
//        'menu_order' => 0,
//        'position' => 'normal',
//        'style' => 'default',
//        'label_placement' => 'top',
//        'instruction_placement' => 'label',
//        'hide_on_screen' => '',
//        'active' => 1,
//        'description' => '',
//    ));
//
//    acf_add_local_field_group(array (
//        'key' => 'group_5afa98e504821',
//        'title' => 'Cấu hình chung',
//        'fields' => array (
//            array (
//                'key' => 'field_5b36e4f7c355a',
//                'label' => 'Cài đặt chung',
//                'name' => '',
//                'type' => 'tab',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'placement' => 'top',
//                'endpoint' => 0,
//            ),
//            array (
//                'key' => 'field_5afa98ebf67a8',
//                'label' => 'Email',
//                'name' => 'email',
//                'type' => 'text',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'prepend' => '',
//                'append' => '',
//                'maxlength' => '',
//            ),
//            array (
//                'key' => 'field_5afa9913f67a9',
//                'label' => 'Số điện thoại',
//                'name' => 'phone',
//                'type' => 'text',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'prepend' => '',
//                'append' => '',
//                'maxlength' => '',
//            ),
//            array (
//                'key' => 'field_5b067c8fb0fb0',
//                'label' => 'Địa chỉ',
//                'name' => 'address',
//                'type' => 'text',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'prepend' => '',
//                'append' => '',
//                'maxlength' => '',
//            ),
//            array (
//                'key' => 'field_5afa991af67aa',
//                'label' => 'Hotline',
//                'name' => 'hotline',
//                'type' => 'text',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'prepend' => '',
//                'append' => '',
//                'maxlength' => '',
//            ),
//            array (
//                'key' => 'field_5afa991ef67ab',
//                'label' => 'Logo',
//                'name' => 'logo',
//                'type' => 'image',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'return_format' => 'url',
//                'preview_size' => 'thumbnail',
//                'library' => 'all',
//                'min_width' => '',
//                'min_height' => '',
//                'min_size' => '',
//                'max_width' => '',
//                'max_height' => '',
//                'max_size' => '',
//                'mime_types' => '',
//            ),
//            array (
//                'key' => 'field_5b02723579bcc',
//                'label' => 'Fanpage',
//                'name' => 'fanpage',
//                'type' => 'text',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'prepend' => '',
//                'append' => '',
//                'maxlength' => '',
//            ),
//            array (
//                'key' => 'field_5b02724579bcd',
//                'label' => 'Link Facebook',
//                'name' => 'link_facebook',
//                'type' => 'text',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'prepend' => '',
//                'append' => '',
//                'maxlength' => '',
//            ),
//            array (
//                'key' => 'field_5b02724c79bce',
//                'label' => 'Link Zalo',
//                'name' => 'link_zalo',
//                'type' => 'text',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'prepend' => '',
//                'append' => '',
//                'maxlength' => '',
//            ),
//            array (
//                'key' => 'field_5b02725379bcf',
//                'label' => 'Link Youtube',
//                'name' => 'link_youtube',
//                'type' => 'text',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'prepend' => '',
//                'append' => '',
//                'maxlength' => '',
//            ),
//            array (
//                'key' => 'field_5b02725979bd0',
//                'label' => 'Link Instagram',
//                'name' => 'link_instagram',
//                'type' => 'text',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'prepend' => '',
//                'append' => '',
//                'maxlength' => '',
//            ),
//            array (
//                'key' => 'field_5b02720879bc9',
//                'label' => 'Support',
//                'name' => 'support',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b02721879bca',
//                        'label' => 'Ảnh',
//                        'name' => 'image',
//                        'type' => 'image',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'return_format' => 'url',
//                        'preview_size' => 'thumbnail',
//                        'library' => 'all',
//                        'min_width' => '',
//                        'min_height' => '',
//                        'min_size' => '',
//                        'max_width' => '',
//                        'max_height' => '',
//                        'max_size' => '',
//                        'mime_types' => '',
//                    ),
//                    array (
//                        'key' => 'field_5b02722b79bcb',
//                        'label' => 'Link',
//                        'name' => 'link',
//                        'type' => 'text',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'default_value' => '',
//                        'placeholder' => '',
//                        'prepend' => '',
//                        'append' => '',
//                        'maxlength' => '',
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b36e509c355b',
//                'label' => 'Cart Settings',
//                'name' => '',
//                'type' => 'tab',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'placement' => 'top',
//                'endpoint' => 0,
//            ),
//            array (
//                'key' => 'field_5b36e53ac355c',
//                'label' => 'Cart checkout info',
//                'name' => 'cart_Info',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => 'Add more info',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b36e549c355d',
//                        'label' => 'Bank image',
//                        'name' => 'cart_Info_img',
//                        'type' => 'image',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'return_format' => 'url',
//                        'preview_size' => 'thumbnail',
//                        'library' => 'all',
//                        'min_width' => '',
//                        'min_height' => '',
//                        'min_size' => '',
//                        'max_width' => '',
//                        'max_height' => '',
//                        'max_size' => '',
//                        'mime_types' => '',
//                    ),
//                    array (
//                        'key' => 'field_5b36e55bc355e',
//                        'label' => 'Infomation bank',
//                        'name' => 'infomation_bank',
//                        'type' => 'wysiwyg',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'default_value' => '',
//                        'tabs' => 'all',
//                        'toolbar' => 'basic',
//                        'media_upload' => 1,
//                        'delay' => 0,
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b370479a5ccf',
//                'label' => 'footer content',
//                'name' => '',
//                'type' => 'tab',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'placement' => 'top',
//                'endpoint' => 0,
//            ),
//            array (
//                'key' => 'field_5b370493a5cd0',
//                'label' => 'logo footer',
//                'name' => 'logo_footer',
//                'type' => 'image',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'return_format' => 'url',
//                'preview_size' => 'thumbnail',
//                'library' => 'all',
//                'min_width' => '',
//                'min_height' => '',
//                'min_size' => '',
//                'max_width' => '',
//                'max_height' => '',
//                'max_size' => '',
//                'mime_types' => '',
//            ),
//            array (
//                'key' => 'field_5b3704aea5cd1',
//                'label' => 'footer company infomation',
//                'name' => 'footer_company_infomation',
//                'type' => 'wysiwyg',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'tabs' => 'all',
//                'toolbar' => 'full',
//                'media_upload' => 1,
//                'delay' => 0,
//            ),
//        ),
//        'location' => array (
//            array (
//                array (
//                    'param' => 'options_page',
//                    'operator' => '==',
//                    'value' => 'acf-options-cai-dat-chung',
//                ),
//            ),
//        ),
//        'menu_order' => 0,
//        'position' => 'normal',
//        'style' => 'default',
//        'label_placement' => 'top',
//        'instruction_placement' => 'label',
//        'hide_on_screen' => '',
//        'active' => 1,
//        'description' => '',
//    ));
//
//    acf_add_local_field_group(array (
//        'key' => 'group_5afe333968d80',
//        'title' => 'Danh mục sản phẩm',
//        'fields' => array (
//            array (
//                'key' => 'field_5b03914bf2eaf',
//                'label' => 'Thương hiệu',
//                'name' => 'brand',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b03915cf2eb0',
//                        'label' => 'item',
//                        'name' => 'item',
//                        'type' => 'taxonomy',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'taxonomy' => 'pwb-brand',
//                        'field_type' => 'select',
//                        'allow_null' => 0,
//                        'add_term' => 1,
//                        'save_terms' => 0,
//                        'load_terms' => 0,
//                        'return_format' => 'object',
//                        'multiple' => 0,
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b03957da5c38',
//                'label' => '#1 Best Sale',
//                'name' => 'top_best_sale',
//                'type' => 'post_object',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'post_type' => array (
//                    0 => 'product',
//                ),
//                'taxonomy' => array (
//                ),
//                'allow_null' => 0,
//                'multiple' => 0,
//                'return_format' => 'object',
//                'ui' => 1,
//            ),
//            array (
//                'key' => 'field_5b0396959aabb',
//                'label' => 'Số sản phẩm best sale đã bán',
//                'name' => 'number_sale_top',
//                'type' => 'text',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'prepend' => '',
//                'append' => '',
//                'maxlength' => '',
//            ),
//        ),
//        'location' => array (
//            array (
//                array (
//                    'param' => 'taxonomy',
//                    'operator' => '==',
//                    'value' => 'product_cat',
//                ),
//            ),
//        ),
//        'menu_order' => 0,
//        'position' => 'normal',
//        'style' => 'default',
//        'label_placement' => 'top',
//        'instruction_placement' => 'label',
//        'hide_on_screen' => '',
//        'active' => 1,
//        'description' => '',
//    ));
//
//    acf_add_local_field_group(array (
//        'key' => 'group_5afdafea85d93',
//        'title' => 'Header, Footer',
//        'fields' => array (
//            array (
//                'key' => 'field_5b026ee30566a',
//                'label' => 'Header',
//                'name' => 'header',
//                'type' => 'textarea',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'maxlength' => '',
//                'rows' => '',
//                'new_lines' => '',
//            ),
//            array (
//                'key' => 'field_5b026ee90566b',
//                'label' => 'Footer',
//                'name' => 'footer',
//                'type' => 'textarea',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'maxlength' => '',
//                'rows' => '',
//                'new_lines' => '',
//            ),
//        ),
//        'location' => array (
//            array (
//                array (
//                    'param' => 'options_page',
//                    'operator' => '==',
//                    'value' => 'acf-options-header-footer-code',
//                ),
//            ),
//        ),
//        'menu_order' => 0,
//        'position' => 'normal',
//        'style' => 'default',
//        'label_placement' => 'top',
//        'instruction_placement' => 'label',
//        'hide_on_screen' => '',
//        'active' => 1,
//        'description' => '',
//    ));
//
//    acf_add_local_field_group(array (
//        'key' => 'group_5afe31829b1c6',
//        'title' => 'Liên hệ',
//        'fields' => array (
//            array (
//                'key' => 'field_5b038f958071a',
//                'label' => 'Mô tả địa chỉ',
//                'name' => 'address_description',
//                'type' => 'textarea',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'maxlength' => '',
//                'rows' => '',
//                'new_lines' => 'wpautop',
//            ),
//            array (
//                'key' => 'field_5b038fe58071b',
//                'label' => 'Danh sách địa chỉ',
//                'name' => 'address',
//                'type' => 'wysiwyg',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'tabs' => 'all',
//                'toolbar' => 'full',
//                'media_upload' => 1,
//                'delay' => 0,
//            ),
//            array (
//                'key' => 'field_5b038ffa8071c',
//                'label' => 'Nhúng bản đồ',
//                'name' => 'map',
//                'type' => 'text',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'prepend' => '',
//                'append' => '',
//                'maxlength' => '',
//            ),
//        ),
//        'location' => array (
//            array (
//                array (
//                    'param' => 'post_template',
//                    'operator' => '==',
//                    'value' => 'templates/contact.php',
//                ),
//            ),
//        ),
//        'menu_order' => 0,
//        'position' => 'normal',
//        'style' => 'default',
//        'label_placement' => 'top',
//        'instruction_placement' => 'label',
//        'hide_on_screen' => '',
//        'active' => 1,
//        'description' => '',
//    ));
//
//    acf_add_local_field_group(array (
//        'key' => 'group_5afd8f3a40591',
//        'title' => 'Màu sắc',
//        'fields' => array (
//            array (
//                'key' => 'field_5b023e4618db0',
//                'label' => 'Chọn màu',
//                'name' => 'color',
//                'type' => 'color_picker',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//            ),
//        ),
//        'location' => array (
//            array (
//                array (
//                    'param' => 'taxonomy',
//                    'operator' => '==',
//                    'value' => 'pa_mau-sac',
//                ),
//            ),
//        ),
//        'menu_order' => 0,
//        'position' => 'normal',
//        'style' => 'default',
//        'label_placement' => 'top',
//        'instruction_placement' => 'label',
//        'hide_on_screen' => '',
//        'active' => 1,
//        'description' => '',
//    ));
//
//    acf_add_local_field_group(array (
//        'key' => 'group_5afd93969a6c0',
//        'title' => 'Sản phẩm',
//        'fields' => array (
//            array (
//                'key' => 'field_5b02429dddc31',
//                'label' => 'Mẫu âm thanh',
//                'name' => 'sound_model',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b0242b5ddc32',
//                        'label' => 'Tên mẫu',
//                        'name' => 'name',
//                        'type' => 'text',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'default_value' => '',
//                        'placeholder' => '',
//                        'prepend' => '',
//                        'append' => '',
//                        'maxlength' => '',
//                    ),
//                    array (
//                        'key' => 'field_5b05345098b1e',
//                        'label' => 'Độ dài',
//                        'name' => 'length',
//                        'type' => 'text',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'default_value' => '',
//                        'placeholder' => '',
//                        'prepend' => '',
//                        'append' => '',
//                        'maxlength' => '',
//                    ),
//                    array (
//                        'key' => 'field_5b0242bbddc33',
//                        'label' => 'File Mp3',
//                        'name' => 'file_mp3',
//                        'type' => 'file',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'return_format' => 'array',
//                        'library' => 'all',
//                        'min_size' => '',
//                        'max_size' => '',
//                        'mime_types' => '',
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b024f68df37f',
//                'label' => 'Thông tin khác',
//                'name' => 'other_info',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b024f92df380',
//                        'label' => 'Tên',
//                        'name' => 'name',
//                        'type' => 'text',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'default_value' => '',
//                        'placeholder' => '',
//                        'prepend' => '',
//                        'append' => '',
//                        'maxlength' => '',
//                    ),
//                    array (
//                        'key' => 'field_5b024f9bdf381',
//                        'label' => 'Giá trị',
//                        'name' => 'value',
//                        'type' => 'text',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'default_value' => '',
//                        'placeholder' => '',
//                        'prepend' => '',
//                        'append' => '',
//                        'maxlength' => '',
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b03c90daa9d5',
//                'label' => 'Ship',
//                'name' => 'ship',
//                'type' => 'text',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'prepend' => '',
//                'append' => '',
//                'maxlength' => '',
//            ),
//            array (
//                'key' => 'field_5b03c91daa9d6',
//                'label' => 'Bảo hành',
//                'name' => 'care',
//                'type' => 'text',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => '',
//                'placeholder' => '',
//                'prepend' => '',
//                'append' => '',
//                'maxlength' => '',
//            ),
//            array (
//                'key' => 'field_5b03c929aa9d7',
//                'label' => 'Quà tặng',
//                'name' => 'gift',
//                'type' => 'repeater',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'collapsed' => '',
//                'min' => 0,
//                'max' => 0,
//                'layout' => 'table',
//                'button_label' => '',
//                'sub_fields' => array (
//                    array (
//                        'key' => 'field_5b03c936aa9d8',
//                        'label' => 'Quà tặng',
//                        'name' => 'gift',
//                        'type' => 'text',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array (
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'default_value' => '',
//                        'placeholder' => '',
//                        'prepend' => '',
//                        'append' => '',
//                        'maxlength' => '',
//                    ),
//                ),
//            ),
//            array (
//                'key' => 'field_5b36db7cb5b3e',
//                'label' => 'Rating',
//                'name' => 'rating',
//                'type' => 'number',
//                'instructions' => '',
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => array (
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ),
//                'default_value' => 5,
//                'placeholder' => '',
//                'prepend' => '',
//                'append' => '',
//                'min' => 1,
//                'max' => 5,
//                'step' => '',
//            ),
//        ),
//        'location' => array (
//            array (
//                array (
//                    'param' => 'post_type',
//                    'operator' => '==',
//                    'value' => 'product',
//                ),
//            ),
//        ),
//        'menu_order' => 0,
//        'position' => 'normal',
//        'style' => 'default',
//        'label_placement' => 'top',
//        'instruction_placement' => 'label',
//        'hide_on_screen' => '',
//        'active' => 1,
//        'description' => '',
//    ));
//
//endif;

?>