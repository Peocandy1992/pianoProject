<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>


<div class="row">
    <div class="col-sm-7">
        <div class="about_title">
            Cảm ơn Quý khách đã đặt hàng tại Pianobt.vn!
        </div>
        <div class="order_result_container">
            <div class="form-group">
                Pianobt xác nhận đã nhận được đơn đặt hàng của Quý khách theo thông tin bên dưới.<br/>
                Đội hỗ trợ sẽ liên hệ với Quý khách sớm nhất có thể. Nếu Quý khách thắc mắc trong việc mua hàng, đừng ngần ngại liên lạc với chúng tôi.
            </div>
            <div class="cus_info_row">
                <div class="cus_title">ĐỊA CHỈ:</div>
                <div class="cus_content">
                    <?php echo get_field('address', 'option'); ?>
                </div>
            </div>
            <div class="cus_info_row">
                <span class="cus_title">EMAIL:</span>
                <span class="cus_content">
                    <?php echo get_field('email', 'option'); ?>
                </span>
            </div>
            <div class="cus_info_row">
                <span class="cus_title">SỐ ĐIỆN THOẠI:</span>
                <span class="cus_content">
                    <?php echo get_field('phone', 'option'); ?>
                </span>
            </div>

            <table class="table table-bordered table_order">
                <tr>
                    <td>23232</td>
                    <td>1x Thomann DP-31 B</td>
                    <td>Có sẵn hàng</td>
                    <td class="text-right">48.000.000đ</td>
                </tr>
                <tr>
                    <td colspan="3">Phí shipping</td>
                    <td class="text-right">0đ</td>
                </tr>
                <tr>
                    <td colspan="3">Tổng</td>
                    <td class="text-right">48.000.000đ</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="col-sm-5">
        <img src="<?php bloginfo('template_url'); ?>/images/thanks.png" class="img-responsive pull-right" alt="Thanks" />
        <div class="clearfix"></div>
    </div>
</div>

<div class="our_channer_container">
    <div class="row">
        <div class="col-sm-6">
            <div class="sidebar_box channel_box">
                <div class="channel_image">
                    <a href="<?php echo get_field('link_facebook', 'option'); ?>">
                        <img src="<?php bloginfo('template_url'); ?>/images/ic-facebook-big.png" class="img-responsive" alt="Facebook" />
                    </a>
                </div>
                <div class="channel_name">
                    <a href="<?php echo get_field('link_facebook'); ?>">
                        Fanpage
                    </a>
                </div>
                <div class="channel_description">Video, demo, hướng dẫn, các sự kiện đặc biệt và gặp gỡ những nghệ sĩ, những người bạn của Pianobt</div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="sidebar_box channel_box">
                <div class="channel_image">
                    <a href="<?php echo get_field('link_youtube', 'option'); ?>">
                        <img src="<?php bloginfo('template_url'); ?>/images/ic-youtube-big.png" class="img-responsive" alt="Facebook" />
                    </a>
                </div>
                <div class="channel_info">
                    <div class="channel_name">
                        <a href="<?php echo get_field('link_youtube', 'option'); ?>">
                            Youtube Channel
                        </a>
                    </div>
                    <div class="channel_description">Video, demo, hướng dẫn, các sự kiện đặc biệt và gặp gỡ những nghệ sĩ, những người bạn của Pianobt</div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php return; ?>
<div class="woocommerce-order">

	<?php if ( $order ) : ?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>

			<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); ?></p>

			<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

				<li class="woocommerce-order-overview__order order">
					<?php _e( 'Order number:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_order_number(); ?></strong>
				</li>

				<li class="woocommerce-order-overview__date date">
					<?php _e( 'Date:', 'woocommerce' ); ?>
					<strong><?php echo wc_format_datetime( $order->get_date_created() ); ?></strong>
				</li>

				<?php if ( is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) : ?>
					<li class="woocommerce-order-overview__email email">
						<?php _e( 'Email:', 'woocommerce' ); ?>
						<strong><?php echo $order->get_billing_email(); ?></strong>
					</li>
				<?php endif; ?>

				<li class="woocommerce-order-overview__total total">
					<?php _e( 'Total:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_formatted_order_total(); ?></strong>
				</li>

				<?php if ( $order->get_payment_method_title() ) : ?>
					<li class="woocommerce-order-overview__payment-method method">
						<?php _e( 'Payment method:', 'woocommerce' ); ?>
						<strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
					</li>
				<?php endif; ?>

			</ul>

		<?php endif; ?>

		<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>

	<?php endif; ?>

</div>
