<?php
/**
 * Cart totals
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-totals.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.6
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<div class="cart_total_container">
    <div class="row">
        <div class="col-sm-6 col-xs-6 hidden-xs">
            <a href="<?php echo home_url(); ?>" style="text-decoration: underline">Tiếp tục mua hàng</a>
        </div>
        <div class="col-sm-6 col-xs-6 text-right hidden-xs">
            <span class="s_total">Tổng: <?php wc_cart_totals_order_total_html(); ?> </span>
            <p>*Giá trên đã bao gồm VAT</p>
        </div>
        <div class="col-xs-12 visible-xs cart_total_responsive">
            <span class="s_total col-xs-4" style="padding: 0px">Tổng: </span> <div class="text-right col-xs-8" style="padding: 0px"> <?php wc_cart_totals_order_total_html(); ?></div>
            <div class="text-left" style="clear:left;font-family: Roboto-regular;font-size: 12px;line-height: 16px;color: #929292">*Giá trên đã bao gồm VAT</div>
        </div>
    </div>

    <a href="<?php echo esc_url( wc_get_checkout_url() );?>" class="btn_checkout hidden-xs" >
        Tiến hành Thanh toán
    </a>

    <a href="<?php echo esc_url( wc_get_checkout_url() );?>" class="btn_checkout btn_checkout_responsive visible-xs" >
        Thanh toán
    </a>
</div>