<?php get_header();  ?>
<div class="row product_category_page">

    <div class="sidebar col-sm-3">
        <?php get_template_part('templates/part/sidebar', 'support'); ?>

        <div class="sidebar_box">
            <div class="sidebar_title">Sản phẩm bán chạy</div>
            <div class="sidebar_content">
                <?php
                $brand = get_queried_object();
                $bestSale = getBestSaleProductBy('pwb-brand', $brand->slug);
                if (!empty($bestSale->posts)) {
                    foreach ($bestSale->posts as $key => $value) {
                        $productId = $value->ID;
                        $_product = wc_get_product($productId);
                        ?>
                        <div class="sidebar_product col-sm-12 col-xs-6 no-padding-lg">
                            <div class="row">
                                <div class="sidebar_product_image col-sm-4 no-padding-right">
                                    <a href="<?php echo get_permalink($productId); ?>">
                                        <img src="<?php bloginfo('template_url'); ?>/images/product.png"
                                             class="img-responsive" alt="<?php echo get_the_title($productId); ?>"/>
                                    </a>
                                </div>
                                <div class="sidebar_product_info col-sm-8">
                                    <div class="sidebar_product_name">
                                        <a href="<?php echo get_the_permalink($productId); ?>">
                                            <?php echo get_the_title($productId); ?>
                                        </a>
                                    </div>
                                    <div class="sidebar_product_price">
                                        <?php echo $_product->get_price() ? number_format($_product->get_price()) . 'đ' : 'Liên hệ'; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
                <div class="clearfix"></div>
            </div>
        </div>

        <?php get_template_part('templates/part/sidebar', 'fanpage'); ?>
    </div>
    <div class="col-sm-9">
        <div class="breadcrumb">
            <?php
            if (function_exists('bcn_display')) {
                bcn_display();
            }
            ?>
        </div>
        <div class="about_content">
            <div class="product_title_block">
                <h1>
                <?php
                $category = get_queried_object();
                echo $category->name;
                ?>
                </h1>
            </div>
            <div class="form-group">
                <?php
                $args = array(
                    'post_type' => 'product',
                    'posts_per_page' => 10
                );
                $listProduct = new WP_Query($args);

                ?>
                Có <?php echo $listProduct->post_count; ?> sản phẩm. Hiển thị sản phẩm
                1-<?php echo $listProduct->post_count > 12 ? 12 : $listProduct->post_count; ?>.
            </div>

            <div class="product_filter_block">
                <div class="row">
                    <div class="col-sm-2 col-xs-6">
                        <div class="form-group">
                            <select name="order_by" class="form-control" style="max-width: 120px">
                                <option value="newest">Mới nhất</option>
                                <option value="oldest">Cũ nhất</option>
                                <option value="price_highest">Giá cao nhất</option>
                                <option value="price_lowest">Giá thấp nhất</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-7 hidden-xs">
                        <div class="row show_filter_chose"></div>
                    </div>
                    <div class="col-sm-2 col-xs-6 pull-right">
                        <div class="row">
                            <div class="col-sm-6 col-xs-6 text-right lh34 no-padding-right">
                                <div class="form-group">
                                    Hiển thị
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <select name="post_per_page" class="post_per_page form-control" style="width: 50px;height: 36px;border-radius: 2px">
                                        <option value="4">4</option>
                                        <option value="8">8</option>
                                        <option value="12" selected>12</option>
                                        <option value="24">24</option>
                                        <option value="48">48</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="product_page_list">
                <div class="list_product_slide product_ajax_show">
                    <?php
                    if (have_posts()) {
                        while (have_posts()) {
                            the_post();
                            $_product = wc_get_product(get_the_ID());
                            ?>
                            <div class="product_box col-sm-3 col-xs-6">
                                <div class="product_image">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php the_post_thumbnail_url(); ?>" class="img-responsive"
                                             alt="<?php the_title(); ?>">
                                    </a>
                                </div>

                                <div class="product_rating hidden-lg visible-xs">
                                    <?php $numberStar = get_field('rating', $_product);
                                if($numberStar > 0){
                                for ($star = 0; $star < $numberStar; $star++) {
                                    ?>

                                    <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                <?php }
                                }else{ ?>
                                    <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                    <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                    <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                    <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                    <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                <?php
                                }
                                ?>
                                </div>

                                <div class="product_name">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_title(); ?>
                                    </a>

                                </div>

                                <div class="product_rating hidden-xs">
                                    <?php $numberStar = get_field('rating', $_product);
                                            if($numberStar > 0){
                                                for ($star = 0; $star < $numberStar; $star++) {
                                                    ?>
                                                    <!--                                    <i class='fa fa-star'></i>-->
                                                    <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <?php }
                                            }else{ ?>
                                                <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <?php
                                            }
                                            ?>
                                </div>


                                <div class="product_price">
                                    <?php echo $_product->get_price() ? number_format($_product->get_price()) . 'đ' : 'Liên hệ'; ?>
                                </div>
                            </div>
                            <?php
                        }
                    }

                    ?>

                </div>

                <div class="product_pagination">
                    <div class="col-sm-2 col-xs-6 pull-left" style="padding: 0px; margin-top: 30px;padding-left: 5px;clear: left;">
                        <div class="row">
                            <div class="col-sm-6 col-xs-6 text-right lh34 no-padding-right" style="width: 70px">
                                <div class="form-group text-left">
                                    Hiển thị
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-6" style="padding: 0px">
                                <div class="form-group">
                                    <select name="post_per_page" class="post_per_page form-control" style="width: 50px;height: 36px;border-radius: 2px;padding: 0px;">
                                        <option value="4">4</option>
                                        <option value="8">8</option>
                                        <option value="12" selected>12</option>
                                        <option value="24">24</option>
                                        <option value="48">48</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo wp_link_pages();

                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>


</div>
<?php get_footer(); ?>
