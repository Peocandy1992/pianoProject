<?php
/**
 * Template Name: Product
 */
?>
<?php get_header(); ?>
<div class="archive_page container">
    <div class="row">
        <form name="frm_filter_product" class="frm_filter_product" action="<?php echo admin_url('admin-ajax.php'); ?>">
            <div class="sidebar col-sm-3">
                <div class="sidebar_box">
                    <input type="hidden" name="action" value="product"/>
                    <div class="sidebar_title special_title">Danh mục</div>
                    <div class="sidebar_title" >Thương hiệu</div>
                    <div class="sidebar_content">
                        <div class="filter_brand">
                            <?php
                            $brand = get_terms(array('taxonomy' => 'pwb-brand', 'hide_empty' => false));
                            if (!empty($brand)) {
                                foreach ($brand as $key => $value) {
                                    ?>
                                    <div class="form-group checkbox">
                                        <label><input type="checkbox" class="icheck"
                                                      name="brand[<?php echo $value->slug; ?>]"/> <?php echo $value->name; ?>
                                            (<span class="number_product_<?php echo $value->slug; ?>"><?php echo $value->count; ?></span>)</label>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>

                    <div class="sidebar_title">Mức giá</div>
                    <div class="sidebar_content filter_price">
                        <div class="form-group">
                            <div class="col-sm-5 col-xs-5 no-padding">
                                <div class="input-group">
                                    <input type="text" name="price_from" class="price_from_input form-control"
                                           value="0"/>
                                    <span class="input-group-addon">đ</span>
                                </div>
                            </div>
                            <div class="col-sm-2 col-xs-2 no-padding text-center lh34">đến</div>
                            <div class="col-sm-5 col-xs-5 no-padding">
                                <div class="input-group">
                                    <input type="text" name="price_to" class="price_to_input form-control" value="0"/>
                                    <span class="input-group-addon">đ</span>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php
                        $priceRange = array(
                            array('from' => '0', 'to' => '10000000'),
                            array('from' => '10000000', 'to' => '20000000'),
                            array('from' => '20000000', 'to' => '50000000'),
                            array('from' => '50000000', 'to' => '100000000'),
                            array('from' => '100000000', 'to' => '200000000'),
                            array('from' => '200000000', 'to' => '500000000'),
                            array('from' => '500000000', 'to' => '0')
                        );
                        if (!empty($priceRange)) {
                            foreach ($priceRange as $key => $value) {
                                $countProduct = 0;
                                $args = array(
                                    'post_type' => 'product',
                                    'posts_per_page' => -1,
                                    'meta_query' => array(
                                        array(
                                            'key' => '_price',
                                            'value' => $value['from'],
                                            'compare' => '>',
                                            'type' => 'NUMERIC'
                                        ),
                                        array(
                                            'key' => '_price',
                                            'value' => $value['to'] != 0 ? $value['to'] : 10000000000,
                                            'compare' => '<=',
                                            'type' => 'NUMERIC'
                                        )
                                    )
                                );
                                $listProduct = new WP_Query($args);
                                $countProduct = $listProduct->post_count;

                                ?>
                                <div class="form-group select_price_filter" data-from="<?php echo $value['from']; ?>"
                                     data-to="<?php echo $value['to']; ?>">
                                    <?php if (!empty($value['to'])) { ?>
                                        <span class="filter_price_from"><?php echo number_format($value['from']); ?>
                                            đ</span> - <span
                                                class="filter_price_to"><?php echo number_format($value['to']); ?></span> (
                                        <span class="filter_price_count filter_price_count_<?php echo $value['from'] . '_' . $value['to']; ?>"><?php echo $countProduct; ?></span>)
                                    <?php } else { ?>
                                        <span class="filter_price_from">Trên <?php echo number_format($value['from']); ?>
                                            đ</span> (<span
                                                class="filter_price_count filter_price_count_<?php echo $value['from'] . '_' . $value['to']; ?>"><?php echo $countProduct; ?></span>)
                                    <?php } ?>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>

                    <div class="sidebar_title">Màu sắc</div>
                    <div class="sidebar_content">
                        <?php
                        $color = get_terms(array('taxonomy' => 'pa_mau-sac', 'hide_empty' => false));

                        if (!empty($color)) {
                            foreach ($color as $key => $value) {
                                ?>
                                <div class="col-sm-12" style="padding: 0">
                                    <div class="form-group">
                                        <?php $colorCode = get_field('color', $value);
                                        ?>
                                        <div class="color_box select_color"
                                             data-color="<?php echo $value->slug; ?>" style="  font-size: 14px;line-height: 20px" ><?php echo !empty($value->name) ?   $value->name  : ''; ?></div>

                                    </div>
                                </div>
                                <?php
                            }
                        } ?>
                        <div class="clearfix"></div>
                        <input type="hidden" class="input_color" name="color"/>
                    </div>

                    <input type="submit" class="hide"/>

                    <div class="remove_filter_attr"><a href="#">Xóa bộ lọc</a></div>
                </div>

                <?php get_template_part('templates/part/sidebar', 'support'); ?>

                <?php get_template_part('templates/part/sidebar', 'bestsale'); ?>

            </div>
            <div class="col-sm-9">
                <div class="breadcrumb">
                    <?php
                    if (function_exists('bcn_display')) {
                        bcn_display();
                    }
                    ?>
                </div>
                <div class="about_content">
                    <div class="product_title_block">
                        <h1>Piano</h1>
                    </div>
                    <div class="form-group hidden-xs">
                        <?php
                        $args = array(
                            'post_type' => 'product',
                            'posts_per_page' => 10
                        );
                        $listProduct = new WP_Query($args);

                        ?>
                        Có <?php echo $listProduct->post_count; ?> sản phẩm. Hiển thị sản phẩm
                        1-<?php echo $listProduct->post_count > 12 ? 12 : $listProduct->post_count; ?>.
                    </div>

                    <div class="product_filter_block">
                        <div class="row">
                            <div class="col-sm-2 col-xs-6">
                                <div class="form-group">
                                    <select name="order_by" class="form-control" style="max-width: 120px">
                                        <option value="newest">Mới nhất</option>
                                        <option value="oldest">Cũ nhất</option>
                                        <option value="price_highest">Giá cao nhất</option>
                                        <option value="price_lowest">Giá thấp nhất</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-7 hidden-xs">
                                <div class="row show_filter_chose"></div>
                            </div>
                            <div class="col-sm-2 col-xs-6 pull-right hidden-xs">
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6 text-right lh34 no-padding-right">
                                        <div class="form-group">
                                            Hiển thị
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <select name="post_per_page" class="post_per_page form-control" style="width: 50px;height: 36px;border-radius: 2px">
                                                <option value="4">4</option>
                                                <option value="8">8</option>
                                                <option value="12" selected>12</option>
                                                <option value="24">24</option>
                                                <option value="48">48</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-2 pull-right hidden-lg visible-xs">
                                <div class="btn_open_fileter" style=" width: 78px;
								  height: 30px;background-image: linear-gradient(to bottom, #f7f8fa, #eaecef);
								  border-radius: 2px;border: solid 1px #cccccc; font-size: 13px;
								  font-weight: normal;
								  font-style: normal;
								  font-stretch: normal;
								  line-height: 30px;
								  letter-spacing: normal;
								  text-align: center;
								  color: #333333;">Bộ lọc</div>
                            </div>
                        </div>
                    </div>

                    <div class="product_page_list clearfix">
                        <div class="list_product_slide product_ajax_show">
                            <?php
                            if (have_posts()) {
                                while (have_posts()) {
                                    the_post();
                                    $_product = wc_get_product(get_the_ID());
                                    ?>
                                    <div class="product_box col-sm-3 col-xs-6">
                                        <div class="product_image">
                                            <a href="<?php the_permalink(); ?>">
                                                <img src="<?php the_post_thumbnail_url(); ?>" class="img-responsive"
                                                     alt="<?php the_title(); ?>">
                                            </a>
                                        </div>

                                        <div class="product_rating hidden-lg visible-xs">
                                            <?php $numberStar = get_field('rating', $_product);
                                            if($numberStar > 0){
                                                for ($star = 0; $star < $numberStar; $star++) {
                                                    ?>
                                                    <!--                                    <i class='fa fa-star'></i>-->
                                                    <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <?php }
                                            }else{ ?>
                                                <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <?php
                                            }
                                            ?>
                                        </div>

                                        <div class="product_name">
                                            <a href="<?php the_permalink(); ?>">
                                                <?php the_title(); ?>
                                            </a>

                                        </div>

                                        <div class="product_rating hidden-xs">
                                            <?php $numberStar = get_field('rating', $_product);
                                            if($numberStar > 0){
                                                for ($star = 0; $star < $numberStar; $star++) {
                                                    ?>
                                                    <!--                                    <i class='fa fa-star'></i>-->
                                                    <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <?php }
                                            }else{ ?>
                                                <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <img src="<?php echo get_bloginfo('template_url').'/images/star.png'  ?>">
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        
                                        <div class="stock_status hidden-lg visible-xs">
                                            <?php 
                                                $checkstatus = $_product->get_stock_quantity();
                                                if($checkstatus > 0){
                                                    echo "Còn hàng";
                                                }else{
                                                    echo "Hết hàng";
                                                }
                                            ?>
                                        </div>
                                        <div class="product_price">
                                            <?php echo $_product->get_price() ? number_format($_product->get_price()) . 'đ' : 'Liên hệ'; ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }

                            ?>

                        </div>

                        <div class="product_pagination">
                            <div class="col-sm-2 col-xs-6 pull-left hidden-xs" style="padding: 0px; margin-top: 30px;padding-left: 5px">
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6 text-right lh34 no-padding-right" style="width: 70px">
                                        <div class="form-group text-left">
                                            Hiển thị
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6" style="padding: 0px">
                                        <div class="form-group">
                                            <select name="post_per_page" class="post_per_page form-control" style="width: 50px;height: 36px;border-radius: 2px;padding: 0px;">
                                                <option value="4">4</option>
                                                <option value="8">8</option>
                                                <option value="12" selected>12</option>
                                                <option value="24">24</option>
                                                <option value="48">48</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php echo wp_link_pages();

                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<?php get_footer(); ?>

