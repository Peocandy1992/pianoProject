<?php get_header();
?>
<div class="single-product-page">
    <div class="breadcrumb">
        <?php
        if (function_exists('bcn_display')) {
            bcn_display();
        }
        ?>
    </div>
    <div class="row">
        <?php
        if (have_posts()) {
            the_post();
            $productId = get_the_ID();
            $brand = get_the_terms($productId, 'pwb-brand');
            if (!empty($brand)) {
                $brand = $brand[0];
            }
            $_product = wc_get_product($productId);
            setPostViews($productId);
            ?>
            <div class="col-sm-9">
                <div class="product_title_block">
                    <div class="row">
                        <div class="col-sm-9">
                            <h1>
                                <?php the_title(); ?>
                            </h1>
                            <div class="product_rating hidden-lg visible-xs">
                                <?php $numberStar = get_field('rating', $productId);
                                for ($star = 0; $star < $numberStar; $star++) {
                                    ?>
                                    <i class='fa fa-star'></i>
                                <?php }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="product_brand">
                                <?php
                                if (!empty($brand)) {
                                    $imageId = get_term_meta($brand->term_id, 'pwb_brand_image');
                                    if (!empty($imageId[0])) {
                                        $image = wp_get_attachment_url($imageId[0]);
                                    } else {
                                        $image = get_bloginfo('template_url') . '/images/yamaha.png';
                                    }
                                    ?>
                                    <img src="<?php echo $image; ?>" class="img-responsive"
                                         alt="<?php the_title(); ?>"/>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="product_image_block">
                    <div class="product_main_image">
                        <img src="<?php the_post_thumbnail_url(); ?>" class="img-responsive main_image"
                             alt="<?php the_title(); ?>"/>
                        <div class="product_main_zoom"></div>
                    </div>
                    <div class="product_thumb">
                        <div class="product_thumb_list center-block">
                            <?php
                            $attachment_ids = $_product->get_gallery_attachment_ids();
                            foreach ($attachment_ids as $key => $attachment_id) {
                                $image_link = wp_get_attachment_url($attachment_id);
                                ?>
                                <div class="product_thumb_item <?php echo $key == 0 ? 'active' : ''; ?>">
                                    <img src="<?php echo $image_link; ?>" data-src="<?php echo $image_link; ?>"
                                         class="img-responsive" alt="<?php the_title(); ?>"/>
                                </div>
                            <?php } ?>
                            <div class="clearfix"></div>
                        </div>
                        <div class="product_thumb_expand"><i class="fa fa-angle-down"></i></div>
                    </div>
                </div>

                <div class="visible-xs">
                    <div class="sidebar_product_detail_price">
                        <?php echo $_product->get_price() ? number_format($_product->get_price()) . 'đ' : 'Liên hệ'; ?>
                    </div>
                    <div class="product_id_name visibale-xs hidden-lg">
                        Mã sản phẩm: <?php the_title(); ?>
                    </div>

                    <div class="sidebar_product_ship">
                        <?php echo get_field('ship', $productId); ?>
                    </div>
                    <div class="sidebar_product_care">
                        <?php echo get_field('care', $productId); ?>
                    </div>
                    <div class="sidebar_product_status">
                        <?php
                        $store = $_product->get_stock_status();
                        if ($store == 'instock') {
                            echo 'Có sẵn hàng';
                        } else if ($store == 'outofstock') {
                            echo 'Đã hết hàng';
                        } else {
                            echo 'Chờ hàng về';
                        }
                        ?>
                    </div>
                    <ul class="sidebar_product_gift hidden-xs">
                        <?php
                        $gift = get_field('gift', $productId);
                        if (!empty($gift)) {
                            foreach ($gift as $key => $value) {
                                ?>
                                <li><?php echo $value['gift']; ?></li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                    <div class="sidebar_product_order hidden-xs">
                        <div class="input-group">
                            <input type="text" name="number" class="form-control number_product" value="1"/>
                            <input type="hidden" name="product_id" class="product_id"
                                   value="<?php echo $productId; ?>"/>
                            <div class="input-group-addon">
                                <button class="btn btn-warning btn_order">Đặt hàng ngay</button>
                            </div>
                        </div>
                    </div>

                    <div class="sidebar_product_order visible-xs">
                        <div class="input-group">
                            <input type="hidden" name="number" class="form-control number_product" value="1"/>
                            <input type="hidden" name="product_id" class="product_id"
                                   value="<?php echo $productId; ?>"/>
                            <div class="input-group-addon clearfix">
                                <!--                                <button class="btn btn-warning btn_order">Đặt hàng ngay</button>-->
                                <button class="btn btn-warning btn_order"><i class="fa fa-shopping-cart"
                                                                             aria-hidden="true"></i></button>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div style="text-decoration: underline" class="">Chia sẻ</div>

                        <div class="contact_order">
                            <h1>Liên hệ đặt hàng</h1>
                            <div class="btn_call">
                                <a href="tel:<?php echo get_field('phone', 'option'); ?>"><?php echo get_field('phone', 'option'); ?></a>
                            </div>

                            <div class="btn_email">
                                <a href="mailto:<?php echo get_field('email', 'option'); ?>"><?php echo get_field('email', 'option'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="product_content ">
                    <div class="row">

                        <div class="col-sm-6 hidden-xs">
                            <div class="product_content_box">
                                <div class="product_content_title">
                                    Thông tin khác
                                </div>
                                <div class="product_content_detail">
                                    <table class="table table-striped">
                                        <?php
                                        $otheInfo = get_field('other_info', $productId);
                                        if (!empty($otheInfo)) {
                                            foreach ($otheInfo as $key => $value) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $value['name']; ?></td>
                                                    <td class="text-right"><?php echo $value['value']; ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 hidden-xs">
                            <div class="product_content_box">
                                <?php $listSound = get_field('sound_model', $productId);
                                if ($listSound[0]['file_mp3'] !== false) {
                                    ?>
                                    <div class="product_content_title">
                                        Mẫu âm thanh
                                    </div>
                                    <div class="product_content_detail">
                                        <div class="product_sound_list">
                                            <div class="table_sound_container">
                                                <?php
                                                if (!empty($listSound)) {
                                                    $first = $listSound[0];
                                                    ?>
                                                    <div class="form-group">
                                                        <audio class="audio_play" controls controlsList="nodownload">
                                                            <source class="play_sound"
                                                                    src="<?php echo $first['file_mp3']['url']; ?>"
                                                                    type="audio/mpeg">
                                                            Your browser does not support the audio element.
                                                        </audio>
                                                    </div>
                                                    <table class="table table_sound">
                                                        <?php foreach ($listSound as $key => $value) { ?>
                                                            <tr <?php if ($key == 0) echo 'class="active"'; ?>>
                                                                <td>
                                                                    <a href="#" class="click_play_sound"
                                                                       data-src="<?php echo $value['file_mp3']['url']; ?>">
                                                                        <?php echo $value['name']; ?>
                                                                    </a>
                                                                </td>
                                                                <td class="text-right">
                                                                    <a href="#" class="click_play_sound"
                                                                       data-src="<?php echo $value['file_mp3']['url']; ?>">
                                                                        <?php echo $value['length']; ?>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </table>
                                                    <?php
                                                }
                                                ?>
                                            </div>

                                            <div class="product_sound_list_more">
                                                Xem thêm
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>


                        <div class="col-sm-12 hidden-xs">
                            <div class="product_content_box">
                                <div class="product_content_title">
                                    Mô tả sản phẩm
                                </div>
                                <div class="product_content_detail">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 visible-xs hidden-lg" style="padding: 0px;margin-bottom:30px">
                            <ul class="product_info">
                                <li class="gift treeview">
                                    <a href="javascript:void(0)" onclick="">Khuyến mại <span class="pull-right">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </span></a>
                                    <ul class="ul_item">
                                        <?php
                                        $gifts = get_field('gift', 'options');
                                        if (!empty($gifts)) {
                                            foreach ($gifts as $gift) {
                                                echo '<li>' . $gift['gift'] . '</li>';
                                            }
                                        } else {
                                            echo "<li> sản phẩm hiện không có quà tặng nào </li>";
                                        }
                                        ?>
                                    </ul>
                                </li>

                                <li class="technical_info treeview">
                                    <a href="#" onclick="">Thông số kĩ thuật <span class="pull-right">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </span></a>
                                    <ul class="ul_item">
                                        <?php
                                        $otheInfo = get_field('other_info', $productId);
                                        if (!empty($otheInfo)) {
                                            foreach ($otheInfo as $key => $value) {
                                                ?>
                                                <li>
                                                    <span class="col-xs-6 text-left"><?php echo $value['name']; ?></span>
                                                    <span class="col-xs-6 text-right">
                                                        <?php echo $value['value']; ?>
                                                    </span>
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>

                                <li class="simple_sound treeview">
                                    <a href="javascript:void(0)" onclick="">Âm thanh mẫu <span class="pull-right">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </span></a>
                                    <ul class="ul_item">
                                        <?php $listSound = get_field('sound_model', $productId);
                                        if ($listSound[0]['file_mp3'] !== false) {
                                            if (!empty($listSound)) {
                                                $first = $listSound[0]; ?>
                                                <div class="form-group">
                                                    <audio class="audio_play" controls controlsList="nodownload">
                                                        <source class="play_sound"
                                                                src="<?php echo $first['file_mp3']['url']; ?>"
                                                                type="audio/mpeg">
                                                        Your browser does not support the audio element.
                                                    </audio>
                                                </div>
                                                <div class="table table_sound">
                                                    <?php foreach ($listSound as $key => $value) { ?>
                                                        <div <?php if ($key == 0) echo 'class="active"'; ?>>
                                                            <div class="col-xs-6">
                                                                <a href="#" class="click_play_sound"
                                                                   data-src="<?php echo $value['file_mp3']['url']; ?>">
                                                                    <?php echo $value['name']; ?>
                                                                </a>
                                                            </div>
                                                            <div class="text-right">
                                                                <a href="#" class="click_play_sound"
                                                                   data-src="<?php echo $value['file_mp3']['url']; ?>">
                                                                    <?php echo $value['length']; ?>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>

                                <li class="product_description treeview">
                                    <a href="javascript:void(0)" onclick="">Mô tả <span class="pull-right">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </span></a>
                                    <ul class="ul_item">
                                        <?php the_content(); ?>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="relate_product visible-xs">
                        <div class="relate_product_title">
                            Sản phẩm liên quan
                        </div>
                        <div class="relate_product_list">
                            <?php
                            $relateProduct = wc_get_related_products($productId, 4);
                            if (!empty($relateProduct)) {
                                foreach ($relateProduct as $key => $value) {
                                    $p = wc_get_product($value);

                                    ?>
                                    <div class="product_box col-sm-3 col-xs-12">
                                        <div class="product_image col-xs-3">
                                            <a href="<?php echo get_the_permalink($value);
                                            ?>">
                                                <img src="<?php echo get_the_post_thumbnail_url($value); ?>"
                                                     class="img-responsive" alt="<?php echo get_the_title($value);
                                                ?>">
                                            </a>
                                        </div>
                                        <div class="product_name col-xs-8">
                                            <a href="<?php echo get_the_permalink($value);
                                            ?>">
                                                <?php echo get_the_title($value);
                                                ?>
                                            </a>
                                        </div>
                                        <div class="product_price">
                                            <?php echo $p->get_price() ? number_format($p->get_price()) . 'đ' : 'Liên hệ';
                                            ?>
                                        </div>
                                        <span class="right_angle"><i class="fa fa-angle-right"
                                                                     aria-hidden="true"></i></span>
                                    </div>
                                    <?php
                                }
                            }

                            ?>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-3">
                <div class="sidebar_box">
                    <div class="sidebar_content">
                        <div class="hidden-xs">
                            <div class="sidebar_product_title">
                                <?php the_title(); ?>
                            </div>
                            <div class="sidebar_product_detail_price">
                                <?php echo $_product->get_price() ? number_format($_product->get_price()) . 'đ' : 'Liên hệ'; ?>
                            </div>
                            <div class="sidebar_product_ship">
                                <?php echo get_field('ship', $productId); ?>
                            </div>
                            <div class="sidebar_product_care">
                                <?php echo get_field('care', $productId); ?>
                            </div>
                            <div class="sidebar_product_status">
                                <?php
                                $store = $_product->get_stock_status();
                                if ($store == 'instock') {
                                    echo 'Có sẵn hàng';
                                } else if ($store == 'outofstock') {
                                    echo 'Đã hết hàng';
                                } else {
                                    echo 'Chờ hàng về';
                                }
                                ?>
                            </div>
                            <div class="sidebar_product_order">
                                <div class="input-group hidden-xs">
                                    <input type="text" name="number" class="form-control number_product" value="1"/>
                                    <input type="hidden" name="product_id" class="product_id"
                                           value="<?php echo $productId; ?>"/>
                                    <div class="input-group-addon">
                                        <button class="btn btn-warning btn_order">Đặt hàng ngay</button>
                                    </div>
                                </div>

                                <div class="input-group visible-xs">
                                    <input type="hidden" name="number" class="form-control number_product" value="1"/>
                                    <input type="hidden" name="product_id" class="product_id"
                                           value="<?php echo $productId; ?>"/>
                                    <div class="input-group-addon">
                                        <button class="btn btn-warning btn_order btn_order_responsive"><i
                                                    class="fas fa-shopping-cart"></i></button>
                                    </div>
                                </div>
                            </div>
                            <ul class="sidebar_product_gift hidden-xs">
                                <?php
                                $gift = get_field('gift', $productId);
                                if (!empty($gift)) {
                                    foreach ($gift as $key => $value) {
                                        ?>
                                        <li><?php echo $value['gift']; ?></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>

                        <script type="text/javascript" language="javascript">
                            var $ = jQuery;
                            $(document).ready(function () {
                                $('.btn_order').click(function () {
                                    var product_id = $('.product_id').val();
                                    var number_product = parseInt($('.number_product').val());
                                    if (number_product > 0) {
                                        window.location = location.href + '?add-to-cart=' + product_id + '&quantity=' + number_product;
                                    } else {
                                        alert('Bạn phải nhập số sản phẩm muốn đặt hàng');
                                    }
                                });


                                $('.treeview').click(function (e) {
                                    e.preventDefault();

                                    $(this).find('fa-plus').toggle(function () {
                                        $(this).find('fa-plus').removeClass('fa-plus');
                                        $(this).find('fa-plus').addClass('fa-minus');
                                    });
                                    $(this).find('.ul_item').toggle(function () {
                                        $(this).addClass("menu-open");
                                        $(this).parent().addClass('active');
                                    });
                                });
                            });
                        </script>
                    </div>
                </div>

                <?php get_template_part('templates/part/sidebar', 'support'); ?>

                <div class="sidebar_box">
                    <div class="sidebar_title">Chia sẻ</div>
                    <div class="sidebar_content">
                        <div class="col-sm-4 col-xs-6 share_box">
                            <div class="share_box_image">
                                <img src="<?php bloginfo('template_url'); ?>/images/ic-facebook-big.png"
                                     class="img-responsive" alt="Share"/>
                            </div>
                            <div class="share_box_name">
                                Facebook
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6 share_box">
                            <div class="share_box_image">
                                <img src="<?php bloginfo('template_url'); ?>/images/icon_gplus.png"
                                     class="img-responsive" alt="Share"/>
                            </div>
                            <div class="share_box_name">
                                Google+
                            </div>
                        </div>
                        <div class="clearfix visible-xs"></div>

                        <div class="col-sm-4 col-xs-6 share_box">
                            <div class="share_box_image">
                                <img src="<?php bloginfo('template_url'); ?>/images/icon_email.png"
                                     class="img-responsive" alt="Share"/>
                            </div>
                            <div class="share_box_name">
                                Email
                            </div>
                        </div>
                        <div class="clearfix hidden-xs"></div>
                        <div class="col-sm-4 col-xs-6 share_box">
                            <div class="share_box_image">
                                <img src="<?php bloginfo('template_url'); ?>/images/icon_pinterest.png"
                                     class="img-responsive" alt="Share"/>
                            </div>
                            <div class="share_box_name">
                                Pinterest
                            </div>
                        </div>
                        <div class="clearfix visible-xs"></div>
                        <div class="col-sm-4 col-xs-6 share_box">
                            <div class="share_box_image">
                                <img src="<?php bloginfo('template_url'); ?>/images/icon_twitle.png"
                                     class="img-responsive" alt="Share"/>
                            </div>
                            <div class="share_box_name">
                                Twitter
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>


                <?php get_template_part('templates/part/sidebar', 'bestsale'); ?>
            </div>
        <?php } ?>
    </div>
</div>


<?php get_footer(); ?>
