<?php get_header(); ?>
<div class="container page_container">
    <div class="breadcrumb">
        <?php
        if (function_exists('bcn_display')) {
            bcn_display();
        }
        ?>
    </div>
    <div class="row">

        <?php if (is_cart()) {
            get_template_part('templates/part/cart', 'table');
        } else if (is_checkout()) {
            get_template_part('templates/part/cart', 'checkout');
        } else {
            the_content();
        } ?>

    </div>
</div>
<?php get_footer(); ?>
