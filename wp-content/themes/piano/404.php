<?php get_header(); ?>
<div class="err_page container">
    <div class="col-sm-12">
        <div class="breadcrumb">
            <?php
            if (function_exists('bcn_display')) {
                bcn_display();
            }
            ?>
        </div>
        <div class="about_content">
            <div class="about_title">
                404 Not found
            </div>
            <div class="form-group">
                Đường dẫn không tồn tại hoặc đã bị xóa bởi quản trị viên
            </div>
            <div class="form-group">&nbsp;</div>
        </div>
    </div>
</div>
<?php get_footer(); ?>

