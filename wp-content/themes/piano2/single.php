<?php
/**
 * Template Name: Online Guide
 */
?>
<?php get_header(); ?>
<div class="single_container container">
    <div class="col-sm-9 pull-right">
        <div class="breadcrumb">
            <?php
            if (function_exists('bcn_display')) {
                bcn_display();
            }
            ?>
        </div>
        <div class="about_content">
            <?php if (have_posts()) {
                the_post();
                ?>
                <div class="about_title">
                    <?php the_title(); ?>
                </div>
                <div class="form-group">
                    <?php the_excerpt(); ?>
                </div>
                <div class="form-group">&nbsp;</div>

                <div class="news_block">
                    <?php the_content(); ?>
                </div>
                <div class="about_title">
                    Tin liên quan
                </div>
                <div class="news_block">
                    <div class="news_list">
                        <div class="row">
                            <?php
                            $related = get_posts(array('category__in' => wp_get_post_categories($post->ID), 'numberposts' => 6, 'post__not_in' => array($post->ID)));
                            if ($related) foreach ($related as $post) {
                                setup_postdata($post); ?>
                                <div class="news_box col-sm-4 col-xs-6">
                                    <div class="news_image">
                                        <a href="<?php the_permalink(); ?>">
                                            <img src="<?php the_post_thumbnail_url(); ?>" class="img-responsive"
                                                 alt="<?php the_title(); ?>"/>
                                        </a>
                                    </div>
                                    <div class="news_info">
                                        <div class="news_title">
                                            <a href="<?php the_permalink(); ?>">
                                                <?php the_title(); ?>
                                            </a>
                                        </div>
                                        <div class="news_description">
                                            <?php the_excerpt(); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                            wp_reset_postdata(); ?>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <?php
            } ?>
        </div>
    </div>
    <div class="sidebar col-sm-3 pull-left">

        <?php get_sidebar(); ?>
    </div>
</div>
<?php get_footer(); ?>

