<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}

wc_print_notices();

do_action('woocommerce_before_cart'); ?>


<form class="woocommerce-cart-form clearfix" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">
    <?php wp_nonce_field( 'woocommerce-cart' ); ?>
    <table class="table table-striped hidden-xs">
        <thead>
        <tr>
            <th class="hidden-xs" colspan="2">Sản phẩm</th>
            <th class="visible-xs">Sản phẩm</th>
            <th>Số lượng</th>
            <th class="text-right">Tổng cộng</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
            $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
            $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

            if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
                ?>

                <tr class="<?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?>">
                    <td class="hidden-xs" width="80">
                        <?php
                        $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

                        if (!$product_permalink) {
                            echo $thumbnail;
                        } else {
                            printf('<a href="%s">%s</a>', esc_url($product_permalink), $thumbnail);
                        }
                        ?>
                    </td>
                    <td class="cart_product_name">
                        <?php
                        if (!$product_permalink) {
                            echo apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key) . '&nbsp;';
                        } else {
                            echo apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s</a>', esc_url($product_permalink), $_product->get_name()), $cart_item, $cart_item_key);
                        }
                        ?>
                        <br/>
                        <span class="status_remain">Có sẵn hàng</span><br/>
                        <?php
                        echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
                            '<a href="%s" aria-label="%s" data-product_id="%s" data-product_sku="%s">Xoá</a>',
                            esc_url(wc_get_cart_remove_url($cart_item_key)),
                            __('Xoá', 'woocommerce'),
                            esc_attr($product_id),
                            esc_attr($_product->get_sku())
                        ), $cart_item_key);
                        ?>
                    </td>
                    <td class="cart_product_quantity">
                        <?php
                        if ($_product->is_sold_individually()) {
                            $product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
                        } else {
                            $product_quantity = woocommerce_quantity_input(array(
                                'input_name' => "cart[{$cart_item_key}][qty]",
                                'input_value' => $cart_item['quantity'],
                                'max_value' => $_product->get_max_purchase_quantity(),
                                'min_value' => '0',
                                'product_name' => $_product->get_name(),
                            ), $_product, false);
                        }

                        echo apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item);
                        $pro_price = wc_get_product( $product_id );

                        ?>
                        <input type="hidden" class="product_price" value="<?php echo $pro_price->get_price(); ?>" data-id="<?php echo $product_id ?>" >
                    </td>
                    <td class="text-right cart_product_price" id="<?php echo "price_".$product_id; ?>">
                    <span class="cart_price">
                        <?php
                        echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key);
                        ?>
                    </span>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
        <tfoot>
        <tr class="hidden-xs">
            <td>
                <img src="<?php bloginfo('template_url'); ?>/images/ship.png" class="img-responsive" alt="Ship"/>
            </td>
            <td colspan="2">
                <label class="control-label pull-left">Ship hàng tới</label>
                <select name="location" class="form-control pull-left" style="width: 220px; margin-left: 27px;">
                    <option value="">Nghệ an</option>
                </select>
                <div class="clearfix"></div>
            </td>
            <td class="text-right">
                0đ
            </td>
        </tr>
        </tfoot>
    </table>
    <button style="display: none" type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>
</form>
<form class="woocommerce-cart-form2 clearfix" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">
    <table class="table-striped visible-xs col-xs-12 cart_table_responsive">
        <thead>
        <tr class="hidden-xs">
            <th>Sản phẩm</th>
            <th class="text-right">Tổng cộng</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
            $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
            $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

            if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
                ?>

                <tr class="<?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?>">

                    <td class="cart_product_name col-xs-6">
                        <div class="img_thumbnail_cart">
                            <?php
                            $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

                            if (!$product_permalink) {
                                echo $thumbnail;
                            } else {
                                printf('<a href="%s">%s</a>', esc_url($product_permalink), $thumbnail);
                            } ?>
                        </div>
                        <div class="right_thumb_cart">
                            <?php
                            if (!$product_permalink) {
                                echo apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key) . '&nbsp;';
                            } else {
                                echo apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s</a>', esc_url($product_permalink), $_product->get_name()), $cart_item, $cart_item_key);
                            }
                            ?>
                            <br/>
                            <div class="status_remain">Có sẵn hàng</div>

                            <div class="cart_price">
                                <?php
                                echo "Giá: " . apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key);
                                ?>
                            </div>
                            <div class="remove_items">
                                <?php
                                echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
                                    '<a href="%s" aria-label="%s" data-product_id="%s" data-product_sku="%s">Xoá</a>',
                                    esc_url(wc_get_cart_remove_url($cart_item_key)),
                                    __('Xoá', 'woocommerce'),
                                    esc_attr($product_id),
                                    esc_attr($_product->get_sku())
                                ), $cart_item_key);
                                ?>
                            </div>
                        </div>

                    </td>

                    <td class="text-right cart_product_price col-xs-6">

                        <div class="clearfix">
                            <?php
                            if ($_product->is_sold_individually()) {
                                $product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
                            } else {
                                $product_quantity = woocommerce_quantity_input(array(
                                    'input_name' => "cart[{$cart_item_key}][qty]",
                                    'input_value' => $cart_item['quantity'],
                                    'max_value' => $_product->get_max_purchase_quantity(),
                                    'min_value' => '0',
                                    'product_name' => $_product->get_name(),
                                ), $_product, false);
                            }

                            echo apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item);
                            ?>
                        </div>
                        <div class="total_product">
                            <?php
                            echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key);
                            ?>
                        </div>
                    </td>

                </tr>
                <?php
            }
        }
        ?>
    </table>

    <div class="clearfix hidden-lg visible-xs">
        <div class="col-xs-12" style="padding:8px 0px 4px 0px;">
            <div class="control-label pull-left" style=" font-family: Roboto;
                  line-height: 20px;
                  font-size: 15px;
                  font-weight: normal;
                  font-style: normal;
                  font-stretch: normal;
                  line-height: normal;
                  letter-spacing: normal;
                  text-align: left;
                  color: #333333;margin-bottom: 4px">Ship hàng tới
            </div>
            <div class="ship_location">
                <select name="location" class="form-control pull-left" style="border-radius: 4px">
                    <option value="">Nghệ an</option>
                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="text-left col-xs-6" style="padding: 0px;  line-height: 21px;
          font-family: Roboto;
          font-size: 16px;
          font-weight: normal;
          font-style: normal;
          font-stretch: normal;
          letter-spacing: normal;
          text-align: left;
          color: #333333;">
            Chi phí vận chuyển
        </div>
        <div class="text-right col-xs-6" style="padding: 0px;line-height: 21px;
    font-family: Roboto-regular;
    font-size: 16px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    letter-spacing: normal;
    text-align: right;
    color: #333333;">
            0 đ
        </div>
    </div>
    </tfoot>

    
</form>
<div class="cart-collaterals">
    <?php
    /**
     * Cart collaterals hook.
     *
     * @hooked woocommerce_cross_sell_display
     * @hooked woocommerce_cart_totals - 10
     */
    do_action('woocommerce_cart_collaterals');
    ?>
</div>

<?php do_action('woocommerce_after_cart'); ?>


<script type="text/javascript" language="javascript">
    var $ = jQuery;
    $(document).ready(function () {
        $('.cart_product_quantity .qty').on('input',function (e) {
            e.preventDefault();
            var numProduct = $(this).val();
            $(this).attr('value',numProduct);

            // var price = parseInt($('.product_price').val()) * numProduct;

            // $(this).closest('.cart_item').find('.cart_product_price').empty();
            // $(this).closest('.cart_item').find('.cart_product_price').html("<span class='cart_price'><span class='woocommerce-Price-amount amount'>"+ formatNumber(price,'.', ',') +"<span class='woocommerce-Price-currencySymbol'>₫</span></span>");

            // $('.s_total .woocommerce-Price-amount').empty();

            // var sum = 0;
            // $('.cart_product_price .woocommerce-Price-amount').each(function(i,obj){
            //     var price = $(this).text();
            //     var priceRemove = price.replace('₫','');
            //     var priceRmoveSeperate = priceRemove.split(',');

            //     var temp = '';
            //     for(i = 0; i < priceRmoveSeperate.length; i++){
            //         temp += priceRmoveSeperate[i];
            //     };

            //     sum = parseInt(temp) + parseInt(sum);
            // });

            // $('.s_total .woocommerce-Price-amount').html("<span class='woocommerce-Price-amount amount'>"+formatNumber(sum,'.', ',')+"<span class='woocommerce-Price-currencySymbol'>₫</span></span>");

            $("[name='update_cart']").trigger('click');
        });

        // function formatNumber(nStr, decSeperate, groupSeperate) {
        //     nStr += '';
        //     x = nStr.split(decSeperate);
        //     x1 = x[0];
        //     x2 = x.length > 1 ? '.' + x[1] : '';
        //     var rgx = /(\d+)(\d{3})/;
        //     while (rgx.test(x1)) {
        //         x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
        //     }
        //     return x1 + x2;
        // }

        // $('div.woocommerce').on('click', 'input.qty', function(){
        //         jQuery("[name='update_cart']").trigger("click");
        //     });
    });
</script>