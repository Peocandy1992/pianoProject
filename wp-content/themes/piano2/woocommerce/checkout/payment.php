<?php
/**
 * Checkout Payment Section
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}

if (!is_ajax()) {
    do_action('woocommerce_review_order_before_payment');
}
?>
    <div class="col-sm-7 payment_wrap hidden-xs">
        <div id="payment" class="woocommerce-checkout-payment " style="clear: left;">
            <div class="sidebar_title">HÌNH THỨC THANH TOÁN</div>
            <?php if (WC()->cart->needs_payment()) : ?>
                <ul class="wc_payment_methods payment_methods methods">
                    <?php
                    if (!empty($available_gateways)) {
                        foreach ($available_gateways as $gateway) {
                            wc_get_template('checkout/payment-method.php', array('gateway' => $gateway));
                        }
                    } else {
                        echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">' . apply_filters('woocommerce_no_available_payment_methods_message', WC()->customer->get_billing_country() ? esc_html__('Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce') : esc_html__('Please fill in your details above to see available payment methods.', 'woocommerce')) . '</li>'; // @codingStandardsIgnoreLine
                    }
                    ?>
                </ul>
            <?php endif; ?>
            <div class="payment_guild">
                <?php
                $paymentRepeat = get_field('cart_Info', 'option');
                if ($paymentRepeat) {
                    foreach ($paymentRepeat as $row) {
                        $imgBankUrl = $row['cart_Info_img'];
                        $infomation = $row['infomation_bank'];
                        ?>
                        <div class="payment_guild_box">
                            <?php if (!empty($imgBankUrl)) { ?>
                                <div class="payment_guild_bank">
                                    <img src="<?php echo $imgBankUrl ?>" alt="">
                                </div>
                            <?php }
                            if (!empty($infomation)) {
                                ?>
                                <div class="payment_guild_info">
                                    <?php echo $infomation; ?>
                                </div>
                            <?php } ?>
                        </div>
                        <?php
                    }
                }

                ?>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="col-sm-7 payment_wrap visible-xs hidden-lg">
        <div id="payment_responsive" class="woocommerce-checkout-payment " style="clear: left;">
            <div class="sidebar_title">2. Hình thức thanh toán</div>
            <?php if (WC()->cart->needs_payment()) : ?>
                <ul class="wc_payment_methods payment_methods methods">
                    <?php
                    if (!empty($available_gateways)) {
                        foreach ($available_gateways as $gateway) {
                            wc_get_template('checkout/payment-method.php', array('gateway' => $gateway));
                        }
                    } else {
                        echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">' . apply_filters('woocommerce_no_available_payment_methods_message', WC()->customer->get_billing_country() ? esc_html__('Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce') : esc_html__('Please fill in your details above to see available payment methods.', 'woocommerce')) . '</li>'; // @codingStandardsIgnoreLine
                    }
                    ?>
                </ul>
            <?php endif; ?>
            <div class="payment_guild">
                <?php
                $paymentRepeat = get_field('cart_Info', 'option');
                if ($paymentRepeat) {
                    foreach ($paymentRepeat as $row) {
                        $imgBankUrl = $row['cart_Info_img'];
                        $infomation = $row['infomation_bank'];
                        ?>
                        <div class="payment_guild_box">
                            <?php if (!empty($imgBankUrl)) { ?>
                                <div class="payment_guild_bank">
                                    <img src="<?php echo $imgBankUrl ?>" alt="">
                                </div>
                            <?php }
                            if (!empty($infomation)) {
                                ?>
                                <div class="payment_guild_info">
                                    <?php echo $infomation; ?>
                                </div>
                            <?php } ?>
                        </div>
                        <?php
                    }
                }

                ?>
            </div>
        </div>
    </div>
<?php
if (!is_ajax()) {
    do_action('woocommerce_review_order_after_payment');
}
