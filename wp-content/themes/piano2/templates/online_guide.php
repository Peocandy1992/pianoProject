<?php
/**
 * Template Name: Online Guide
 */
?>
<?php get_header(); ?>
<div class="container">
    <div class="sidebar col-sm-3">
        <?php get_sidebar(); ?>
    </div>
    <div class="col-sm-9">
        <div class="breadcrumb">
            <a href="#">HOME</a> &nbsp; &nbsp; > &nbsp; &nbsp;
            <a href="#">Tư vấn</a>
        </div>
        <div class="about_content">
            <div class="about_title">
                Tư vấn trực tuyến
            </div>
            <div class="form-group">
                <p>
                    Dưới đây là danh sách Hướng dẫn trực tuyến hiện tại của chúng tôi. Chúng tôi cố gắng phân tích một
                    số dòng sản phẩm cụ thể theo các thuật ngữ đơn giản với sự trợ giúp của hình ảnh và sơ đồ, bao gồm
                    lịch sử, cách sử dụng với từng dòng khác nhau. Một số bài viết bao gồm các thiết lập hoàn chỉnh và
                    các kỹ thuật sử dụng cụ thể khác. Nếu bạn đang phân vân cách chọn sản phẩm phù hợp cho mình hoặc bạn
                    đang cần tìm hiểu về một vấn đề cụ thể khác, vui lòng tìm hiểu các bài viết dưới đây.
                </p>
            </div>

            <div class="news_block">
                <div class="news_list">
                    <div class="row">
                        <?php for ($i = 0; $i < 12; $i++) { ?>
                            <div class="news_box col-sm-4">
                                <div class="news_image">
                                    <a href="#">
                                        <img src="<?php bloginfo('template_url'); ?>/images/news.png"
                                             class="img-responsive" alt="Product"/>
                                    </a>
                                </div>
                                <div class="news_info">
                                    <div class="news_title">
                                        <a href="#">
                                            Học đàn Pianio chuyên nghiệp dành cho mọi lứa tuổi
                                        </a>
                                    </div>
                                    <div class="news_description">
                                        Roland HP-603 là một chiếc digital piano flagship của Roland thuộc HP 600
                                        series.
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                </div>


                <div class="paginate text-right">
                    <ul class="ul_paginate">
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">6</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>

