<?php
/**
 * Template Name: Add To Cart
 */
?>
<?php get_header(); ?>
<div class="breadcrumb">
    <a href="#">HOME</a> &nbsp; &nbsp; > &nbsp; &nbsp;
    <a href="#">Giỏ hàng</a>
</div>
<div class="row">
    <div class="container">
        <div class="col-sm-9">
            <div class="about_title">
                Giỏ hàng của bạn
            </div>
            <div class="shopping_cart_container">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th colspan="2">Sản phẩm</th>
                        <th>Số lượng</th>
                        <th class="text-right">Tổng cộng</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td width="80">
                            <img src="<?php bloginfo('template_url'); ?>/images/product.png" class="img-responsive"
                                 style="width: 50px;" alt="Product"/>
                        </td>
                        <td>
                            <a href="#">Thomann DP-31 B</a><br/>
                            <span>Có sẵn hàng</span><br/>
                            <a href="#">Xoá</a>
                        </td>
                        <td width="100">
                            <input type="number" class="form-control" value="1"/>
                        </td>
                        <td width="200" class="text-right">
                        <span class="cart_price">
                            48.200.000 đ
                        </span>
                        </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td>
                            <img src="<?php bloginfo('template_url'); ?>/images/ship.png" class="img-responsive"
                                 alt="Ship"/>
                        </td>
                        <td colspan="2">
                            <label class="control-label pull-left">Ship hàng tới</label>
                            <select name="location" class="form-control pull-left"
                                    style="width: 220px; margin-left: 27px;">
                                <option value="">Nghệ an</option>
                            </select>
                            <div class="clearfix"></div>
                        </td>
                        <td class="text-right">
                            0đ
                        </td>
                    </tr>
                    </tfoot>
                </table>
                <div class="cart_total_container">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="#">Tiếp tục mua hàng</a>
                        </div>
                        <div class="col-sm-6 text-right">
                            <span class="s_total">Tổng: 48.200.000 đ </span>
                            <p>*Giá trên đã bao gồm VAT</p>
                        </div>
                    </div>

                    <a href="#" class="btn_checkout">
                        Tiến hành Thanh toán
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <?php get_sidebar('cart'); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
