<?php
/**
 * Template Name: Product
 */
?>
<?php get_header(); ?>
<div class="container">
    <div class="sidebar col-sm-3">

        <div class="sidebar_box">
            <div class="sidebar_title">THƯƠNG HIỆU</div>
            <div class="sidebar_content">
                <div class="form-group">
                    <div class="checkbox"><label><input type="checkbox"/> Yamaha (827)</label></div>
                    <div class="checkbox"><label><input type="checkbox"/> Yamaha (827)</label></div>
                    <div class="checkbox"><label><input type="checkbox"/> Yamaha (827)</label></div>
                    <div class="checkbox"><label><input type="checkbox"/> Yamaha (827)</label></div>
                    <div class="checkbox"><label><input type="checkbox"/> Yamaha (827)</label></div>
                    <div class="checkbox"><label><input type="checkbox"/> Yamaha (827)</label></div>
                    <div class="checkbox"><label><input type="checkbox"/> Yamaha (827)</label></div>
                    <div class="checkbox"><label><input type="checkbox"/> Yamaha (827)</label></div>
                    <div class="checkbox"><label><input type="checkbox"/> Yamaha (827)</label></div>
                    <div class="checkbox"><label><input type="checkbox"/> Yamaha (827)</label></div>
                    <div class="checkbox"><label><input type="checkbox"/> Yamaha (827)</label></div>
                </div>
            </div>

            <div class="sidebar_title">MỨC GIÁ</div>
            <div class="sidebar_content">
                <div class="form-group">15.000.000 - 30.000.000</div>
                <div class="form-group">15.000.000 - 30.000.000</div>
                <div class="form-group">15.000.000 - 30.000.000</div>
                <div class="form-group">15.000.000 - 30.000.000</div>
                <div class="form-group">15.000.000 - 30.000.000</div>
                <div class="form-group">15.000.000 - 30.000.000</div>
                <div class="form-group">15.000.000 - 30.000.000</div>
                <div class="form-group">15.000.000 - 30.000.000</div>
                <div class="form-group">15.000.000 - 30.000.000</div>
            </div>

            <div class="sidebar_title">MÀU SẮC</div>
            <div class="sidebar_content">
                <?php for ($i = 0; $i < 10; $i++) { ?>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="color_box"></div>
                        </div>
                    </div>
                <?php } ?>
                <div class="clearfix"></div>
            </div>

        </div>

        <?php get_template_part('templates/part/sidebar', 'support'); ?>

        <?php get_template_part('templates/part/sidebar', 'bestsale'); ?>

    </div>
    <div class="col-sm-9">
        <div class="breadcrumb">
            <a href="#">HOME</a> &nbsp; &nbsp; > &nbsp; &nbsp;
            <a href="#">Tư vấn</a>
        </div>
        <div class="about_content">
            <div class="product_title_block">
                <h1>Piano</h1>
            </div>
            <div class="form-group">
                2 sản phẩm. Hiển thị sản phẩm 1-24.
            </div>

            <div class="product_filter_block">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <select name="" class="form-control">
                                <option value="">Mới nhất</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <select name="" class="form-control">
                                <option value="">Yamaha</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3 pull-right">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    Hiển thị
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <select name="" class="form-control">
                                        <option value="">24</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="product_page_list">
                <div class="list_product_slide">
                    <?php for ($i = 0; $i < 20; $i++) { ?>
                        <div class="product_box col-sm-3">
                            <div class="product_image">
                                <a href="#">
                                    <img src="http://piano.local/wp-content/themes/piano/images/product.png"
                                         class="img-responsive" alt="Product">
                                </a>
                            </div>
                            <div class="product_name">
                                <a href="#">
                                    Yamaha B3 SG2 PE
                                </a>
                            </div>
                            <div class="product_price">
                                34.000.000d
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>

