<div class="col-sm-9">
    <div class="about_title">
        Giỏ hàng của bạn
    </div>
    <div class="shopping_cart_container">
        <?php the_content(); ?>
    </div>
</div>
<div class="col-sm-3">
    <?php get_sidebar('cart'); ?>
</div>