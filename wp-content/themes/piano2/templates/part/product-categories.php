<div class="category_block hidden-xs container">
    <div class="block_title">Danh mục sản phẩm</div>
    <div class="block_container">
        <ul class="list_category">
            <?php
            $cat = get_field('category', 'option');
            if (!empty($cat)) {

                foreach ($cat as $key => $value) {
                    $value = $value['product_category'];
                    $link = get_term_link($value->term_id);
                    $imageId = get_woocommerce_term_meta($value->term_id, 'thumbnail_id');
                    $image = wp_get_attachment_url($imageId);
                    $term = get_term($value->term_id, 'product_cat');


                    if ($key == 0) { ?>
                        <li class="col-xs-12  category_li first_cat_item">
                            <a href="<?php echo $link; ?>" class="clearfix">
                            <div class="category_image">

                                    <img src="<?php echo $image; ?>" class="img-responsive"
                                         alt="<?php echo $value->name; ?>"/>

                            </div>
                            <div class="category_name">
                                    <?php echo $value->name; ?>
                            </div>
                            <?php if (($term->count) > 0) { ?>
                                <div class="category_total_item">
                                    <?php echo $term->count ?> sản phẩm
                                </div>
                            <?php } ?>
                            </a>
                        </li>
                    <?php } else { ?>
                        <li class="col-xs-12 category_li">
                            <a href="<?php echo $link; ?>" class="clearfix">
                            <div class="category_image">

                                    <img src="<?php echo $image; ?>" class="img-responsive"
                                         alt="<?php echo $value->name; ?>"/>

                            </div>
                            <div class="category_name">
                                    <?php echo $value->name; ?>
                            </div>
                            <?php if (($term->count) > 0) { ?>
                                <div class="category_total_item">
                                    <?php echo $term->count ?> sản phẩm
                                </div>
                            <?php } ?>
                            </a>
                        </li>
                        <?php
                    }

                }
            }
            ?>
        </ul>
        <div class="clearfix"></div>
    </div>
</div>

<div class="searchBox_responsive hidden-lg visibile-xs clearfix">
    <form name="frm_search" class="frm_search" method="get" action="/">
        <button class="btn btn_search"><i class="fa fa-search"></i></button>
        <input type="text" name="q" placeholder="Tìm kiếm" class="form-control"/>

    </form>
</div>

<div class="quick_link_block hidden-lg visibile-xs clearfix">
    <?php
    $quickLink = get_field('quick_link', 'option');
    if (!empty($quickLink)) {
        foreach ($quickLink as $item) { ?>
            <div class='col-xs-6 quick_block_item'>
                <a href="<?php echo $item['href_quick_link'] ?>">
                    <div class="float-left col-xs-4 left_block">
                        <img src="<?php echo $item['icon_link'] ?>" alt="<?php echo $item['title_quick_link']; ?>">
                    </div>
                    <div class="float-right col-xs-8 right_block"><?php echo $item['title_quick_link']; ?></div>
                </a>
            </div>
        <?php }
    }
    ?>
</div>

<div class="category_block category_block_responsive visible-xs">
    <div class="block_title">Danh mục sản phẩm</div>
    <div class="block_container">
        <ul class="list_category">
            <?php
            $cat = get_field('category', 'option');
            if (!empty($cat)) {

                foreach ($cat as $key => $value) {
                    $value = $value['product_category'];
                    $link = get_term_link($value->term_id);
                    $imageId = get_woocommerce_term_meta($value->term_id, 'thumbnail_id');
                    $image = wp_get_attachment_url($imageId);
                    $term = get_term($value->term_id, 'product_cat');
                    ?>
                    <li class="col-xs-12 category_li">
                        <a href="<?php echo $link; ?>">
                            <div class="category_image">

                                <img src="<?php echo $image; ?>" class="img-responsive"
                                     alt="<?php echo $value->name; ?>"/>

                            </div>
                            <div class="category_name">
                                <?php echo $value->name; ?>
                            </div>
                            <span class="right_navigation">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </span>
                        </a>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>
        <div class="clearfix"></div>
    </div>
</div>