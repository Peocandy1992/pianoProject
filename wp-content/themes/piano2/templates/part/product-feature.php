<div class="hot_product_block hidden-xs container">
    <div class="block_title">Sản phẩm nổi bật</div>
    <div class="block_container">
        <div class="block_tab">
            <?php
            $brand = get_field('suggest_product', 'option');
            $idArray = array();
            foreach ($brand as $item) {
                $idArray[] = $item['product'];
            }

            if (!empty($brand)) {
            $feature = getSuggestProductByid($idArray, 9);
            ?>

            <div class="block_tab_content_wrap">
                <div class="list_product_slide">
                    <?php
                    if (!empty($feature)) {
                    foreach ($feature->posts as $key => $v) {
                        $productId = $v->ID;
                        $link = get_permalink($productId);
                        $_product = wc_get_product($productId);
                        ?>
                        <div class="col-xs-6 product_box product_20p">
                            <div class="product_image">
                                <a href="<?php echo $link; ?>">
                                    <img src="<?php echo get_the_post_thumbnail_url($productId); ?>"
                                         class="img-responsive"
                                         alt="<?php echo get_the_title($productId); ?>"/>
                                </a>
                            </div>
                            <div class="product_info_right">
                                <div class="product_name">
                                    <a href="<?php echo $link; ?>">
                                        <?php echo get_the_title($productId); ?>
                                    </a>
                                </div>
                                <div class="product_rating">
                                    <?php $numberStar = get_field('rating', $productId);
                                    if($numberStar > 0) {
                                        for ($star = 0; $star < $numberStar; $star++) {
                                            ?>
                                            <!--                                        <i class='fa fa-star'></i>-->
                                            <img src="<?php echo get_bloginfo('template_url') . '/images/star.png' ?>">
                                        <?php }
                                    }else{ ?>
                                        <img src="<?php echo  get_bloginfo('template_url').'/images/star.png'  ?>">
                                        <img src="<?php echo  get_bloginfo('template_url').'/images/star.png'  ?>">
                                        <img src="<?php echo  get_bloginfo('template_url').'/images/star.png'  ?>">
                                        <img src="<?php echo  get_bloginfo('template_url').'/images/star.png'  ?>">
                                        <img src="<?php echo  get_bloginfo('template_url').'/images/star.png'  ?>">
                                    <?php }
                                    ?>
                                </div>
                                <div class="product_price">
                                    <?php echo $_product->get_price() ? number_format($_product->get_price()) . 'đ' : 'Liên hệ'; ?>
                                </div>

                            </div>
                            <div class="product_mask clearfix">
                                <div class="product_image">
                                    <a href="<?php echo $link; ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url($productId); ?>"
                                             class="img-responsive"
                                             alt="<?php echo get_the_title($productId); ?>"/>
                                    </a>
                                </div>
                                <div class="product_info_right">
                                    <div class="product_name">
                                        <a href="<?php echo $link; ?>">
                                            <?php echo get_the_title($productId); ?>
                                        </a>
                                    </div>
                                    <div class="product_rating">
                                        <?php $numberStar = get_field('rating', $productId);
                                        if($numberStar > 0) {
                                            for ($star = 0; $star < $numberStar; $star++) {
                                                ?>
                                                <!--                                        <i class='fa fa-star'></i>-->
                                                <img src="<?php echo get_bloginfo('template_url') . '/images/star.png'; ?>">
                                            <?php }
                                        }else{ ?>
                                            <img src="<?php echo get_bloginfo('template_url').'/images/star.png';  ?>">
                                            <img src="<?php echo get_bloginfo('template_url').'/images/star.png';  ?>">
                                            <img src="<?php echo get_bloginfo('template_url').'/images/star.png';  ?>">
                                            <img src="<?php echo get_bloginfo('template_url').'/images/star.png';  ?>">
                                            <img src="<?php echo get_bloginfo('template_url').'/images/star.png';  ?>">
                                        <?php }
                                        ?>
                                    </div>
                                    <div class="product_price">
                                        <?php echo $_product->get_price() ? number_format($_product->get_price()) . 'đ' : 'Liên hệ'; ?>
                                    </div>
                                    <div class="add_to_cart">

                                        <?php $product = wc_get_product($productId); ?>
                                        <a href='<?php echo $product->add_to_cart_url() . '&quantity=1' ?>'
                                           class="btn btn-warning btn_order">Thêm Vào giỏ
                                            hàng</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                    }

                    ?>
                </div>
                <div class="clearfix"></div>
                <div class="view_all">
                    <a href="<?php echo get_home_url() . '/shop/' ?>">Xem tất cả</a>
                </div>
            </div>
        <?php } ?>
        </div>
        <?php
        }
        ?>

    </div>
</div>

<div class="hot_product_block hot_product_block_responsive visibile-xs hidden-lg">
    <div class="block_title">Sản phẩm nổi bật</div>
    <div class="block_container">
        <div class="block_tab_content_wrap">
            <ul class="list_product_responsive clearfix">
                <?php
                if (!empty($feature)) {
                    foreach ($feature->posts as $key => $v) {

                        $productId = $v->ID;
                        $link = get_permalink($productId);
                        $_product = wc_get_product($productId);
                        ?>
                        <li class="col-xs-12 product_box product_20p">
                            <a href="<?php echo $link; ?>">
                                <div class="product_image col-xs-3">

                                    <img src="<?php echo get_the_post_thumbnail_url($productId); ?>"
                                         class="img-responsive"
                                         alt="<?php echo get_the_title($productId); ?>"/>

                                </div>
                                <div class="product_info_right col-xs-9">
                                    <div class="product_name">
                                        <?php echo get_the_title($productId); ?>
                                    </div>

                                    <div class="product_price">
                                        <?php echo $_product->get_price() ? number_format($_product->get_price()) . 'đ' : 'Liên hệ'; ?>
                                    </div>
                                    <span class="right_angle"><i class="fa fa-angle-right"
                                                                 aria-hidden="true"></i></span>
                                </div>
                            </a>
                        </li>

                    <?php }
                }
                ?>
            </ul>
            <div class="view_all">
                <a href="#">Xem thêm</a>
            </div>
        </div>

    </div>

</div>


<div class="clearfix"></div>