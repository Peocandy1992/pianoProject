<div class="sidebar_box hidden-xs">
    <div class="sidebar_content sidebar_policy">
        <div class="row">
            <div class="col-sm-6 col-xs-6 text-center padding-top-1">
                <img src="<?php bloginfo('template_url'); ?>/images/policy_1.png" class="img-responsive center-block" alt="Policy" />
                <span>Bảo hành 3 năm cho tất cả sản phẩm</span>
            </div>
            <div class="col-sm-6 col-xs-6 text-center padding-top-1">
                <img src="<?php bloginfo('template_url'); ?>/images/policy_2.png" class="img-responsive center-block" alt="Policy" />
                <span>Miễn phí giao hàng khu vực nội thành</span>
            </div>
        </div>
    </div>
</div>