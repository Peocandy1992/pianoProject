<div class="news_block mt-40">
    <div class="block_title has_carousel_nav hidden-xs">
        <span>Tư vấn trực tuyến</span>
        <div class="carousel_box">
            <div class="carousel_pre">
                <img src="<?php echo  get_bloginfo('template_url').'/images/left-btn.png' ?>" alt="" class="btn-disable" >
                <img src="<?php echo  get_bloginfo('template_url').'/images/left-btn-disable.png' ?>" alt="" class="btn-able">
            </div>
            <div class="carousel_next">
                <img src="<?php echo  get_bloginfo('template_url').'/images/right-btn.png' ?>" alt="" class="btn-disable">
                <img src="<?php echo  get_bloginfo('template_url').'/images/right-btn-disable.png' ?>" alt="" class="btn-able">
            </div>
        </div>
    </div>

    <div class="block_title block_title_responsive visible-xs">
        <span>Tư vấn trực tuyến</span>
    </div>

    <div class="block_container">
        <div class="row">
            <div class="online_guide_box_carousel owl-carousel owl-theme hidden-xs">
                <?php
                $news = get_field('news', 'option');
                if (!empty($news)) {
                    foreach ($news as $key => $value) {
                        $value = $value['item'];
                        ?>
                        <div class="news_box">
                            <div class="news_image">
                                <a href="<?php echo get_the_permalink($value->ID); ?>">
                                    <img src="<?php echo get_the_post_thumbnail_url($value->ID); ?>"
                                         class="img-responsive" alt="<?php echo get_the_title($value->ID); ?>"/>
                                </a>
                            </div>
                            <div class="news_info">
                                <div class="news_title">
                                    <a href="<?php echo get_the_permalink($value->ID); ?>">
                                        <?php echo get_the_title($value->ID); ?>
                                    </a>
                                </div>
                                <div class="news_description">
                                    <?php echo get_the_excerpt($value->ID); ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>

            <div class="online_guide_box visibile-xs hidden-lg">
                <?php
                $news = get_field('news', 'option');
                $count = 0;
                if (!empty($news)) {
                    foreach ($news as $key => $value) {
                        if($count < 4) {
                            $count++;
                            $value = $value['item'];
                            ?>
                            <div class="news_box col-xs-6">
                                <div class="news_image">
                                    <a href="<?php echo get_the_permalink($value->ID); ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url($value->ID); ?>"
                                             class="img-responsive" alt="<?php echo get_the_title($value->ID); ?>"/>
                                    </a>
                                </div>
                                <div class="news_info">
                                    <div class="news_title">
                                        <a href="<?php echo get_the_permalink($value->ID); ?>">
                                            <?php echo get_the_title($value->ID); ?>
                                        </a>
                                    </div>
                                    <div class="news_description">
                                        <?php echo get_the_excerpt($value->ID); ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>