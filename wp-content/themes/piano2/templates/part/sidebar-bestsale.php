<div class="sidebar_box">
    <div class="sidebar_title special_title">Sản phẩm bán chạy</div>
    <div class="sidebar_content clearfix">
        <?php
        $bestSale = get_field('bestsale_product', 'option');
        if(!empty($bestSale)){
            foreach($bestSale as $key => $value){
                $product = $value['item'];
                $productId = $product->ID;
                $_product = wc_get_product($productId);
        ?>
        <div class="sidebar_product col-sm-12 no-padding-lg col-xs-6">
            <div class="row">
                <div class="sidebar_product_image col-sm-4 no-padding-right">
                    <a href="<?php echo get_permalink($productId); ?>">
                        <img src="<?php bloginfo('template_url'); ?>/images/product.png" class="img-responsive" alt="<?php echo get_the_title($productId); ?>" />
                    </a>
                </div>
                <div class="sidebar_product_info col-sm-8">
                    <div class="sidebar_product_name">
                        <a href="<?php echo get_the_permalink($productId); ?>">
                            <?php echo get_the_title($productId); ?>
                        </a>
                    </div>
                    <div class="sidebar_product_price">
                        <?php echo $_product->get_price() ? number_format($_product->get_price()).'đ' : 'Liên hệ'; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
            }
        }
        ?>

    </div>
</div>