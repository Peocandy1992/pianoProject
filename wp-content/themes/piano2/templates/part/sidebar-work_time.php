<div class="sidebar_box">
    <div class="sidebar_title">Giờ làm việc</div>
    <div class="sidebar_content">
        <div class="support_box text-center">
            <div class="row">
                <table class="col-sm-8 col-sm-push-2 text-right">
                    <tr>
                        <td class="text-right" style="font-size: 15px;font-family: Roboto-regular;">Thứ hai</td>
                        <td class="text-left" style="padding-left: 10px;font-size: 15px;font-family: Roboto-regular;margin-bottom:2px;line-height: 20px;">8:00-21:30</td>
                    </tr>
                    <tr>
                        <td class="text-right" style="font-size: 15px;font-family: Roboto-regular;">Thứ ba</td>
                        <td class="text-left" style="padding-left: 10px;font-size: 15px;font-family: Roboto-regular;margin-bottom:2px;line-height: 20px;">8:00-21:30</td>
                    </tr>
                    <tr>
                        <td class="text-right" style="font-size: 15px;font-family: Roboto-regular;">Thứ tư</td>
                        <td class="text-left" style="padding-left: 10px;font-size: 15px;font-family: Roboto-regular;margin-bottom:2px;line-height: 20px;">8:00-21:30</td>
                    </tr>
                    <tr>
                        <td class="text-right" style="font-size: 15px;font-family: Roboto-regular;">Thứ năm</td>
                        <td class="text-left" style="padding-left: 10px;font-size: 15px;font-family: Roboto-regular;margin-bottom:2px;line-height: 20px;">8:00-21:30</td>
                    </tr>
                    <tr>
                        <td class="text-right" style="font-size: 15px;font-family: Roboto-regular;">Thứ sáu</td>
                        <td class="text-left" style="padding-left: 10px;font-size: 15px;font-family: Roboto-regular;margin-bottom:2px;line-height: 20px;">8:00-21:30</td>
                    </tr>
                    <tr>
                        <td class="text-right" style="font-size: 15px;font-family: Roboto-regular;">Thứ bảy</td>
                        <td class="text-left" style="padding-left: 10px;font-size: 15px;font-family: Roboto-regular;margin-bottom:2px;line-height: 20px;">8:00-21:30</td>
                    </tr>
                    <tr>
                        <td class="text-right" style="font-size: 15px;font-family: Roboto-regular;">Chủ nhật</td>
                        <td class="text-left" style="padding-left: 10px;font-size: 15px;font-family: Roboto-regular;margin-bottom:2px;line-height: 20px;">8:00-21:30</td>
                    </tr>
                </table>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>