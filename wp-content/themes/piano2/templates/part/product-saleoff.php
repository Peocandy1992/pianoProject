<div class="news_product_block saleoff_block hidden-xs">
    <div class="block_title">
        <span>Sản phẩm giảm giá</span>
        <div class="carousel_box clearfix">
            <div class="carousel_pre">
                <img src="<?php echo  get_bloginfo('template_url').'/images/left-btn.png' ?>" alt="" class="btn-disable" >
                <img src="<?php echo  get_bloginfo('template_url').'/images/left-btn-disable.png' ?>" alt="" class="btn-able">
            </div>
            <div class="carousel_next">
                <img src="<?php echo  get_bloginfo('template_url').'/images/right-btn.png' ?>" alt="" class="btn-disable">
                <img src="<?php echo  get_bloginfo('template_url').'/images/right-btn-disable.png' ?>" alt="" class="btn-able">
            </div>
        </div>
    </div>
    <div class="block_container">
        <div class="row">
            <div class="saleoff_carousel owl-carousel owl-theme">
                <?php
                $newProduct = get_field('new_product', 'option');
                if (!empty($newProduct)) {
                    foreach ($newProduct as $key => $value) {
                        $value = $value['product'];
                        $productId = $value->ID;
                        $link = get_permalink($productId);
                        $_product = wc_get_product($productId);

                        ?>
                        <div class="saleoff_container">
                            <div class="product_box">
                                <div class="product_image">
                                    <a href="<?php echo $link; ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url($productId); ?>"
                                             class="img-responsive" alt="<?php echo get_the_title($productId); ?>"/>
                                    </a>
                                    <?php
                                    $productOriginaPrice = $_product->get_regular_price('number');
                                    $productPrice = $_product->get_price('number');
                                    $percent = '0';
                                    if (!empty($productOriginaPrice)) {
                                        $percent = (int)(($productPrice - $productOriginaPrice) / $productOriginaPrice * 100);
                                    }
                                    ?>
                                    <div class="saleoff_percent"><?php echo $percent; ?>%</div>
                                </div>
                                <div class="product_name">
                                    <a href="<?php echo $link; ?>">
                                        <?php echo get_the_title($productId); ?>
                                    </a>
                                </div>
                                <div class="product_rating">
                                    <?php $numberStar = get_field('rating', $productId);
                                    if ($numberStar > 0) {
                                        for ($star = 0; $star < $numberStar; $star++) {
                                            ?>
                                            <!--                                        <i class='fa fa-star'></i>-->
                                            <img src="<?php echo get_bloginfo('template_url') . '/images/star.png' ?>">
                                        <?php }
                                    } else { ?>
                                        <img src="<?php echo get_bloginfo('template_url') . '/images/star.png' ?>">
                                        <img src="<?php echo get_bloginfo('template_url') . '/images/star.png' ?>">
                                        <img src="<?php echo get_bloginfo('template_url') . '/images/star.png' ?>">
                                        <img src="<?php echo get_bloginfo('template_url') . '/images/star.png' ?>">
                                        <img src="<?php echo get_bloginfo('template_url') . '/images/star.png' ?>">
                                    <?php }
                                    ?>
                                </div>
                                <div class="product_price">
                                    <?php echo $_product->get_price() ? number_format($_product->get_price()) . 'đ' : 'Liên hệ'; ?>
                                </div>
                                <div class="product_mask">
                                    <div class="product_image">
                                        <a href="<?php echo $link; ?>">
                                            <img src="<?php echo get_the_post_thumbnail_url($productId); ?>"
                                                 class="img-responsive" alt="<?php echo get_the_title($productId); ?>"/>
                                        </a>
                                        <?php
                                        $productOriginaPrice = $_product->get_regular_price('number');
                                        $productPrice = $_product->get_price('number');
                                        $percent = '0';
                                        if (!empty($productOriginaPrice)) {
                                            $percent = (int)(($productPrice - $productOriginaPrice) / $productOriginaPrice * 100);
                                        }
                                        ?>
                                        <div class="saleoff_percent"><?php echo $percent; ?>%</div>
                                    </div>
                                    <div class="product_name">
                                        <a href="<?php echo $link; ?>">
                                            <?php echo get_the_title($productId); ?>
                                        </a>
                                    </div>
                                    <div class="product_rating" style="    padding-bottom: 0px;
                                    margin-top: 0px;
                                    padding-top: 0px;">
                                        <?php $numberStar = get_field('rating', $productId);
                                        if ($numberStar > 0) {
                                            for ($star = 0; $star < $numberStar; $star++) {
                                                ?>
                                                <!--                                        <i class='fa fa-star'></i>-->
                                                <img src="<?php echo get_bloginfo('template_url') . '/images/star.png' ?>">
                                            <?php }
                                        } else { ?>
                                            <img src="<?php echo get_bloginfo('template_url') . '/images/star.png' ?>">
                                            <img src="<?php echo get_bloginfo('template_url') . '/images/star.png' ?>">
                                            <img src="<?php echo get_bloginfo('template_url') . '/images/star.png' ?>">
                                            <img src="<?php echo get_bloginfo('template_url') . '/images/star.png' ?>">
                                            <img src="<?php echo get_bloginfo('template_url') . '/images/star.png' ?>">
                                        <?php }
                                        ?>
                                    </div>
                                    <div class="product_price" style=" padding-top: 0px; padding-bottom: 0px;">
                                        <?php echo $_product->get_price() ? number_format($_product->get_price()) . 'đ' : 'Liên hệ'; ?>
                                    </div>
                                    <div class="add_to_cart">

                                        <?php $product = wc_get_product($productId); ?>
                                        <a href='<?php echo $product->add_to_cart_url() . '&quantity=1' ?>'>
                                            <div class="btn btn-warning btn_order">Thêm Vào giỏ
                                                hàng
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                } ?>
            </div>
        </div>
    </div>
</div>