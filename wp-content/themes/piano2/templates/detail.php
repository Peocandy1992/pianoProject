<?php
/**
 * Template Name: Detail
 */
?>
<?php get_header(); ?>
<div class="breadcrumb">
    <a href="#">HOME</a> &nbsp; &nbsp; > &nbsp; &nbsp;
    <a href="#">Yamaha</a> &nbsp; &nbsp; > &nbsp; &nbsp;
    <a href="#">CVP-709 B</a>
</div>
<div class="row">
    <div class="container">
        <div class="col-sm-9">
            <div class="product_title_block">
                <div class="row">
                    <div class="col-sm-9">
                        <h1>
                            Yamaha CVP-709 B
                        </h1>
                    </div>
                    <div class="col-sm-3">
                        <div class="product_brand">
                            <img src="<?php bloginfo('template_url'); ?>/images/yamaha.png" class="img-responsive"
                                 alt="Product"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="product_image_block">
                <div class="product_main_image">
                    <img src="<?php bloginfo('template_url'); ?>/images/product_large.png" class="img-responsive"
                         alt="Product"/>
                    <div class="product_main_zoom"></div>
                </div>
                <div class="product_thumb">
                    <div class="product_thumb_list center-block">
                        <?php for ($i = 0; $i < 20; $i++) { ?>
                            <div class="product_thumb_item <?php echo $i == 0 ? 'active' : ''; ?>">
                                <img src="<?php bloginfo('template_url'); ?>/images/product.png"
                                     data-src="<?php bloginfo('template_url'); ?>/images/product.png"
                                     class="img-responsive" alt="Product"/>
                            </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                    <div class="product_thumb_expand"><i class="fa fa-angle-down"></i></div>
                </div>
            </div>

            <div class="product_content">
                <div class="row">

                    <div class="col-sm-6">
                        <div class="product_content_box">
                            <div class="product_content_title">
                                Mẫu âm thanh
                            </div>
                            <div class="product_content_detail">
                                <div class="product_sound_list">
                                    <table class="table table-striped">
                                        <tr>
                                            <td>E-Piano 1</td>
                                            <td class="text-right">00:32</td>
                                        </tr>
                                        <tr>
                                            <td>E-Piano 1</td>
                                            <td class="text-right">00:32</td>
                                        </tr>
                                        <tr>
                                            <td>E-Piano 1</td>
                                            <td class="text-right">00:32</td>
                                        </tr>
                                        <tr>
                                            <td>E-Piano 1</td>
                                            <td class="text-right">00:32</td>
                                        </tr>
                                        <tr>
                                            <td>E-Piano 1</td>
                                            <td class="text-right">00:32</td>
                                        </tr>
                                        <tr>
                                            <td>E-Piano 1</td>
                                            <td class="text-right">00:32</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="product_content_box">
                            <div class="product_content_title">
                                Thông tin khác
                            </div>
                            <div class="product_content_detail">
                                <table class="table table-striped">
                                    <tr>
                                        <td>Màu sắc</td>
                                        <td class="text-right">Gỗ nâu bóng</td>
                                    </tr>
                                    <tr>
                                        <td>Màu sắc</td>
                                        <td class="text-right">Gỗ nâu bóng</td>
                                    </tr>
                                    <tr>
                                        <td>Màu sắc</td>
                                        <td class="text-right">Gỗ nâu bóng</td>
                                    </tr>
                                    <tr>
                                        <td>Màu sắc</td>
                                        <td class="text-right">Gỗ nâu bóng</td>
                                    </tr>
                                    <tr>
                                        <td>Màu sắc</td>
                                        <td class="text-right">Gỗ nâu bóng</td>
                                    </tr>
                                    <tr>
                                        <td>Màu sắc</td>
                                        <td class="text-right">Gỗ nâu bóng</td>
                                    </tr>
                                    <tr>
                                        <td>Màu sắc</td>
                                        <td class="text-right">Gỗ nâu bóng</td>
                                    </tr>
                                    <tr>
                                        <td>Màu sắc</td>
                                        <td class="text-right">Gỗ nâu bóng</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="product_content_box">
                            <div class="product_content_title">
                                Mô tả sản phẩm
                            </div>
                            <div class="product_content_detail">
                                Đàn piano Kawai ND-21 mang đến chất lượng âm thanh trong, mạnh và một sự ổn định tuyệt
                                đối về kết cấu bề mặt bởi kỹ thuật bộ máy được sản xuất theo tiêu chuẩn của hãng Kawai –
                                Nhật Bản.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="relate_product">
                    <div class="relate_product_title">
                        Sản phẩm liên quan
                    </div>
                    <div class="relate_product_list list_product_slide">
                        <?php for ($i = 0; $i < 4; $i++) { ?>
                            <div class="product_box col-sm-3">
                                <div class="product_image">
                                    <a href="#">
                                        <img src="http://piano.local/wp-content/themes/piano/images/product.png"
                                             class="img-responsive" alt="Product">
                                    </a>
                                </div>
                                <div class="product_name">
                                    <a href="#">
                                        Yamaha B3 SG2 PE
                                    </a>
                                </div>
                                <div class="product_price">
                                    34.000.000d
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-sm-3">

            <div class="sidebar_box">
                <div class="sidebar_content">
                    <div class="sidebar_product_title">
                        Yamaha CVP-709 B
                    </div>
                    <div class="sidebar_product_detail_price">
                        76.000.000 đ
                    </div>
                    <div class="sidebar_product_ship">
                        Miễn phí giao hàng khu vực nội thành
                    </div>
                    <div class="sidebar_product_care">
                        Bảo hành 36 tháng
                    </div>
                    <div class="sidebar_product_status">
                        Có sẵn hàng
                    </div>
                    <div class="sidebar_product_order">
                        <div class="input-group">
                            <input type="text" name="number" class="form-control" value="1"/>
                            <div class="input-group-addon">
                                <button class="btn btn-warning btn_order">Đặt hàng ngay</button>
                            </div>
                        </div>
                    </div>
                    <ul class="sidebar_product_gift">
                        <li>Tặng ghế piano</li>
                        <li>Tặng bộ đổi nguồn</li>
                        <li>Tặng khăn phủ đàn</li>
                        <li>Tặng khăn phủ phím</li>
                    </ul>
                </div>
            </div>

            <?php get_template_part('templates/part/sidebar', 'support'); ?>

            <div class="sidebar_box">
                <div class="sidebar_title">Chia sẻ</div>
                <div class="sidebar_content">
                    <?php for ($i = 0; $i < 5; $i++) { ?>
                        <div class="col-sm-4 share_box">
                            <div class="share_box_image">
                                <img src="<?php bloginfo('template_url'); ?>/images/ic-facebook-big.png"
                                     class="img-responsive" alt="Share"/>
                            </div>
                            <div class="share_box_name">
                                Facebook
                            </div>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                </div>
            </div>


            <?php get_template_part('templates/part/sidebar', 'bestsale'); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
