<?php
/**
 * Template Name: About Us
 */
?>
<?php get_header(); ?>
<div class="container about_page">
    <div class="sidebar col-sm-3">
        <?php get_template_part('templates/part/sidebar', 'support'); ?>

        <?php get_template_part('templates/part/sidebar', 'work_time'); ?>

        <?php get_template_part('templates/part/sidebar', 'fanpage'); ?>
    </div>
    <div class="col-sm-9">
        <div class="breadcrumb">
            <?php
            if (function_exists('bcn_display')) {
                bcn_display();
            }
            ?>
        </div>
        <div class="about_content">
            <?php if (have_posts()) {
                the_post();
                ?>
                <div class="about_title">
                    <?php the_title(); ?>
                </div>

                <div class="about_content_top">
                    <div class="row">
                        <div class="col-sm-7">
                            <?php echo get_field('description_text', get_the_ID()); ?>
                        </div>
                        <div class="col-sm-5">
                            <img src="<?php echo get_field('description_image', get_the_ID()); ?>"
                                 class="img-responsive" alt="Director"/>
                            <span class="about_director">Nguyễn Bá Thành</span>
                        </div>
                    </div>
                    <?php the_content(); ?>
                </div>

                <div class="about_gallery">
                    <div class="about_title">
                        Một số hình ảnh về Pianobt
                    </div>
                    <div class="about_gallery_list">
                        <?php
                        $gallery = get_field('gallery', get_the_ID());
                        if (!empty($gallery)) {
                            foreach ($gallery as $key => $value) {
                                $image = !empty($value['sizes']['274_153']) ? $value['sizes']['274_153'] : $value['sizes']['medium'];
                                ?>
                                <div class="about_gallery_item">
                                    <img src="<?php echo $image; ?>" class="img-responsive" alt="Image"/>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
