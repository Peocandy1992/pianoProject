<?php get_header(); ?>
<div class="container" style="margin-top: 97px">
<div class="col-sm-9 pull-right">
    <div class="breadcrumb">
        <?php
        if(function_exists('bcn_display')){
            bcn_display();
        }
        ?>
    </div>
    <div class="about_content">
        <div class="about_title">
            <?php
            $category = get_queried_object();
            echo $category->name;
            ?>
        </div>
        <div class="form-group about_descriptions">
            <?php echo $category->category_description; ?>
        </div>
        <div class="form-group">&nbsp;</div>

        <div class="news_block">
            <div class="news_list">
                <div class="row">
                    <?php
                    if(have_posts()){
                        while(have_posts()){
                            the_post();
                    ?>
                    <div class="news_box col-sm-4 col-xs-6">
                        <div class="news_image">
                            <a href="<?php the_permalink(); ?>">
                                <img src="<?php the_post_thumbnail_url(); ?>" class="img-responsive" alt="<?php the_title(); ?>" />
                            </a>
                        </div>
                        <div class="news_info">
                            <div class="news_title">
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_title(); ?>
                                </a>
                            </div>
                            <div class="news_description">
                                <?php the_excerpt(); ?>
                            </div>
                        </div>
                    </div>
                    <?php
                        }
                    }
                    ?>
                    <div class="clearfix"></div>
                </div>
            </div>


            <div class="paginate text-right">
                <ul class="ul_paginate">
                    <?php
                    $args = array(
                        'type' => 'list',
                        'prev_text'          => __( '&laquo;' ),
                        'next_text'          => __( '&raquo;' ),
                    );
                    echo paginate_links($args); ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="sidebar col-sm-3 pull-left">
    <?php get_sidebar(); ?>
</div>
</div>
<?php get_footer(); ?>

