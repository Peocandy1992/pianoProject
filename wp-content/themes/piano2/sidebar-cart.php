<?php get_template_part('templates/part/sidebar-cart', 'policy'); ?>

<div class="sidebar_box hidden-xs">
    <div class="sidebar_title">Kênh thanh toán</div>
    <div class="sidebar_content">
        <p>Bạn có thể thanh toán theo các hình thức sau đây: Tiền mặt, Chuyển khoản Ngân hàng</p> 
        <div class="row">
            <div class="col-sm-6 col-xs-6">
                <img src="<?php bloginfo('template_url'); ?>/images/pay_1.png" class="img-responsive" alt="Pay" />
            </div>
            <div class="col-sm-6 col-xs-6">
                <img src="<?php bloginfo('template_url'); ?>/images/pay_2.png" class="img-responsive" alt="Pay" />
            </div>
        </div>
    </div>
</div>