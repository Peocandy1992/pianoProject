<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/html1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us">
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1.0, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo wp_title(''); ?></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/font-awesome.css"/>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.carousel.css"/>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/icheck/minimal.css"/>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.theme.default.css"/>
    <!-- Include swiper css -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/swiper.min.css"/>


    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head(); ?>

    <?php echo get_field('header', 'option'); ?>
</head>
<body>
<div class="header hidden-xs">
    <div class="header_top">
        <div class="container">
            <div class="header_top_left pull-left">
                <ul>
                    <!--                    <li>-->
                    <!--                        <a href="mailto:-->
                    <?php //echo get_field('email', 'option'); ?><!--" class="header_email"><i class="fa fa-envelope fz-15 mr-4"></i> -->
                    <?php //echo get_field('email', 'option'); ?><!--</a>-->
                    <!--                    </li>-->
                    <!--                    <li>-->
                    <!--                        <a href="mailto:-->
                    <?php //echo get_field('phone', 'option'); ?><!--" class="header_phone"><i class="fa fa-phone fz-16 mr-4"></i> -->
                    <?php //echo get_field('phone', 'option'); ?><!--</a>-->
                    <!--                    </li>-->
                    <li>
                        <a href="tel:<?php echo get_field('hotline', 'option'); ?>" class="header_hotline">
                            <!--                            <i class="fa fa-mobile fz-20 mr-4"></i> -->
                            <span>Hotline: </span> <?php echo get_field('phone', 'option'); ?>
                            / <?php echo get_field('hotline', 'option'); ?>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="header_top_right pull-right">
                <ul>
                    <li><a href="<?php echo site_url().'/lien-he'; ?>">Miễn phí shiping tất cả sản phẩm</a></li>
                    <li><a href="<?php echo site_url().'/lien-he'; ?>"><i class="fa fa-info" aria-hidden="true"></i> Hỗ trợ</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header_main">
        <div class="container">
            <div class="header_main_container">
                <div class="header_logo col-sm-2">
                    <a href="/">
                        <img src="<?php echo get_field('logo', 'option'); ?>" class="img-responsive" alt="Logo"/>
                    </a>
                </div>
                <div class="col-sm-5 col-sm-push-1 menu_left">
                    <?php
                    $nav_menu_walker = new nav_menu_walker;
                    wp_nav_menu(array('theme_location' => 'header-menu', 'container' => false,'walker'=> $nav_menu_walker)); ?>
                </div>

                <div class="header_search col-sm-2">
                    <form name="frm_search" class="frm_search" method="get" action="/">
                        <button class="btn btn_search"><i class="fa fa-search"></i></button>
                        <input type="text" name="q" placeholder="Tìm kiếm" class="form-control"/>

                    </form>
                </div>

                <div class="text-left header_cart">
                    <div class="dropdown">
                        <div class="btn_cart dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown"
                             aria-haspopup="true" aria-expanded="false">
                            <!--                            <img style="width: 23px" src="-->
                            <?php //bloginfo('template_url'); ?><!--/images/cart.png"-->
                            <!--                                 alt="Cart"/>         -->
                            <?php
                            $numberInCart = wc()->cart->get_cart_contents_count();
                            if($numberInCart >0){
                                ?>
                                <img style="width: 35px;height: 28px;"
                                     src="<?php bloginfo('template_url'); ?>/images/page-1.png"
                                     alt="Cart"/>
                                <span class="number_cart_items"
                                      style="color: #f36500;"><?php echo ($numberInCart !== null) ? $numberInCart : '0'; ?></span>
                                <?php
                            }else{ ?>
                                <img style="width: 35px;height: 28px;"
                                     src="<?php bloginfo('template_url'); ?>/images/cart-empty.png"
                                     alt="Cart"/>
                                <?php
                            }
                            ?>


                            <!--                            Giỏ hàng -->

                        </div>
                        <div class="dropdown-menu header_cart_dropdow" aria-labelledby="dropdownMenuButton">
                            <?php
                            $cart = wc()->cart->get_cart();
                            if (!empty($cart)) {
                                foreach ($cart as $key => $value) {
                                    $productId = $value['product_id'];
                                    $_product = wc_get_product($productId);
                                    ?>
                                    <div class="header_cart_item">
                                        <div class="header_cart_image">
                                            <img src="<?php echo get_the_post_thumbnail_url($productId); ?>"
                                                 class="img-responsive"
                                                 alt="<?php echo get_the_title($productId); ?>"/>
                                        </div>
                                        <div class="header_cart_info">
                                            <div class="header_cart_name">
                                                <a href="<?php echo get_the_permalink($productId); ?>"><?php echo get_the_title($productId); ?></a>
                                            </div>
                                            <div class="header_cart_quantity">
                                                <?php echo $value['quantity']; ?> chiếc
                                            </div>
                                            <div class="header_cart_price">
                                                <?php echo number_format($value['line_subtotal']); ?> đ
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                <?php }
                            } else { ?>
                                <div class="header_cart_empty">
                                    Không có sản phẩm nào trong giỏ hàng của bạn
                                </div>
                            <?php } ?>
                            <div class="header_cart_view">
                                <a href="<?php echo wc()->cart->get_cart_url(); ?>" class="header_cart_button">Xem
                                    giỏ hàng</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="header_menu">
        <div class="desktop_menu">
            <div class="container desktop_menu_container">
                <div class="row">
                    <!--                    <div class="col-sm-9 menu_left">-->
                    <!--                        --><?php //wp_nav_menu(array('theme_location' => 'header-menu', 'container' => false)); ?>
                    <!--                    </div>-->
                    <!--                    <div class="col-sm-3 text-right pull-right header_cart">-->
                    <!--                        <div class="dropdown">-->
                    <!--                            <div class="btn_cart dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown"-->
                    <!--                                 aria-haspopup="true" aria-expanded="false">-->
                    <!--                                <img style="width: 23px" src="-->
                    <?php //bloginfo('template_url'); ?><!--/images/cart.png"-->
                    <!--                                     alt="Cart"/>-->
                    <!--                                --><?php
                    //                                $numberInCart = wc()->cart->get_cart_contents_count();
                    //                                ?>
                    <!--                                Giỏ hàng (--><?php //echo $numberInCart; ?><!--)-->
                    <!--                            </div>-->
                    <!--                            <div class="dropdown-menu header_cart_dropdow" aria-labelledby="dropdownMenuButton">-->
                    <!--                                --><?php
                    //                                $cart = wc()->cart->get_cart();
                    //                                if (!empty($cart)) {
                    //                                    foreach ($cart as $key => $value) {
                    //                                        $productId = $value['product_id'];
                    //                                        $_product = wc_get_product($productId);
                    //                                        ?>
                    <!--                                        <div class="header_cart_item">-->
                    <!--                                            <div class="header_cart_image">-->
                    <!--                                                <img src="-->
                    <?php //echo get_the_post_thumbnail_url($productId); ?><!--"-->
                    <!--                                                     class="img-responsive"-->
                    <!--                                                     alt="-->
                    <?php //echo get_the_title($productId); ?><!--"/>-->
                    <!--                                            </div>-->
                    <!--                                            <div class="header_cart_info">-->
                    <!--                                                <div class="header_cart_name">-->
                    <!--                                                    <a href="-->
                    <?php //echo get_the_permalink($productId); ?><!--">-->
                    <?php //echo get_the_title($productId); ?><!--</a>-->
                    <!--                                                </div>-->
                    <!--                                                <div class="header_cart_quantity">-->
                    <!--                                                    -->
                    <?php //echo $value['quantity']; ?><!-- chiếc-->
                    <!--                                                </div>-->
                    <!--                                                <div class="header_cart_price">-->
                    <!--                                                    -->
                    <?php //echo number_format($value['line_subtotal']); ?><!-- đ-->
                    <!--                                                </div>-->
                    <!--                                            </div>-->
                    <!--                                            <div class="clearfix"></div>-->
                    <!--                                        </div>-->
                    <!--                                    --><?php //}
                    //                                } else { ?>
                    <!--                                    <div class="header_cart_empty">-->
                    <!--                                        Không có sản phẩm nào trong giỏ hàng của bạn-->
                    <!--                                    </div>-->
                    <!--                                --><?php //} ?>
                    <!--                                <div class="header_cart_view">-->
                    <!--                                    <a href="-->
                    <?php //echo wc()->cart->get_cart_url(); ?><!--" class="header_cart_button">Xem-->
                    <!--                                        giỏ hàng</a>-->
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="header_menu visible-xs hidden-lg">
    <nav class="navbar navbar-inverse navbar-default" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle"
            >
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="col-xs-6 res_icon" href="/" style="padding-top: 13px;">
                <img src="<?php echo get_field('logo', 'option'); ?>" class="img-responsive" alt="Logo"/>
            </a>
            <?php if(is_archive()){ ?>
                <div class="btn_open_fileter" style=" color: #ffffff;
                    float: right;
                    margin-right: 16px;
                    margin-left: 20px;
                    margin-top: 3px;">
                    <i class="fa fa-search" style="font-size: 20px"></i>
                </div>
            <?php }else{ ?>
                <div class="btn_open_fileter_not_shop" style=" color: #ffffff;
                    float: right;
                    margin-right: 16px;
                    margin-left: 20px;
                    margin-top: 3px;">
                    <i class="fa fa-search" style="font-size: 20px"></i>
                </div>
            <?php } ?>

            <div class="text-left header_cart">
                <div class="dropdown">
                    <div class="btn_cart dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown"
                         aria-haspopup="true" aria-expanded="false">
                        <!--                            <img style="width: 23px" src="-->
                        <?php //bloginfo('template_url'); ?><!--/images/cart.png"-->
                        <!--                                 alt="Cart"/>         -->
                        <?php
                        $numberInCart = wc()->cart->get_cart_contents_count();
                        if($numberInCart > 0){
                            ?>
                            <img style="width: 35px;height: 28px;"
                                 src="<?php bloginfo('template_url'); ?>/images/page-1.png"
                                 alt="Cart"/>

                            <span class="number_cart_items"
                                  style="color: #f36500;"><?php echo ($numberInCart !== null) ? $numberInCart : ''; ?></span>

                            <!--                            Giỏ hàng -->
                        <?php }else{ ?>
                            <img style="width: 35px;height: 28px;"
                                 src="<?php bloginfo('template_url'); ?>/images/cart-empty.png"
                                 alt="Cart"/>

                        <?php } ?>
                    </div>


                    <div class="dropdown-menu header_cart_dropdow" aria-labelledby="dropdownMenuButton">
                        <?php
                        $cart = wc()->cart->get_cart();
                        if (!empty($cart)) {
                            foreach ($cart as $key => $value) {
                                $productId = $value['product_id'];
                                $_product = wc_get_product($productId);
                                ?>
                                <div class="header_cart_item">
                                    <div class="header_cart_image">
                                        <img src="<?php echo get_the_post_thumbnail_url($productId); ?>"
                                             class="img-responsive"
                                             alt="<?php echo get_the_title($productId); ?>"/>
                                    </div>
                                    <div class="header_cart_info">
                                        <div class="header_cart_name">
                                            <a href="<?php echo get_the_permalink($productId); ?>"><?php echo get_the_title($productId); ?></a>
                                        </div>
                                        <div class="header_cart_quantity">
                                            <?php echo $value['quantity']; ?> chiếc
                                        </div>
                                        <div class="header_cart_price">
                                            <?php echo number_format($value['line_subtotal']); ?> đ
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            <?php }
                        } else { ?>
                            <div class="header_cart_empty">
                                Không có sản phẩm nào trong giỏ hàng của bạn
                            </div>
                        <?php } ?>
                        <div class="header_cart_view">
                            <a href="<?php echo wc()->cart->get_cart_url(); ?>" class="header_cart_button">Xem
                                giỏ hàng</a>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div class="full_screen_toogle">
                <a href="#" class="closed_menu">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M405 136.798L375.202 107 256 226.202 136.798 107 107 136.798 226.202 256 107 375.202 136.798 405 256 285.798 375.202 405 405 375.202 285.798 256z"/></svg>
                </a>
                <div class="menu_toggle_info">
                    <?php $logo_header_menu = get_field('logo_header_menu', 'option');
                    if (!empty($logo_header_menu)) { ?>
                        <div class="footer_logo">
                            <img src="<?php echo $logo_header_menu; ?>" alt="">
                        </div>
                        <?php
                    }
                    ?>
                    <div class="footer_comp_info">
                        <?php
                        $footer_company_infomation = get_field('footer_company_infomation', 'option');
                        if (!empty($footer_company_infomation)) {
                            echo $footer_company_infomation;
                        }?>
                        <div style="margin-top: 10px">
                            <?php
                            $hot_line = get_field('hot_line_responsive_menu','option');

                            if(!empty($hot_line)){
                                echo $hot_line;
                            }
                            ?>
                        </div>
                    </div>


                    <div class="footer_social">
                        <?php $facebookUrl = get_field('link_facebook', 'option');
                        if (!empty($facebookUrl)) {
                            ?>
                            <div class="social_item">
                                <a href="<?php echo $facebookUrl ?>">
                                    <i class="fa fa-facebook-f"></i>
                                </a>
                            </div>
                        <?php } ?>

                        <?php
                        $instaUrl = get_field('link_instagram', 'option');
                        if (!empty($instaUrl)) { ?>
                            <div class="social_item">
                                <a href="<?php echo $instaUrl ?>">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </div>
                            <?php
                        }
                        ?>

                        <?php
                        $pinteresUrl = get_field('link_pinterest', 'option');
                        if (!empty($pinteresUrl)) { ?>
                            <div class="social_item">
                                <a href="<?php echo $pinteresUrl ?>">
                                    <i class="fa fa-pinterest-p"></i>
                                </a>
                            </div>
                            <?php
                        }
                        ?>

                        <?php
                        $youUrl = get_field('link_youtube', 'option');
                        if (!empty($youUrl)) { ?>
                            <div class="social_item">
                                <a href="<?php echo $youUrl ?>">
                                    <i class="fa fa-youtube-play"></i>
                                </a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php


                    wp_nav_menu(array('theme_location' => 'header-menu', 'container' => false, 'menu_class' => 'nav navbar-nav')); ?>
                </div>
            </div><!-- /.navbar-collapse -->


    </nav>

    <div class="rightsidebar hidden-lg visible-xs">
        <form
                name="frm_filter_product" class="frm_filter_product2" action="<?php echo admin_url('admin-ajax.php'); ?>"
        >
            <input type="hidden" name="action" value="product" />
            <ul class="filter_block">
                <li class="clearfix">
                    <div class="text-left col-xs-4">
                        <h1 class="title_filter">
                            Bộ lọc
                        </h1>
                    </div>
                    <div class="col-xs-8">
                        <div class="btn btn_reset col-xs-6">Thiết lập lại</div>
                        <button class="btn btn_filter col-xs-5">Xong</button>
                    </div>
                </li>

                <li class="list_filter clearfix">
                    <div class="float-left col-xs-6">
                        Tình trạng
                    </div>
                    <div class="float-right col-xs-6 text-right">
                        <i class="fa fa-plus"></i>
                    </div>
                    <ul class="list_sub_filter">
                        <li class="list_sub_item">
                            <div class="form-group checkbox">

                                <label><input type="checkbox" class="icheck"
                                              name="in_stock"/> Còn hàng
                                </label>


                            </div>
                        </li>

                        <li class="list_sub_item">
                            <div class="form-group checkbox">

                                <label><input type="checkbox" class="icheck"
                                              name=""/> Liên hệ
                                </label>
                            </div>
                        </li>

                        <li class="list_sub_item">
                            <div class="form-group checkbox">

                                <label><input type="checkbox" class="icheck"
                                              name="outofstock"/> Đặt trước
                                </label>
                            </div>
                        </li>
                    </ul>
                </li>

                <li class="list_filter clearfix">
                    <div class="float-left col-xs-7">
                        Hãng sản xuất
                    </div>
                    <div class="float-right col-xs-5 text-right">
                        <i class="fa fa-plus"></i>
                    </div>
                    <ul class="list_sub_filter">
                        <?php $brand = get_terms(array('taxonomy' => 'pwb-brand', 'hide_empty' => false));
                        if (!empty($brand)) {
                            foreach ($brand as $key => $value) {
                                ?>
                                <li class="list_sub_item">
                                    <div class="form-group checkbox">
                                        <label><input type="checkbox" class="icheck"
                                                      name="brand[<?php echo $value->slug; ?>]"/> <?php echo $value->name; ?>
                                            (<span class="number_product_<?php echo $value->slug; ?>"><?php echo $value->count; ?></span>)</label>
                                    </div>
                                </li>
                                <?php
                            }
                        } ?>
                    </ul>
                </li>

                <li class="list_filter clearfix">
                    <div class="float-left col-xs-6">
                        Mức giá
                    </div>
                    <div class="float-right col-xs-6 text-right">
                        <i class="fa fa-plus"></i>
                    </div>
                    <ul class="list_sub_filter">
                        <li class="list_sub_item">
                            <div class="col-sm-5 col-xs-5 no-padding">
                                <div class="input-group">
                                    <input type="text" name="price_from" class="price_from_input form-control"
                                           value="0"/>
                                    <span class="input-group-addon">đ</span>
                                </div>
                            </div>
                            <div class="col-sm-2 col-xs-2 no-padding text-center lh34">đến</div>
                            <div class="col-sm-5 col-xs-5 no-padding">
                                <div class="input-group">
                                    <input type="text" name="price_to" class="price_to_input form-control" value="0"/>
                                    <span class="input-group-addon">đ</span>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <?php
                            $priceRange = array(
                                array('from' => '0', 'to' => '10000000'),
                                array('from' => '10000000', 'to' => '20000000'),
                                array('from' => '20000000', 'to' => '50000000'),
                                array('from' => '50000000', 'to' => '100000000'),
                                array('from' => '100000000', 'to' => '200000000'),
                                array('from' => '200000000', 'to' => '500000000'),
                                array('from' => '500000000', 'to' => '0')
                            );
                            if (!empty($priceRange)) {
                                foreach ($priceRange as $key => $value) {
                                    $countProduct = 0;
                                    $args = array(
                                        'post_type' => 'product',
                                        'posts_per_page' => -1,
                                        'meta_query' => array(
                                            array(
                                                'key' => '_price',
                                                'value' => $value['from'],
                                                'compare' => '>',
                                                'type' => 'NUMERIC'
                                            ),
                                            array(
                                                'key' => '_price',
                                                'value' => $value['to'] != 0 ? $value['to'] : 10000000000,
                                                'compare' => '<=',
                                                'type' => 'NUMERIC'
                                            )
                                        )
                                    );
                                    $listProduct = new WP_Query($args);
                                    $countProduct = $listProduct->post_count;

                                    ?>
                                    <div class="form-group select_price_filter clearfix"
                                         data-from="<?php echo $value['from']; ?>"
                                         data-to="<?php echo $value['to']; ?>">
                                        <?php if (!empty($value['to'])) { ?>
                                            <span class="filter_price_from"><?php echo number_format($value['from']); ?>
                                                đ</span> - <span
                                                    class="filter_price_to"><?php echo number_format($value['to']); ?></span> (
                                            <span class="filter_price_count filter_price_count_<?php echo $value['from'] . '_' . $value['to']; ?>"><?php echo $countProduct; ?></span>)
                                        <?php } else { ?>
                                            <span class="filter_price_from">Trên <?php echo number_format($value['from']); ?>
                                                đ</span> (<span
                                                    class="filter_price_count filter_price_count_<?php echo $value['from'] . '_' . $value['to']; ?>"><?php echo $countProduct; ?></span>)
                                        <?php } ?>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </li>
                    </ul>
                </li>

                <li class="list_filter clearfix">
                    <div class="float-left col-xs-6">
                        Màu sắc
                    </div>
                    <div class="float-right col-xs-6 text-right">
                        <i class="fa fa-plus"></i>
                    </div>
                    <ul class="list_sub_filter">
                        <?php
                        $color = get_terms(array('taxonomy' => 'pa_mau-sac', 'hide_empty' => false));
                        if (!empty($color)) {
                            foreach ($color as $key => $value) {
                                ?>
                                <?php $colorCode = get_field('color', $value); ?>
                                <div class="color_box select_color col-xs-4"
                                     data-color="<?php $value->slug; ?>" <?php echo !empty($colorCode) ? 'style="background: ' . $colorCode . '"' : ''; ?>></div>
                                <?php
                            }
                        } ?>
                        <div class="clearfix"></div>
                        <input type="hidden" class="input_color" name="color"/>
                    </ul>
                </li>
            </ul>
        </form>
    </div>

    <div class="rightsidebar_not_shop hidden-lg visible-xs">
        <form name="frm_search" class="frm_search" method="get" action="/">
            <button class="btn btn_search"><i class="fa fa-search"></i></button>
            <input type="text" name="q" placeholder="Tìm kiếm" class="form-control"/>

        </form>
    </div>

</div>

<script type="text/javascript">

    jQuery(document).ready(function () {
        var $ = jQuery;
        $('.closed_menu').on('click', function (e) {
            e.preventDefault();
            if ($(this).parent().parent().hasClass('open')) {
                $(this).parent().parent().removeClass('open');
            }
        });

        $('.btn_open_fileter').click(function (e) {
            e.preventDefault();
            if ($('.rightsidebar').hasClass('open')) {
                $('.rightsidebar').removeClass('open');
            } else {
                $('.rightsidebar').addClass('open');
            }
        });


        $('.btn_open_fileter_not_shop').click(function(e){
            e.preventDefault();
            if ($('.rightsidebar_not_shop').hasClass('active')) {
                $('.rightsidebar_not_shop').removeClass('active');
            } else {
                $('.rightsidebar_not_shop').addClass('active');
            }
        });

        $('.navbar-toggle').click(function (e) {
            e.preventDefault();
            if ($('#bs-example-navbar-collapse-1').hasClass('open')) {
                $('#bs-example-navbar-collapse-1').removeClass('open');
            } else {
                $('#bs-example-navbar-collapse-1').addClass('open');
            }
        });

        $('.list_filter').click(function (e) {
            e.preventDefault();
            console.log("chet");
            if ($(this).find('.list_sub_filter').hasClass('open')) {
                $(this).find('.list_sub_filter').removeClass('open');
            } else {
                $(this).find('.list_sub_filter').addClass('open');
            }
        });

        $('.btn_reset').click(function (e) {
            e.preventDefault();
            $('.list_sub_filter input').iCheck('uncheck');
            $('.price_from_input').val(0);
            $('.price_to_input ').val(0);
            $('.input_color').val('');
        })
    })
    ;

</script>

<div class="wrapper">