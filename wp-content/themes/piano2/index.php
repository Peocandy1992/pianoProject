<?php get_header(); ?>

<div class="policy_block container">
    <?php
    $commit_policy = get_field('commit_policy', 'option');
    if (!empty($commit_policy)) { ?>
        <div class="commit_policy visible-xs col-xs-12 text-center">
            <?php echo $commit_policy; ?>
        </div>
        <div class="clearfix"></div>
    <?php }
    ?>
    <div class="col-sm-12 col-sm-push-0 col-xs-8 col-xs-push-2" style="padding: 0px">
        <div class="row">

            <?php
            $policy = get_field('policy', 'option');
            if (!empty($policy)) {
                foreach ($policy as $key => $value) {
                    ?>
                    <div class="policy_box col-sm-3 col-xs-4 ">
                        <div class="policy_image">
                            <img src="<?php echo $value['icon']; ?>" class="img-responsive"
                                 alt="<?php echo $value['name']; ?>"/>
                        </div>
                        <div class="policy_name hidden-xs">
                            <?php echo $value['name']; ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php
                }
            }

            ?>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$commit_policy = get_field('commit_policy', 'option');
if (!empty($commit_policy)) { ?>
    <div class="commit_policy hidden-xs col-sm-6 col-sm-push-4 text-center">
        <?php echo $commit_policy; ?>
    </div>
    <div class="clearfix"></div>
<?php }
?>
<div class="banner">
    <div class="banner_image">
        <div class="banner_slider owl-carousel owl-theme">
            <?php
            $slider = get_field('slider', 'option');
            if (!empty($slider)) {
                foreach ($slider as $key => $value) {
                    ?>
                    <a href="<?php echo $value['link']; ?>">
                        <img src="<?php echo $value['image']; ?>" class="img-responsive" alt="Banner"/>
                    </a>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>

<?php get_template_part('templates/part/product', 'categories'); ?>


<?php get_template_part('templates/part/product', 'feature'); ?>

<div class="container main_content">
    <div class="row">
        <div class="col-sm-9">

            <?php get_template_part('templates/part/product', 'brand'); ?>


            <?php get_template_part('templates/part/product', 'new'); ?>


            <?php get_template_part('templates/part/product', 'support'); ?>


            <?php get_template_part('templates/part/product', 'saleoff'); ?>


            <?php get_template_part('templates/part/product', 'blog'); ?>


        </div>
        <div class="sidebar col-sm-3">
            <?php get_sidebar(); ?>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<?php get_footer(); ?>
