</div>

<div class="footer hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 first_footer_col">
                <?php //dynamic_sidebar('footer1'); ?>
                <?php $logo_footer = get_field('logo_footer', 'option');
                if (!empty($logo_footer)) { ?>
                    <div class="footer_logo">
                        <img src="<?php echo $logo_footer; ?>" alt="">
                    </div>
                    <?php
                }
                ?>
                <div class="footer_comp_info">
                    <?php
                    $footer_company_infomation = get_field('footer_company_infomation', 'option');
                    if (!empty($footer_company_infomation)) {
                        echo $footer_company_infomation;
                    }
                    ?>

                </div>


                <div class="footer_social">
                    <?php $facebookUrl = get_field('link_facebook', 'option');
                    if (!empty($facebookUrl)) {
                        ?>
                        <div class="social_item">
                            <a href="<?php echo $facebookUrl ?>">
                                <i class="fa fa-facebook-f"></i>
                            </a>
                        </div>
                    <?php } ?>

                    <?php
                    $instaUrl = get_field('link_instagram', 'option');
                    if (!empty($instaUrl)) { ?>
                        <div class="social_item">
                            <a href="<?php echo $instaUrl ?>">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    $pinteresUrl = get_field('link_pinterest', 'option');
                    if (!empty($pinteresUrl)) { ?>
                        <div class="social_item">
                            <a href="<?php echo $pinteresUrl ?>">
                                <i class="fa fa-pinterest-p"></i>
                            </a>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    $youUrl = get_field('link_youtube', 'option');
                    if (!empty($youUrl)) { ?>
                        <div class="social_item">
                            <a href="<?php echo $youUrl ?>">
                                <i class="fa fa-youtube-play"></i>
                            </a>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <!--            <div class="col-sm-8">-->
            <div class="col-sm-3">
                <?php dynamic_sidebar('footer2'); ?>
            </div>
            <div class="col-sm-3">
                <?php dynamic_sidebar('footer3'); ?>
            </div>
            <div class="col-sm-3">
                <?php dynamic_sidebar('footer4'); ?>
            </div>
            <!--                <div class="col-sm-3">-->
            <!--                    --><?php //dynamic_sidebar('footer5'); ?>
            <!--                </div>-->
            <!--            </div>-->
        </div>
        <div class="footer_copyright">
            © Piano BT 2018. All Rights Reserved
        </div>
    </div>
</div>

<div class="footer-responsive visible-xs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center info_box">
                <i class="fa fa-phone"></i>Hotline: <?php echo get_field('phone', 'option'); ?>
            </div>
            <div class="col-xs-12 text-center info_box">
                <i class="fa fa-envelope"></i>Email: <?php echo get_field('email', 'option'); ?>
            </div>

            <div class="responsive_rows col-xs-12">
                <div class="responsive_box">
                    <a href="<?php echo get_field('link_facebook', 'option'); ?>">
                        <img src="<?php bloginfo('template_url'); ?>/images/ic-facebook.png" class="img-responsive"
                             alt="Share"/>
                    </a>
                </div>

                <div class="responsive_box">
                    <a href="<?php echo get_field('link_youtube', 'option'); ?>">
                        <img src="<?php bloginfo('template_url'); ?>/images/ic-youtube.png" class="img-responsive"
                             alt="Share" style="padding-top: 3px;"/>
                    </a>
                </div>

                <div class="responsive_box">
                    <a href="<?php echo get_field('link_instagram', 'option'); ?>">
                        <img src="<?php bloginfo('template_url'); ?>/images/ic-instagram.png" class="img-responsive"
                             alt="Share"/>
                    </a>
                </div>
            </div>

            <div class="col-xs-12 text-center footer_comp_info">
                <?php
                $footer_company_infomation = get_field('footer_company_infomation', 'option');
                if (!empty($footer_company_infomation)) {
                    echo $footer_company_infomation;
                }
                ?>

                <?php //dynamic_sidebar('footer1'); ?>
            </div>
        </div>

        <div class="footer_copyright_responsive text-center">
            © Piano BT 2018. All Rights Reserved
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" language="JavaScript"
        src="<?php bloginfo('template_url'); ?>/js/owl.carousel.js"></script>
<script type="text/javascript" language="JavaScript" src="<?php bloginfo('template_url'); ?>/js/wow.min.js"></script>
<script type="text/javascript" language="JavaScript" src="<?php bloginfo('template_url'); ?>/js/icheck.min.js"></script>
<!-- include ionicons.js -->
<script src="https://unpkg.com/ionicons@4.2.2/dist/ionicons.js"></script>
<!-- add js swiper -->
<script type="text/javascript" language="JavaScript" src="<?php bloginfo('template_url'); ?>/js/swiper.min.js"></script>
<script type="text/javascript" language="JavaScript" src="<?php bloginfo('template_url'); ?>/js/functions.js"></script>

<?php echo get_field('footer', 'option'); ?>

</body>
</html>