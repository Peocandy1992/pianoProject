$(document).ready(function () {
    function detectmob() {
        if( navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)
        ){
            return true;
        }
        else {
            return false;
        }
    }


    Number.prototype.formatMoney = function (c, d, t) {
        var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };


    $('.ul_block_tab li').click(function () {
        var ID = $(this).attr('data-id');
        $('.ul_block_tab li').removeClass('active');
        $(this).addClass('active');
        $('.block_tab_content').hide();
        $(ID).show();
    });


    /***** BANNER SLIDER ***********/
    $('.banner_slider').owlCarousel({
        responsive : {
            0 : {
                items: 1,
                loop:true,
                autoplay: true
            },
            480 : {
                items: 1,
                loop:true,
                autoplay: true
            },
            768 : {
                items: 2,
                center:true,
                loop:true,
                nav: true,
                autoplay: true
            }
        }
    });


    /******* HOME FEATURE PRODUCT *********/
    $('.slide_product_carousel').owlCarousel({
        responsive : {
            0 : {
                items: 2,
            },
            480 : {
                items: 2,
            },
            768 : {
                items: 5,
            }
        },
        dots: false,
        nav: true,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        autoplay: true
    });

    $('.slide_product_carousel_4').owlCarousel({
        responsive : {
            0 : {
                items: 2,
            },
            480 : {
                items: 2,
            },
            768 : {
                items: 4,
            }
        },
        dots: false,
        nav: true,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        autoplay: true
    });


    /********* NEW PRODUCT *******/
    $('.news_product_list').owlCarousel({
        dots: false,
        margin: 0,
        responsive : {
            0 : {
                items: 2,
            },
            480 : {
                items: 2,
            },
            768 : {
                items: 4,
            }
        },
        loop:true,
    });

    $('.news_product_block .carousel_next').click(function() {
        $('.news_product_list').trigger('next.owl.carousel');
    });
    $('.news_product_block .carousel_pre').click(function() {
        $('.news_product_list').trigger('prev.owl.carousel');
    });


    $('.ul_list_brand').owlCarousel({
        dots: false,
        responsive : {
            0 : {
                items: 2,
            },
            480 : {
                items: 2,
            },
            768 : {
                items: 3,
            }
        }
    });
    $('.ul_list_brand_next').click(function() {
        $('.ul_list_brand').trigger('next.owl.carousel');
    });
    $('.ul_list_brand_pre').click(function() {
        $('.ul_list_brand').trigger('prev.owl.carousel');
    });

    /******* SALEOFF PRODUCT *******/
    $('.saleoff_carousel').owlCarousel({
        dots: false,
        responsive : {
            0 : {
                items: 2,
            },
            480 : {
                items: 2,
            },
            768 : {
                items: 4,
            }
        }
    });

    $('.carousel_next').click(function() {
        $('.saleoff_carousel').trigger('next.owl.carousel');
    });
    $('.carousel_pre').click(function() {
        $('.saleoff_carousel').trigger('prev.owl.carousel');
    });


    /******** NEWS *********/
    $('.news_box_carousel').owlCarousel({
        dots: false,
        responsive : {
            0 : {
                items: 2,
            },
            480 : {
                items: 2,
            },
            768 : {
                items: 4,
            }
        }
    });
    $('.news_box_carousel_next').click(function() {
        $('.news_box_carousel').trigger('next.owl.carousel');
    });
    $('.news_box_carousel_pre').click(function() {
        $('.news_box_carousel').trigger('prev.owl.carousel');
    });


    /********* ONLINE GUIDE ********/
    $('.online_guide_box_carousel').owlCarousel({
        dots: false,
        responsive : {
            0 : {
                items: 2,
            },
            480 : {
                items: 2,
            },
            768 : {
                items: 4,
            }
        }
    });
    $('.news_block .carousel_next').click(function() {
        $('.online_guide_box_carousel').trigger('next.owl.carousel');
    });
    $('.news_block .carousel_pre').click(function() {
        $('.online_guide_box_carousel').trigger('prev.owl.carousel');
    });

    $('.news_block_index .carousel_next').click(function() {
        $('.news_block_index .news_box_carousel').trigger('next.owl.carousel');
    });
    $('.news_block_index .carousel_pre').click(function() {
        $('.news_block_index .news_box_carousel').trigger('prev.owl.carousel');
    });



    $('.click_play_sound').click(function () {
        var src = $(this).attr('data-src');
        change(src);
        $('.table_sound tr').removeClass('active');
        $(this).parent().parent().addClass('active');
        return false;
    });

    function change(sourceUrl) {
        var audio = $(".audio_play");
        $(".play_sound").attr("src", sourceUrl);

        /****************/
        audio[0].pause();
        audio[0].load();//suspends and restores all audio element

        //audio[0].play(); changed based on Sprachprofi's comment below
        audio[0].oncanplaythrough = audio[0].play();
        /****************/
    }

    $('.category_support_carousel').owlCarousel({
        dots: false,
        responsive : {
            0 : {
                items: 2,
            },
            480 : {
                items: 2,
            },
            768 : {
                items: 4,
            }
        }
    });





    $('.product_sound_list_more').click(function () {
        $('.table_sound_container').toggleClass('active');
    });

    $('.product_thumb_expand').click(function(){
        $('.product_thumb_list').toggleClass('active');
    });

    $('.product_thumb_item img').click(function(){
        var SRC = $(this).attr('data-src');
        $('.product_main_image img').attr('src', SRC);
    });


    $('.product_main_zoom').click(function () {

    });

    $('.icheck').iCheck({
        checkboxClass: 'icheckbox_minimal'
    });

    $('.icheck2').iCheck({
        checkboxClass: 'icheckbox_minimal'
    });



    $('.frm_filter_product').submit(function () {
        $.ajax({
            url: $(this).attr('action'),
            type: 'post',
            dataType: 'json',
            data: $(this).serialize(),
            beforeSend: function(){
                showPreload();
            },
            complete: function(){
                hidePreload();
            },
            success: function(res){
                if(res.success){
                    $('.product_ajax_show').html(res.product);
                    $.each(res.count.brand, function (key, value) {
                        $('.number_product_'+key).text(value);
                    });
                    $.each(res.count.price, function (key, value) {
                        $('.filter_price_count_'+key).text(value);
                    });
                }else{
                    alert(res.message);
                }
            },
            error: function(){
                alert('Có lỗi trong quá trình xử lý. Mời bạn thử lại sau');
            }
        });
        return false;
    });

    $('.frm_filter_product2').submit(function () {
        $.ajax({
            url: $(this).attr('action'),
            type: 'post',
            dataType: 'json',
            data: $(this).serialize(),
            beforeSend: function(){
                showPreload();
            },
            complete: function(){
                hidePreload();
            },
            success: function(res){
                if(res.success){
                    $('.product_ajax_show').html(res.product);
                    $.each(res.count.brand, function (key, value) {
                        $('.number_product_'+key).text(value);
                    });
                    $.each(res.count.price, function (key, value) {
                        $('.filter_price_count_'+key).text(value);
                    });
                }else{
                    alert(res.message);
                }
            },
            error: function(){
                alert('Có lỗi trong quá trình xử lý. Mời bạn thử lại sau');
            }
        });
        return false;
    });


    $('.select_price_filter').click(function () {
        var priceFrom = parseInt($(this).attr('data-from'));
        var priceTo = parseInt($(this).attr('data-to'));
        $('.price_from_input').val(priceFrom);
        $('.price_to_input').val(priceTo);
        var html = '<div class="col-sm-8 chose_filter_price">\n' +
            '                                    <div class="form-group">\n' +
            '                                        <div class="input-group">\n' +
            '                                            <span class="form-control">Giá '+priceFrom.formatMoney(0, 3, ',')+' - '+priceTo.formatMoney(0, 3, ',')+'</span>\n' +
            '                                            <div class="input-group-addon btn_close_filter btn_close_filter_price">\n' +
            '                                                <i class="fa fa-close"></i>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                </div>';
        $('.chose_filter_price').remove();
        $('.show_filter_chose').append(html);

        $('.frm_filter_product').submit();
    });

    $('.select_color').click(function () {
        var color = $(this).attr('data-color');
        $('.input_color').val(color);
        $('.frm_filter_product').submit();
    });

    $('.icheck').on('ifChanged', function(event){
        $('.frm_filter_product').submit();
    });

    $('.post_per_page').change(function () {
        $('.frm_filter_product').submit();
    });

    $(document).on('click', '.btn_close_filter_price', function ()
    {
        $('.price_from_input').val(0);
        $('.price_to_input').val(0);
        $(this).parent().parent().parent().remove();
        $('.frm_filter_product').submit();
    });

    /** brand slider*/
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 4,
        slidesPerColumn: 2,
        spaceBetween: 0,
        navigation: {
            nextEl: '.swiper_next',
            prevEl: '.swiper_pre',
        },
    });

    $('.block_container.block_list_brand .row').css('margin-right','6px');

    $('.product_thumb_item').on('click',function (e) {
        $(this).parent().find('.active').removeClass('active');
        $(this).addClass('active');
    });

    $('.cart_row .woocommerce-Price-amount').after("<br>");

    $('.remove_filter_attr').on('click',function(e){
        e.preventDefault();
        $('.frm_filter_product')[0].reset();
        $('.frm_filter_product').submit();
    })

    $('.product_thumb_item').wrapAll('<div class="product_thumb_wrapper clearfix"></div>');
});


function showPreload()
{
    $('.ajax_load').show();
}

function hidePreload()
{
    $('.ajax_load').hide();
}



